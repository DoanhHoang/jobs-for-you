import userSlice from "../store/slices/userSlice";

export const convert = (str) => {
  var date = new Date(str),
    mnth = ("0" + (date.getMonth() + 1)).slice(-2),
    day = ("0" + date.getDate()).slice(-2);
  return [day, mnth, date.getFullYear()].join("-");
};

export const handleLogOut = (setAuth, dispatch, navigate) => {
  localStorage.removeItem("userAccount");
  dispatch(userSlice.actions.logOut());
  setAuth(false);
  navigate("/");
};

export const checkRole = (listCheckRole, role) => {
  const check =
    listCheckRole?.filter((item) => item === role || item?.name === role)
      .length > 0;

  return check;
};

export const checkJobApply = (listJobApply, jobId) => {
  const check =
    listJobApply?.filter((item) => item?.id === parseInt(jobId)).length > 0;

  return check;
};

//https://accounts.google.com/o/oauth2/auth?scope=email&redirect_uri=http://localhost:8080/login&response_type=code&client_id=960937734510-48v4403fs2peo7duacsfp93dq0qcalr9.apps.googleusercontent.com&approval_prompt=force
