import React, { useState, useEffect } from "react";
import { Outlet } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useSelector, useDispatch } from "react-redux";

import "./styles.scss";
import { ClientHeader, ClientFooter } from "../../components";
import { getJobsLikeByCandidateIdThunk } from "../../store/slices/JobSlice";
import { ROLE_NAME } from "../../config/constant/roleNameConstant";

const MainLayout = () => {
  const [open, setOpen] = useState(false);

  const { userDetail, roleDetail } = useSelector((state) => state.user);

  const dispatch = useDispatch();

  useEffect(() => {
    if (
      userDetail?.role?.length > 0 &&
      userDetail?.role[0]?.name === ROLE_NAME.CANDIDATE &&
      roleDetail.id
    ) {
      dispatch(getJobsLikeByCandidateIdThunk(roleDetail.id));
    }
  }, [userDetail, dispatch, roleDetail]);

  const handleOpenDrawer = () => {
    setOpen(true);
  };

  return (
    <>
      <div className="client-layout">
        <ClientHeader handleOpenDrawer={handleOpenDrawer} />
        <Outlet />
        <ClientFooter />
      </div>
      <ToastContainer />
    </>
  );
};

export default MainLayout;
