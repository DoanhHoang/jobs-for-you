import React from "react";

import "./styles.scss";

import { AccountForm } from "../../containers";
import { Outlet } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const AccountLayout = () => {
  return (
    <div className="account-layout">
      <AccountForm>
        <Outlet />
      </AccountForm>
      <ToastContainer hideProgressBar={true} />
    </div>
  );
};

export default AccountLayout;
