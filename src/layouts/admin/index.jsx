import React, { useState } from "react";
import { Grid, Hidden } from "@mui/material";
import { Outlet } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import "./styles.scss";
import AdminSidebar from "../../components/AdminSidebar";
import AdminHeader from "../../components/AdminHeader";
import AdminFooter from "../../components/AdminFooter";
import AdminDrawer from "../../components/AdminDrawer";

const AdminLayout = () => {
  const [open, setOpen] = useState(false);

  const handleOpenDrawer = () => {
    setOpen(true);
  };

  return (
    <>
      <Grid container className="admin-layout">
        <Grid item xs={0} lg={2.5} className="admin-layout__sidebar">
          <AdminSidebar />
        </Grid>
        <Grid item xs={12} lg={9.5} className="admin-layout__main">
          <AdminHeader handleOpenDrawer={handleOpenDrawer} />
          <div className="admin-layout__main-content">
            <Outlet />
          </div>
          <AdminFooter />
        </Grid>
      </Grid>
      <Hidden lgUp>
        <AdminDrawer open={open} setOpen={setOpen}>
          <AdminSidebar />
        </AdminDrawer>
      </Hidden>
      <ToastContainer />
    </>
  );
};

export default AdminLayout;
