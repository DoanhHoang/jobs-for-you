import React, { useState, useEffect } from "react";
import { Divider } from "@mui/material";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import PlaceOutlinedIcon from "@mui/icons-material/PlaceOutlined";
import CalendarMonthOutlinedIcon from "@mui/icons-material/CalendarMonthOutlined";

import "./styles.scss";
import { DemandList } from "../../../components";
// import userLogo from "../../../asset/img/admin-user.png";
import SignInNotification from "../../../components/CustomModal/SignInNotification";
import {
  getDemandDetailThunk,
  getDemandListThunk,
} from "../../../store/slices/DemandSlice";

const DemandDetail = ({ demandId }) => {
  const [containerWidth, setContainerWidth] = useState(false);
  const [open, setOpen] = useState(false);

  const { demandDetail, demandList } = useSelector((state) => state.demand);

  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    id ? setContainerWidth(true) : setContainerWidth(false);
  }, [id]);

  useEffect(() => {
    id && dispatch(getDemandDetailThunk(id));
    demandId && dispatch(getDemandDetailThunk(demandId));
    dispatch(getDemandListThunk());
  }, [dispatch, id, demandId]);

  return (
    <>
      <div className={"job-detail " + (containerWidth ? "job-container" : "")}>
        <div className="job-detail__header box">
          <h1>{demandDetail.name}</h1>
          <div className="job-detail__info">
            <div className="job-detail__info--address">
              <PlaceOutlinedIcon />
              <p>Ho Chi Minh City</p>
            </div>
            <div className="job-detail__info--date">
              <CalendarMonthOutlinedIcon />
              <p>
                {demandDetail.startDate} - {demandDetail.endDate}
              </p>
            </div>
          </div>
        </div>

        <div className="job-detail__main box">
          {/* <h3>Description</h3> */}
          <div classsName="job-detail__main--quill">
            <div
              dangerouslySetInnerHTML={{ __html: demandDetail.desciption }}
            ></div>
            {/* <h3>Requirement</h3> */}
            <div
              dangerouslySetInnerHTML={{ __html: demandDetail.requirement }}
            ></div>
            {/* <h3>Other Info</h3> */}
            <div
              dangerouslySetInnerHTML={{ __html: demandDetail.ortherInfo }}
            ></div>
          </div>
        </div>

        <Divider>More Demands</Divider>
        <DemandList list={demandList} />
      </div>
      <SignInNotification open={open} setOpen={setOpen} />
    </>
  );
};

export default DemandDetail;
