import Home from "./Home";
import FindJob from "./FindJob";
import PostJob from "./Company/PostJob";
import CannidateProfile from "./Cannidate/Profile";
import JobDetail from "./JobDetail";
import CompanyProfile from "./Company/Profile";
import UniversityProfile from "./University/Profile";
import FindTalent from "./FindTalent";
import EditCandidateProfile from "./Cannidate/EditProfile";
import EditCompanyProfile from "./Company/EditProfile";
import FindDemand from "./FindDemand";
import PostDemand from "./University/PostDemand";
import CandidateDetail from "./CandidateDetail";
import DemandDetail from "./DemandDetail";

export {
  Home,
  FindJob,
  PostJob,
  CannidateProfile,
  JobDetail,
  CompanyProfile,
  UniversityProfile,
  FindTalent,
  EditCandidateProfile,
  EditCompanyProfile,
  FindDemand,
  PostDemand,
  CandidateDetail,
  DemandDetail,
};
