import React from "react";
import { Grid } from "@mui/material";
import { useDispatch } from "react-redux";

import "./styles.scss";
// import { FilterSelect } from "../../../../components";
import { getListCandidateThunk } from "../../../../store/slices/CandidateSlice";

const Search = () => {
  // const [searchKey, setSearchKey] = useState(null);

  const dispatch = useDispatch();

  const handleSearchName = (e) => {
    if (e.target.value === "") {
      dispatch(getListCandidateThunk());
    } else {
      dispatch(
        getListCandidateThunk({ lastName: e.target.value, firstName: null })
      );
    }
  };

  return (
    <>
      <div className="search-container">
        <Grid container spacing={2}>
          <Grid item md={12}>
            <div className="search-container__search">
              <input
                type="text"
                placeholder="Search candidate..."
                onChange={handleSearchName}
              />
            </div>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default Search;
