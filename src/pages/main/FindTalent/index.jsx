import React, { useEffect } from "react";
import { Grid } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

import "./styles.scss";
import { FreelancerList, TrustBy } from "../../../components";
import Banner from "./Banner";
import Search from "./Search";
// import userLogo from "../../../asset/img/admin-user.png";

import { getListCandidateThunk } from "../../../store/slices/CandidateSlice";

export const FindTalentContext = React.createContext();

const FindTalent = () => {
  const dispatch = useDispatch();

  const { listCandidate } = useSelector((state) => state.candidate);

  useEffect(() => {
    dispatch(getListCandidateThunk());
  }, [dispatch]);

  return (
    <>
      <div className="find-talent">
        <div className="find-talent__container">
          <Grid container>
            <Grid item md={12}>
              <div className="find-talent_container-header">
                <Banner />
              </div>
            </Grid>
            <Grid item md={12}>
              <div className="find-talent_container-header">
                <TrustBy />
              </div>
            </Grid>
            <Grid item md={12}>
              <div className="find-talent_container-header">
                <Search />
              </div>
            </Grid>
            <Grid item md={12}>
              <div className="find-talent__container-list">
                <FreelancerList
                  title="List freelancer"
                  listFreelancer={listCandidate}
                />
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    </>
  );
};

export default FindTalent;
