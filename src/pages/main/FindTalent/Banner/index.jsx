import React from "react";
import { Grid } from "@mui/material";
import { Link } from "react-router-dom";

import "./styles.scss";

const Banner = () => {
  return (
    <>
      <div className="banner">
        <Grid container spacing={3}>
          <Grid item md={6}>
            <div className="banner__left">
              <h1>Your fastlane to the best tech talent</h1>
              <p>
                Let our recruiters find the right fit for your objectives with
                Talent Scout™.
              </p>
              <Link to="#">Hire a Top Developer</Link>
            </div>
          </Grid>
          <Grid item md={6}>
            <div className="banner__right">
              <h1>Professionals on Upwork rate Developer</h1>
              <p>on average from 2M+ reviews</p>
            </div>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default Banner;
