import React, { useState, useEffect } from "react";
import { Grid } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

import "./styles.scss";
import { JobList, AdminDrawer, TrustBy } from "../../../components";
import Banner from "./Banner";
import JobDetail from "../JobDetail";
import Search from "./Search";
import {
  getJobListThunk,
  searchJobThunk,
} from "../../../store/slices/JobSlice";

export const FindJobContext = React.createContext();

const FindJob = () => {
  const [open, setOpen] = useState(false);
  const [jobId, setJobId] = useState(null);

  const dispatch = useDispatch();
  const { jobList, filtersJob } = useSelector((state) => state.job);

  useEffect(() => {
    dispatch(searchJobThunk(filtersJob));
  }, [dispatch, filtersJob]);

  return (
    <FindJobContext.Provider value={{ open, setOpen }}>
      <div className="find-job">
        <div className="find-job__container">
          <Grid container>
            <Grid item md={12}>
              <div className="find-job_container-header">
                <Banner />
              </div>
            </Grid>
            <Grid item md={12}>
              <div className="find-job_container-header">
                <TrustBy />
              </div>
            </Grid>
            <Grid item md={12}>
              <div className="find-job_container-header">
                <Search filters={filtersJob} filterSlice={searchJobThunk} />
              </div>
            </Grid>
            <Grid item md={12}>
              <div className="find-job__container-list">
                <JobList list={jobList} setOpen={setOpen} setJobId={setJobId} />
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
      <AdminDrawer open={open} setOpen={setOpen}>
        <JobDetail jobId={jobId} />
      </AdminDrawer>
    </FindJobContext.Provider>
  );
};

export default FindJob;
