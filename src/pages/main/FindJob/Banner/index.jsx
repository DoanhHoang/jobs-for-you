import React from "react";
import { Grid } from "@mui/material";
import { Link } from "react-router-dom";

import "./styles.scss";

const Banner = () => {
  return (
    <>
      <div className="banner">
        <Grid container spacing={3}>
          <Grid item md={6}>
            <div className="banner__left">
              <h1>Find the best JavaScript jobs</h1>
              <p>
                It takes just one job to develop a successful relationship that
                can propel your career forward.
              </p>
              <Link to="#">Want to hire a JavaScript Developer?</Link>
            </div>
          </Grid>
          <Grid item md={6}>
            <div className="banner__right">
              <h1>Professionals on Upwork rate clients</h1>
              <p>on average from 2M+ reviews</p>
            </div>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default Banner;
