import React, { useState, useEffect } from "react";
import { Grid } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

import "./styles.scss";
import { FilterSelect } from "../../../../components";
import globalAPI from "../../../../config/api/globalAPI";
import JobSlice from "../../../../store/slices/JobSlice";

const { getProvinceList, getDistrictByProvince } = globalAPI;

const Search = ({ filters, filterSlice }) => {
  const [provinces, setProvinces] = useState([]);
  const [searchKeyword, setSearchKeyword] = useState("");
  const [provinceId, setProvinceId] = useState(0);

  const dispatch = useDispatch();

  useEffect(() => {
    getProvinceList().then((res) => {
      const listProvince = res.data.map((province) => {
        return {
          value: {
            province: province.id,
          },
          name: province.name,
        };
      });
      setProvinces([
        {
          value: {
            province: null,
          },
          name: "[All]",
        },
        ...listProvince,
      ]);
    });
  }, []);

  useEffect(() => {
    dispatch(
      JobSlice.actions.updateFilter({
        name: searchKeyword,
      })
    );
    dispatch(filterSlice({ name: searchKeyword }));
  }, [searchKeyword, dispatch, filterSlice]);

  return (
    <>
      <div className="search-container">
        <Grid container spacing={2}>
          <Grid item md={6}>
            <div className="search-container__search">
              <input
                type="text"
                placeholder="Search jobs..."
                onChange={(e) => {
                  setSearchKeyword(e.target.value);
                }}
              />
            </div>
          </Grid>
          <Grid item md={2}>
            <div className="search-container__filter">
              <FilterSelect
                listFilter={provinces}
                id="province"
                label="Province"
                filters={filters}
                filterSlice={JobSlice}
                setProvinceId={setProvinceId}
              />
            </div>
          </Grid>
          {/* <Grid item md={2}>
            <div className="search-container__filter">
              <FilterSelect
                listFilter={[
                  {
                    value: {
                      sort: 1,
                    },
                    name: "Front-end",
                  },
                  {
                    value: {
                      sort: 2,
                    },
                    name: "Back-end",
                  },
                  {
                    value: {
                      sort: 3,
                    },
                    name: "Business analysis",
                  },
                  {
                    value: {
                      sort: 4,
                    },
                    name: "Tester",
                  },
                  {
                    value: {
                      sort: 3,
                    },
                    name: "Technical lead",
                  },
                ]}
                id="position"
                label="Position"
              />
            </div>
          </Grid> */}

          <Grid item md={2}>
            <div className="search-container__filter">
              <FilterSelect
                listFilter={[
                  {
                    value: {
                      type: 0,
                    },
                    name: "[All]",
                  },
                  {
                    value: {
                      type: 1,
                    },
                    name: "Full-time",
                  },
                  {
                    value: {
                      type: 2,
                    },
                    name: "Part-time",
                  },
                  {
                    value: {
                      type: 3,
                    },
                    name: "Remote",
                  },
                ]}
                id="type"
                label="Type"
                filters={filters}
                filterSlice={JobSlice}
              />
            </div>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default Search;
