import React, { useEffect } from "react";
import { Grid } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";

import "./styles.scss";
import { DemandList, TrustBy } from "../../../components";
import Banner from "./Banner";
import { getDemandListThunk } from "../../../store/slices/DemandSlice";

const FindDemand = () => {
  const dispatch = useDispatch();
  const { demandList } = useSelector((state) => state.demand);

  useEffect(() => {
    dispatch(getDemandListThunk());
  }, [dispatch]);

  return (
    <>
      <div className="find-job">
        <div className="find-job__container">
          <Grid container>
            <Grid item md={12}>
              <div className="find-job_container-header">
                <Banner />
              </div>
            </Grid>
            <Grid item md={12}>
              <div className="find-job_container-header">
                <TrustBy />
              </div>
            </Grid>
            {/* <Grid item md={12}>
              <div className="find-job_container-header">
                <Search filters={filtersJob} filterSlice={searchJobThunk} />
              </div>
            </Grid> */}
            <Grid item md={12}>
              <div className="find-job__container-list">
                <DemandList
                  list={demandList}
                  // setOpen={setOpen}
                  // setJobId={setJobId}
                />
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
      {/* <AdminDrawer open={open} setOpen={setOpen}>
        <JobDetail jobId={jobId} />
      </AdminDrawer> */}
    </>
  );
};

export default FindDemand;
