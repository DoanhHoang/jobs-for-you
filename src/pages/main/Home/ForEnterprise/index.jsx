import React from "react";
import { Grid, Divider } from "@mui/material";

import "./styles.scss";

const ForEnterprise = () => {
  return (
    <>
      <div className="for-enterprise">
        <Grid container spacing={3}>
          <Grid item md={6}>
            <div className="for-enterprise__left">
              <h3>For Partner</h3>
              <h1>Scale with Enterprise Suite</h1>
              <p>
                A fast and secure way to achieve long-term success for your
                business goals.
              </p>
            </div>
            <Divider />
            <div className="for-enterprise__left">
              <p>Access expert talent to fill your skill gaps</p>
              <p>Control your workflow: hire, classify and pay your talent</p>
              <p>Partner with Upwork for end-to-end support</p>
            </div>
          </Grid>
          <Grid item md={6}>
            <div className="for-enterprise__right">
              <img
                src="https://res.cloudinary.com/upwork-cloud-acquisition-prod/image/upload/f_auto,q_auto/brontes/for-enterprise/enterprise-image@2x.png"
                alt="for-enterprise"
              />
            </div>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default ForEnterprise;
