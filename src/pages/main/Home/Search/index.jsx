import React from "react";
import { TextField, Select, Grid } from "@mui/material";

import "./styles.scss";
import fullStack from "../../../../asset/img/fullstack.png";
import frontEnd from "../../../../asset/img/front-end.png";
import backend from "../../../../asset/img/back-end.png";
import businessAnal from "../../../../asset/img/BA.png";

const Search = () => {
  return (
    <>
      <div className="search">
        <div className="search-box">
          <h1>
            explore more <span>jobs</span>
          </h1>
          <div className="search-box__search">
            <Grid container spacing={1}>
              <Grid item md={10}>
                <TextField placeholder="Search" fullWidth />
              </Grid>
              <Grid item md={2}>
                <button type="button">Search</button>
              </Grid>
            </Grid>
          </div>
          <div className="search-box__position">
            <Grid container className="search-box__position--card">
              <Grid item md={3}>
                <div>
                  <img src={fullStack} alt="" />
                  <h4>Full-stack</h4>
                  <p>3700 Jobs</p>
                </div>
              </Grid>
              <Grid item md={3}>
                <div>
                  <img src={frontEnd} alt="" />
                  <h4>Front-end</h4>
                  <p>3700 Jobs</p>
                </div>
              </Grid>
              <Grid item md={3}>
                <div>
                  <img src={backend} alt="" />
                  <h4>Back-end</h4>
                  <p>3700 Jobs</p>
                </div>
              </Grid>
              <Grid item md={3}>
                <div>
                  <img src={businessAnal} alt="" />
                  <h4>Business analysis</h4>
                  <p>3700 Jobs</p>
                </div>
              </Grid>
            </Grid>
          </div>
        </div>
      </div>
    </>
  );
};

export default Search;
