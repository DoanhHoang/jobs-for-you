import React from "react";
import { Grid } from "@mui/material";

import "./styles.scss";
import woman from "../../../../asset/img/woman.png";
import man from "../../../../asset/img/man.png";
import rectangle from "../../../../asset/img/rectangle.png";

const JoinUs = () => {
  return (
    <>
      <div className="join-us">
        <div className="join-us-container">
          <Grid container>
            <Grid item md={6}>
              <div className="join-us-container__left">
                <h1>
                  opportunities for <span>interns</span>
                </h1>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim
                  a debitis natus non rerum dicta mollitia iure. Perspiciatis
                  labore eligendi laboriosam illum, minima molestias nihil nemo
                  quisquam numquam nulla cupiditate fuga nesciunt blanditiis
                  placeat officiis corrupti dolores repellendus voluptates
                  explicabo?
                </p>
                <button type="button">Join Us</button>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="join-us-container__right">
                <img
                  src={rectangle}
                  alt="rectanle"
                  className="join-us-container__right--rectangle"
                />
                <img
                  src={man}
                  alt="man"
                  className="join-us-container__right--man"
                />
                <img
                  src={woman}
                  alt="woman"
                  className="join-us-container__right--woman"
                />
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
    </>
  );
};

export default JoinUs;
