import React from "react";
import { Grid, Divider } from "@mui/material";

import "./styles.scss";

const ForCandidate = () => {
  return (
    <>
      <div className="for-candidate">
        <Grid container spacing={3}>
          <Grid item md={6}>
            <div className="for-candidate__left">
              <img
                src="https://res.cloudinary.com/upwork-cloud-acquisition-prod/image/upload/c_scale,w_600,h_683,q_auto,dpr_2.0/brontes/for-talents/find-great-work@1x.jpg"
                alt="for-candidate"
              />
            </div>
          </Grid>
          <Grid item md={6}>
            <div className="for-candidate__right">
              <h3>For Candidate</h3>
              <h1>Find great work</h1>
              <p>
                Meet clients you’re excited to work with and take your career or
                business to new heights.
              </p>
            </div>
            <Divider />
            <div className="for-candidate__right--bottom">
              <Grid container spacing={3}>
                <Grid item md={4}>
                  <p>
                    Find opportunities for every stage of your freelance career
                  </p>
                </Grid>
                <Grid item md={4}>
                  <p>Control when, where, and how you work</p>
                </Grid>
                <Grid item md={4}>
                  <p>Explore different ways to earn</p>
                </Grid>
              </Grid>
            </div>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default ForCandidate;
