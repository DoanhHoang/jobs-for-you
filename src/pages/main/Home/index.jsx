import React from "react";

import "./styles.scss";
import { ClientBanner, TrustBy } from "../../../components";
import Search from "./Search";
import TopCompany from "./TopCompany";
import JoinUs from "./JoinUs";
import ForEnterprise from "./ForEnterprise";
import ForCandidate from "./ForCandidate";

const Home = () => {
  return (
    <>
      <div className="home">
        <ClientBanner />
        <Search />
        <div className="home__trust-by">
          <TrustBy />
        </div>
        <ForEnterprise />
        <ForCandidate />
        {/* <TopCompany /> */}
        <JoinUs />
      </div>
    </>
  );
};

export default Home;
