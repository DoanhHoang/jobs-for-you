import React from "react";
import { TextField, Select, Grid } from "@mui/material";

import "./styles.scss";
import microsoft from "../../../../asset/img/microsoft.png";
import viettel from "../../../../asset/img/viettel.png";
import dxc from "../../../../asset/img/dxc.png";
import fpt from "../../../../asset/img/fpt.png";

const TopCompany = () => {
  return (
    <>
      <div className="top-company">
        <div className="top-company-container">
          <h1>
            top <span>it companies</span>
          </h1>
          <p>top recruitment companies on J4U</p>
          <div className="top-company-container__company">
            <Grid container className="top-company-container__company-card">
              <Grid item md={3}>
                <div style={{ backgroundColor: "#FFF6F0FF" }}>
                  <img src={microsoft} alt="" />
                  <p>3700 Jobs</p>
                </div>
              </Grid>
              <Grid item md={3}>
                <div style={{ backgroundColor: "#F6F0FFFF" }}>
                  <img src={viettel} alt="" />
                  <p>3700 Jobs</p>
                </div>
              </Grid>
              <Grid item md={3}>
                <div style={{ backgroundColor: "#FDF1FDFF" }}>
                  <img src={dxc} alt="" />
                  <p>3700 Jobs</p>
                </div>
              </Grid>
              <Grid item md={3}>
                <div style={{ backgroundColor: "#e4ebe4" }}>
                  <img src={fpt} alt="" />
                  <p>3700 Jobs</p>
                </div>
              </Grid>
            </Grid>
          </div>
        </div>
      </div>
    </>
  );
};

export default TopCompany;
