import "./styles.scss";
import { CustomInput, SelectCustom, Textarea } from "../../../../components/";
import { useForm, FormProvider } from "react-hook-form";
import "./styles.scss";
import { schema } from "./handleForm";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { getJobDetailThunk } from "../../../../store/slices/JobSlice";

import { getCompanyListThunk } from "../../../../store/slices/CompanySlice";

import { useNavigate } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";

import { AddOrUpdateJob } from "./handleForm";

const PostJob = ({ title, id = null, isEdit }) => {
  // const { majorList } = useSelector((state) => state.major);
  // const { provinceList, districtList } = useSelector((state) => state.location);
  // const { jobPosition, status } = useSelector((state) => state.job);
  const [status, setStatus] = useState("idle");
  const { jobDetail } = useSelector((state) => state.job);
  // const { companyList } = useSelector((state) => state.company);

  const methods = useForm({
    resolver: yupResolver(schema),
  });
  const {
    register,
    handleSubmit,
    setValue,
    reset,
    formState: { errors },
  } = methods;

  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    id && dispatch(getJobDetailThunk(id));
    dispatch(getCompanyListThunk());
    // dispatch(getProvinceList());
    // dispatch(getJobPositionList());
  }, [id, dispatch]);

  useEffect(() => {
    setValue("name", !isEdit ? "" : jobDetail.name);
    setValue("description", !isEdit ? "" : jobDetail.description);
    setValue(
      "jobTypes",
      !isEdit
        ? ""
        : jobDetail?.jobTypes?.length > 0
        ? jobDetail.jobTypes[0]?.id
        : ""
    );
    // setValue("company", !isEdit ? "" : jobDetail?.company?.id);
    setValue("amount", !isEdit ? "" : jobDetail.amount ? jobDetail.amount : 0);
    setValue(
      "start",
      !isEdit ? "" : jobDetail?.start?.split("-").reverse().join("-")
    );
    setValue(
      "end",
      !isEdit ? "" : jobDetail?.end?.split("-").reverse().join("-")
    );
    setValue("description", !isEdit ? "" : jobDetail.description);
  }, [jobDetail, isEdit, setValue]);

  // console.log(jobDetail);

  const jobTypeList = [
    {
      id: 1,
      name: "Full time",
    },
    {
      id: 2,
      name: "Part time",
    },
    {
      id: 3,
      name: "Remote",
    },
  ];

  const positionList = [
    {
      id: 1,
      name: "Front-end",
    },
    {
      id: 2,
      name: "Back-end",
    },
    {
      id: 3,
      name: "Business Analysis",
    },
    {
      id: 4,
      name: "Tester/QC",
    },
    {
      id: 5,
      name: "Project Management",
    },
  ];

  const onSubmit = (data) => {
    console.log(data);
    AddOrUpdateJob(!isEdit, data, id, reset, navigate, setStatus);
  };

  return (
    <FormProvider {...methods}>
      <form
        className="postJob-form"
        autoComplete="off"
        encType="multipart/form-data"
      >
        <div className="hr-post__container">
          <div className="form__container">
            <div className="hr-post__form">
              <div className="hr-post__heading">
                <h2>{title}</h2>
              </div>
              <p className="title-requirement">
                (<span className="field-requirment"> * </span>) Require
              </p>
              <div className="hr-post-title">
                <CustomInput
                  title="Name"
                  name="name"
                  type="text"
                  placeholder="Eg: Fresher UI-UX"
                  register={register}
                  errors={errors}
                  defaultValue={jobDetail?.name}
                />
              </div>
              <div className="row-2-col">
                <div className="hr-post__select">
                  <SelectCustom
                    id="jobTypes"
                    label="Type"
                    placeholder="Please choose option"
                    options={jobTypeList}
                    register={register}
                  >
                    {errors.jobType?.message}
                  </SelectCustom>
                </div>
                {/* <div className="hr-post__select">
                  <SelectCustom
                    id="company"
                    label="Company"
                    placeholder="please choose option"
                    options={companyList}
                    register={register}
                  >
                    {errors.company?.message}
                  </SelectCustom>
                </div> */}
              </div>
              <div className="row-2-col">
                <div className="hr-post__select">
                  <SelectCustom
                    id="jobPosition"
                    label="Position"
                    placeholder="please choose option"
                    options={positionList}
                    register={register}
                  >
                    {errors.jobPosition?.message}
                  </SelectCustom>
                </div>
                <CustomInput
                  title="Amount"
                  name="amount"
                  type="number"
                  placeholder="Type amount"
                  register={register}
                  defaultValue={jobDetail?.amount}
                  errors={errors}
                />
              </div>
              <div className="row-2-col">
                <CustomInput
                  title="Time start"
                  name="start"
                  type="date"
                  placeholder=""
                  register={register}
                  defaultValue={jobDetail?.start}
                  errors={errors}
                />
                <CustomInput
                  title="Time end"
                  name="end"
                  type="date"
                  placeholder=""
                  register={register}
                  defaultValue={jobDetail?.end}
                  errors={errors}
                />
              </div>
              {/* <div className={"row-2-col"}>
                <SelectCustom
                  id="province"
                  label="Province/City"
                  placeholder="please choose option"
                  dispatch={dispatch}
                  // action={getDistrictList}
                  options={jobTypeList}
                  register={register}
                >
                  {errors.province?.message}
                </SelectCustom>
                <SelectCustom
                  id="district"
                  label="District"
                  placeholder="please choose option"
                  options={jobTypeList}
                  register={register}
                >
                  {errors.district?.message}
                </SelectCustom>
              </div> */}
              <div className={"hr-post__select"}>
                <CustomInput
                  title="Address"
                  name="address"
                  type="text"
                  placeholder="Vd. 254, Dương Đình Hội"
                  register={register}
                  defaultValue={jobDetail?.address}
                  errors={errors}
                />
              </div>
              <div className="hr-post__textarea">
                <Textarea
                  label="Description"
                  id="description"
                  placeholder="Type Job Description"
                  register={register}
                  setValue={setValue}
                  check={false}
                  inputValue={isEdit ? jobDetail?.description : ""}
                  status={status}
                >
                  {errors.description?.message}
                </Textarea>
              </div>
              <div className="hr-post__textarea">
                <Textarea
                  label="Requirement"
                  id="requirement"
                  placeholder="Type Job Requirement"
                  setValue={setValue}
                  register={register}
                  check={false}
                  inputValue={isEdit ? jobDetail?.requirement : ""}
                  status={status}
                >
                  {errors.requirement?.message}
                </Textarea>
              </div>
              <div className="hr-post__textarea">
                <Textarea
                  label="Orther Info"
                  id="ortherInfo"
                  placeholder="Type Orther Info"
                  register={register}
                  setValue={setValue}
                  check={false}
                  inputValue={isEdit ? jobDetail?.ortherInfo : ""}
                  status={status}
                >
                  {errors.ortherInfo?.message}
                </Textarea>
              </div>
              <div className="hr-post__salary">
                <div className="hr-post__salary-lable">
                  Salary <span className="field-requirment"> *</span>
                </div>
                <div className="hr-post__salary-range">
                  <CustomInput
                    name="salaryMin"
                    type="number"
                    placeholder="Nhập số tiền tối thiểu"
                    register={register}
                    requirementField={false}
                    errors={errors}
                  />
                  <CustomInput
                    name="salaryMax"
                    type="number"
                    placeholder="Nhập số tiền tối đa"
                    register={register}
                    requirementField={false}
                    errors={errors}
                  />
                </div>
              </div>
              <div className="hr-post__action">
                <button onClick={handleSubmit(onSubmit)}>Submit</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </FormProvider>
  );
};

export default PostJob;
