import React from "react";

import "./styles.scss";
import Header from "./Header";
import Main from "./Main";
import ListJob from "./ListJob";

const CompanyProfile = () => {
  return (
    <>
      <div className="company-profile">
        <Header />
        <Main />
        <ListJob />
      </div>
    </>
  );
};

export default CompanyProfile;
