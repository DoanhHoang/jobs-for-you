import React, { useState, useEffect } from "react";
import { Tooltip, IconButton } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useSelector } from "react-redux";

import { JobList } from "../../../../../components";
import companyAPI from "../../../../../config/api/company/companyAPI";

const { getJobByCompanyId } = companyAPI;

const ListJob = () => {
  const [listJob, setListJob] = useState([]);
  const { roleDetail } = useSelector((state) => state.user);

  useEffect(() => {
    getJobByCompanyId(roleDetail?.id).then((res) => {
      setListJob(res.data);
    });
  }, []);

  return (
    <>
      <div className="profile__list-job box">
        <div className="box__header">
          <div className="box__header-left">
            <h3>List Jobs</h3>
            {/* <p>Endorsements from past clients</p> */}
          </div>
          <Tooltip title="Apply more jobs">
            <IconButton>
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <JobList list={listJob} />
      </div>
    </>
  );
};

export default ListJob;
