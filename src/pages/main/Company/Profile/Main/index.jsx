import React, { useState } from "react";
import { Grid, Divider } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

import userAvatar from "../../../../../asset/img/admin-user.png";
import AddOrUpdateUser from "../../../../../components/CustomModal/AddOrUpdateUser/AddOrUpdateUser";
import UpdateCompanyProfile from "../../../../../components/CustomModal/UpdateCompanyProfile";

const Main = () => {
  const [open, setOpen] = useState(false);
  const [openCompany, setOpenCompany] = useState(false);
  const { userDetail, roleDetail } = useSelector((state) => state.user);

  const navigate = useNavigate();

  return (
    <>
      <div className="company-profile__main box">
        <div className="company-profile__main--header">
          <Grid container>
            <Grid item md={8}>
              <div className="company-profile__main--header-avatar">
                <img src={userAvatar} alt="avatar" />
                <div>
                  <h1>{roleDetail.name}</h1>
                  <p>Active</p>
                </div>
              </div>
            </Grid>
            <Grid item md={4}>
              <div className="profile__main--header-action">
                <button
                  type="button"
                  onClick={() => navigate("/pass/change-password")}
                >
                  Change Password
                </button>
                <div className="company-profile__main--header-action">
                  <button type="button" onClick={() => setOpenCompany(true)}>
                    Edit Company
                  </button>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
        <Divider />
        <div className="company-profile__main--info">
          <h3>Description</h3>
          <p>{roleDetail.description}</p>
          <Grid container spacing={3}>
            <Grid item md={6}>
              <div className="company-profile__main--info-text">
                <h5>Email: </h5>
                <p>{roleDetail.email}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="company-profile__main--info-text">
                <h5>Phone: </h5>
                <p>{roleDetail.phone}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="company-profile__main--info-text">
                <h5>Website: </h5>
                <p>{roleDetail.website}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="company-profile__main--info-text">
                <h5>Tax: </h5>
                <p>{roleDetail.tax}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="company-profile__main--info-text">
                <h5>Address: </h5>
                <p>
                  {roleDetail?.locations?.length > 0 &&
                    roleDetail?.locations[0]?.address}
                  , {}
                </p>
              </div>
            </Grid>
          </Grid>
          <h3>User</h3>
          <Grid container spacing={3}>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>First Name: </h5>
                <p>{userDetail.firstName}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Last Name: </h5>
                <p>{userDetail.lastName}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Username: </h5>
                <p>{userDetail.username}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Gender: </h5>
                <p>{userDetail.gender ? "Male" : "Female"}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Email: </h5>
                <p>{userDetail.email}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Phone: </h5>
                <p>{userDetail.phone}</p>
              </div>
            </Grid>
          </Grid>
          <div className="company-profile__main--header-action">
            <button type="button" onClick={() => setOpen(true)}>
              Edit User
            </button>
          </div>
        </div>
      </div>
      <AddOrUpdateUser open={open} setOpen={setOpen} id={userDetail.id} />
      <UpdateCompanyProfile
        open={openCompany}
        setOpen={setOpenCompany}
        id={roleDetail.id}
      />
    </>
  );
};

export default Main;
