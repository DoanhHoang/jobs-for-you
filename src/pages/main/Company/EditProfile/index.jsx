import React, { useState, useEffect } from "react";
import { Grid, Avatar } from "@mui/material";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";

import "./styles.scss";
import cameraLogo from "../../../../asset/img/camera.png";
import { CustomInput, SelectCustom } from "../../../../components";
import { schema } from "./script";
import globalAPI from "../../../../config/api/globalAPI";

const EditCompanyProfile = () => {
  const [visibility, setVisibility] = useState(false);
  const [image, setImage] = useState(cameraLogo);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  // show preview image
  const showPreviewImage = (e) => {
    if (e.target.files && e.target.files[0]) {
      let imageFile = e.target.files[0];
      const reader = new FileReader();
      reader.onload = (x) => {
        setImage(x.target.result);
      };
      reader.readAsDataURL(imageFile);
    }
  };

  const onChangeVisibility = () => {
    setVisibility(!visibility);
  };

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <>
      <form
        className="edit-company box"
        autoComplete="off"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Grid container spacing={3}>
          <Grid item md={12}>
            <h3>Logo</h3>
            <label htmlFor="avatar">
              <div className="edit-company__logo">
                <Avatar
                  src={image}
                  variant="rounded"
                  alt="cannidate-logo"
                  className="edit-company__logo-avatar"
                />
              </div>
            </label>
            <input
              className="edit-company__file-input"
              id="avatar"
              type="file"
              name="file"
              {...register("file")}
              onChange={showPreviewImage}
            />
            <p className="edit-company__error">{errors.avatar?.message}</p>
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="name"
              register={register}
              title="Name"
              placeholder="Name"
              errors={errors}
            />
            <CustomInput
              name="email"
              register={register}
              title="Email"
              placeholder="Email"
              errors={errors}
            />
            <CustomInput
              name="phone"
              register={register}
              title="Phone"
              placeholder="Phone"
              errors={errors}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="website"
              register={register}
              title="Website"
              placeholder="Website"
              errors={errors}
            />
            <CustomInput
              name="tax"
              register={register}
              title="Tax"
              placeholder="Tax"
              errors={errors}
            />
          </Grid>
          <Grid item md={12}>
            <CustomInput
              title="Description"
              register={register}
              name="description"
              multiline={true}
              minRows={4}
            />
          </Grid>

          <Grid item md={12}>
            <h3>User Info</h3>
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="username"
              register={register}
              errors={errors}
              placeholder="Username"
              title="Username"
            />
            <CustomInput
              name="password"
              register={register}
              type={!visibility && "password"}
              errors={errors}
              placeholder="Password"
              title="Password"
              endIcon={
                !visibility ? (
                  <VisibilityOff fontSize="small" />
                ) : (
                  <Visibility fontSize="small" />
                )
              }
              onClick={onChangeVisibility}
            />
            <CustomInput
              name="phone"
              register={register}
              errors={errors}
              placeholder="Phone"
              title="Phone"
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="email"
              register={register}
              errors={errors}
              placeholder="Email"
              title="Email"
            />
            <CustomInput
              name="comfirmPassword"
              register={register}
              type={!visibility && "password"}
              errors={errors}
              placeholder="Comfirm Password"
              title="Comfirm Password"
              endIcon={
                !visibility ? (
                  <VisibilityOff fontSize="small" />
                ) : (
                  <Visibility fontSize="small" />
                )
              }
              onClick={onChangeVisibility}
            />
          </Grid>
        </Grid>
      </form>
      <div className="edit-company__foot">
        <button type="button" onClick={handleSubmit(onSubmit)}>
          Sign Up
        </button>
      </div>
    </>
  );
};

export default EditCompanyProfile;
