import React, { useState, useEffect, useContext } from "react";
import { Divider, Grid } from "@mui/material";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import PlaceOutlinedIcon from "@mui/icons-material/PlaceOutlined";
import CalendarMonthOutlinedIcon from "@mui/icons-material/CalendarMonthOutlined";
import FormatListBulletedOutlinedIcon from "@mui/icons-material/FormatListBulletedOutlined";
import { toast } from "react-toastify";

import "./styles.scss";
import { JobList, FreelancerList } from "../../../components";
// import userLogo from "../../../asset/img/admin-user.png";
import SignInNotification from "../../../components/CustomModal/SignInNotification";
import {
  getJobDetailThunk,
  // getJobByCompanyIdThunk,
  getJobsApplyByCandidateIdThunk,
} from "../../../store/slices/JobSlice";

import jobAPI from "../../../config/api/job/jobAPI";
import companyAPI from "../../../config/api/company/companyAPI";
import { AuthenticationContext } from "../../../App";
import { checkRole, checkJobApply } from "../../../utils";
import { ROLE_NAME } from "../../../config/constant/roleNameConstant";

const { applyJob } = jobAPI;
const { getCandidatesApplyJobByJobId } = companyAPI;

const JobDetail = ({ jobId }) => {
  const { auth } = useContext(AuthenticationContext);
  const [containerWidth, setContainerWidth] = useState(false);
  const [open, setOpen] = useState(false);
  const [checkApply, setCheckApply] = useState(false);
  const [listCandidatesApply, setListCandidatesApply] = useState([]);

  const { jobDetail, jobList, jobApplyList } = useSelector(
    (state) => state.job
  );
  const { roleDetail, userDetail } = useSelector((state) => state.user);

  const { id } = useParams();
  const dispatch = useDispatch();

  useEffect(() => {
    id ? setContainerWidth(true) : setContainerWidth(false);
  }, [id]);

  useEffect(() => {
    id && dispatch(getJobDetailThunk(id));
    jobId && dispatch(getJobDetailThunk(jobId));
    roleDetail.id && dispatch(getJobsApplyByCandidateIdThunk(roleDetail.id));
    getCandidatesApplyJobByJobId(id || jobId).then((res) => {
      setListCandidatesApply(res);
    });
  }, [dispatch, id, jobId, roleDetail.id]);

  useEffect(() => {
    console.log(jobApplyList);
    if (jobApplyList && jobApplyList.length > 0) {
      setCheckApply(checkJobApply(jobApplyList, id || jobId));
    }
  }, [dispatch, id, jobId, jobApplyList]);

  const handleApplyJob = () => {
    if (auth) {
      applyJob(jobDetail.id, roleDetail.id)
        .then(() => {
          toast.success("Apply successfully");
          console.log(id, jobId);
          dispatch(getJobsApplyByCandidateIdThunk(roleDetail.id));
          id && dispatch(getJobDetailThunk(id));
          jobId && dispatch(getJobDetailThunk(jobId));
        })
        .catch((err) => {
          toast.error(err.response.data.message);
        });
    } else {
      setOpen(true);
    }
  };

  return (
    <>
      <div className={"job-detail " + (containerWidth ? "job-container" : "")}>
        <div className="job-detail__header box">
          <Grid container>
            <Grid item md={10}>
              <h1>{jobDetail.name}</h1>
              <div className="job-detail__info">
                <div className="job-detail__info--address">
                  <PlaceOutlinedIcon />
                  <p>Ho Chi Minh City</p>
                </div>
                <div className="job-detail__info--type">
                  <FormatListBulletedOutlinedIcon />
                  <p>
                    {jobDetail?.jobTypes?.length > 0 &&
                      jobDetail?.jobTypes[0]?.name}
                  </p>
                </div>
                <div className="job-detail__info--date">
                  <CalendarMonthOutlinedIcon />
                  <p>
                    {jobDetail.start} - {jobDetail.end}
                  </p>
                </div>
              </div>
            </Grid>
            <Grid item md={2}>
              <div className="job-detail__header--right">
                <button
                  type="button"
                  onClick={handleApplyJob}
                  // className={checkApply ? "btn-disabled" : null}
                  // disabled={checkApply}
                >
                  {checkApply ? "Aplied" : "Apply Job"}
                </button>
              </div>
            </Grid>
          </Grid>
        </div>

        <div className="job-detail__main box">
          <div className="job-detail__main--header">
            <h3>Salary</h3>
            <p>10K-20K</p>
            <h3>Amount</h3>
            <p>{jobDetail.amount}</p>
          </div>
          {/* <h3>Description</h3> */}
          <div classsName="job-detail__main--quill">
            <div
              dangerouslySetInnerHTML={{ __html: jobDetail.description }}
            ></div>
            {/* <h3>Requirement</h3> */}
            <div
              dangerouslySetInnerHTML={{ __html: jobDetail.requirement }}
            ></div>
            {/* <h3>Other Info</h3> */}
            <div
              dangerouslySetInnerHTML={{ __html: jobDetail.ortherInfo }}
            ></div>
          </div>
        </div>
        {checkRole(userDetail.role, ROLE_NAME.COMPANY) && (
          <FreelancerList listFreelancer={listCandidatesApply} />
        )}

        <Divider>More Jobs</Divider>
        <JobList list={jobList} />
      </div>
      <SignInNotification open={open} setOpen={setOpen} />
    </>
  );
};

export default JobDetail;
