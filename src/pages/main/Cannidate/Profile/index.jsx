import React from "react";
import { useParams } from "react-router-dom";

import { PreviewPdf } from "../../../../components";
import { Profile } from "../../../../containers";
import Testimonial from "./Testimonial";
import Certification from "./Certification";
import ListJobApply from "./ListJobApply";
import ListJobLike from "./ListJobLike";

const CannidateProfile = () => {
  const { id } = useParams();
  return (
    <>
      <div className="cannidate-profile">
        <Profile>
          <PreviewPdf />
          {!id && (
            <>
              <ListJobApply />
              <ListJobLike />
              <Testimonial />
              <Certification />
            </>
          )}
        </Profile>
      </div>
    </>
  );
};

export default CannidateProfile;
