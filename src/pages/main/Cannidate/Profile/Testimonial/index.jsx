import React from "react";
import { IconButton, Tooltip } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";

const Testimonial = () => {
  return (
    <>
      <div className="profile__testimonial box">
        <div className="box__header">
          <div className="box__header-left">
            <h3>Testimonials</h3>
            <p>Endorsements from past clients</p>
          </div>
          <Tooltip title="add a new testimonial">
            <IconButton>
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <div className="profile__testimonial--main">
          <img src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/100/null/external-testimonials-online-money-service-flaticons-lineal-color-flat-icons-2.png" />
          <p>
            Showcasing client testimonials can strengthen your profile. (+5%)
          </p>
          <a href="#">Request a testimonial</a>
        </div>
      </div>
    </>
  );
};

export default Testimonial;
