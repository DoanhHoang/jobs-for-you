import React from "react";
import { IconButton, Tooltip } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";

const Certification = () => {
  return (
    <>
      <div className="profile__certification box">
        <div className="box__header">
          <div className="box__header-left">
            <h3>Certifications</h3>
          </div>
          <Tooltip title="add a new certification">
            <IconButton>
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <div className="profile__certification--main">
          <img src="https://img.icons8.com/external-smashingstocks-thin-outline-color-smashing-stocks/100/null/external-certificate-education-smashingstocks-thin-outline-color-smashing-stocks.png" />
          <p>
            Listing your certifications can help prove your specific knowledge
            or abilities.
          </p>
          <p>(+10%) You can add them manually or import them from Credly.</p>

          <a href="#">Import from Credly</a>
        </div>
      </div>
    </>
  );
};

export default Certification;
