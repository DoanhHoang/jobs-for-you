import React, { useEffect } from "react";
import { Tooltip, IconButton } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useSelector, useDispatch } from "react-redux";

import { JobList } from "../../../../../components";
import { getJobsApplyByCandidateIdThunk } from "../../../../../store/slices/JobSlice";

const ListJobApply = () => {
  const { roleDetail } = useSelector((state) => state.user);
  const { jobApplyList } = useSelector((state) => state.job);

  const dispatch = useDispatch();

  useEffect(() => {
    if (roleDetail.id) {
      dispatch(getJobsApplyByCandidateIdThunk(roleDetail.id));
    }
  }, [dispatch, roleDetail]);

  return (
    <>
      <div className="profile__list-job box">
        <div className="box__header">
          <div className="box__header-left">
            <h3>List Apply Jobs</h3>
            <p>Endorsements from past clients</p>
          </div>
          <Tooltip title="Apply more jobs">
            <IconButton>
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <JobList list={jobApplyList} />
      </div>
    </>
  );
};

export default ListJobApply;
