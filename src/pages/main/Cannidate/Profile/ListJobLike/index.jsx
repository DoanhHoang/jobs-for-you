import React, { useEffect } from "react";
import { Tooltip, IconButton } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { JobList } from "../../../../../components";
import { useSelector, useDispatch } from "react-redux";

import { getJobsLikeByCandidateIdThunk } from "../../../../../store/slices/JobSlice";

const ListJobLike = () => {
  const { roleDetail } = useSelector((state) => state.user);
  const { jobLikeList } = useSelector((state) => state.job);

  const dispatch = useDispatch();

  useEffect(() => {
    if (roleDetail.id) {
      dispatch(getJobsLikeByCandidateIdThunk(roleDetail.id));
    }
  }, [dispatch, roleDetail]);

  return (
    <>
      <div className="profile__list-job box">
        <div className="box__header">
          <div className="box__header-left">
            <h3>List Like Jobs</h3>
            <p>Endorsements from past clients</p>
          </div>
          <Tooltip title="Apply more jobs">
            <IconButton>
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <JobList list={jobLikeList} />
      </div>
    </>
  );
};

export default ListJobLike;
