import * as yup from "yup";

export const schema = yup.object({
  username: yup.string().required("Username is a required field."),
  password: yup.string().required("Password is a required field."),
  comfirmPassword: yup
    .string()
    .required("Comfirm password is a required field."),
  gender: yup.boolean(),
  phone: yup.string(),
  firstName: yup.string().required("First name is a required field."),
  lastName: yup.string().required("Last name is a required field."),
  email: yup.string(),
  major: yup.string(),
  cv: yup
    .mixed()
    // .optional()
    // .required("* please choose image")
    .test("fileSize", "* Uploaded file is too big!", (value) => {
      if (value) {
        return value[0]?.size ? value[0].size <= 51200 * 1024 : true;
      } else {
        return yup.mixed().notRequired();
      }
    })
    .test("type", " * Allowed *.pdf", (value) => {
      if (value) {
        return value[0]?.type ? value[0]?.type === "application/pdf" : true;
      } else {
        return yup.mixed().notRequired();
      }
    }),
  avatar: yup
    .mixed()
    // .optional()
    // .required("* please choose image")
    .test("fileSize", "* Uploaded file is too big!", (value) => {
      if (value) {
        return value[0]?.size ? value[0].size <= 51200 * 1024 : true;
      } else {
        return yup.mixed().notRequired();
      }
    })
    .test("type", " * Allowed *.jpeg, *.jpg, *.png, *.gif.", (value) => {
      if (value) {
        return value[0]?.type
          ? value[0]?.type === "image/jpeg" ||
              value[0].type === "image/png" ||
              value[0].type === "image/jpg" ||
              value[0].type === "image/gif"
          : true;
      } else {
        return yup.mixed().notRequired();
      }
    }),
});
