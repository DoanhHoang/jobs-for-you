import React, { useState, useEffect } from "react";
import { Grid, Avatar } from "@mui/material";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import PermIdentityIcon from "@mui/icons-material/PermIdentity";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import BadgeOutlinedIcon from "@mui/icons-material/BadgeOutlined";
import ContactPhoneOutlinedIcon from "@mui/icons-material/ContactPhoneOutlined";

import "./styles.scss";
import cameraLogo from "../../../../asset/img/camera.png";
import cvLogo from "../../../../asset/img/cv.png";
import { CustomInput, SelectCustom } from "../../../../components";
import { schema } from "./script";
import globalAPI from "../../../../config/api/globalAPI";

const { getAllMajors } = globalAPI;

const EditCandidateProfile = () => {
  const [visibility, setVisibility] = useState(false);
  const [image, setImage] = useState(cameraLogo);
  const [cv, setCv] = useState("");
  const [listMajors, setListMajors] = useState([]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  useEffect(() => {
    getAllMajors().then((res) => {
      setListMajors(res);
    });
  }, []);

  // show preview image
  const showPreviewImage = (e) => {
    if (e.target.files && e.target.files[0]) {
      let imageFile = e.target.files[0];
      const reader = new FileReader();
      reader.onload = (x) => {
        setImage(x.target.result);
      };
      reader.readAsDataURL(imageFile);
    }
  };
  // show preview cv
  const showPreviewCV = (e) => {
    if (e.target.files && e.target.files[0]) {
      let cvFile = e.target.files[0];
      setCv(cvFile.name);
    }
  };

  const onChangeVisibility = () => {
    setVisibility(!visibility);
  };

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <>
      <form
        className="candidate-form box"
        autoComplete="off"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Grid container spacing={3}>
          <Grid item md={12}>
            <h3>Avatar</h3>
            <label htmlFor="avatar">
              <div className="candidate-form__logo">
                <Avatar
                  src={image}
                  variant="rounded"
                  alt="cannidate-logo"
                  className="candidate-form__logo-avatar"
                />
              </div>
            </label>
            <input
              className="candidate-form__file-input"
              id="avatar"
              type="file"
              name="avatar"
              {...register("avatar")}
              onChange={showPreviewImage}
            />
            <p className="candidate-form__error">{errors.avatar?.message}</p>
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="firstName"
              register={register}
              errors={errors}
              placeholder="First Name"
              title="First Name"
              startIcon={<BadgeOutlinedIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="lastName"
              register={register}
              errors={errors}
              placeholder="Last Name"
              title="Last Name"
              startIcon={<BadgeOutlinedIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="username"
              register={register}
              errors={errors}
              placeholder="Username"
              title="Username"
              startIcon={<PermIdentityIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="email"
              register={register}
              errors={errors}
              placeholder="Email"
              title="Email"
              startIcon={<MailOutlineIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="password"
              register={register}
              type={!visibility && "password"}
              errors={errors}
              placeholder="Password"
              title="Password"
              startIcon={<LockOpenIcon />}
              endIcon={
                !visibility ? (
                  <VisibilityOff fontSize="small" />
                ) : (
                  <Visibility fontSize="small" />
                )
              }
              onClick={onChangeVisibility}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="comfirmPassword"
              register={register}
              type={!visibility && "password"}
              errors={errors}
              placeholder="Comfirm Password"
              title="Comfirm Password"
              startIcon={<LockOpenIcon />}
              endIcon={
                !visibility ? (
                  <VisibilityOff fontSize="small" />
                ) : (
                  <Visibility fontSize="small" />
                )
              }
              onClick={onChangeVisibility}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="phone"
              register={register}
              errors={errors}
              placeholder="Phone"
              title="Phone"
              startIcon={<ContactPhoneOutlinedIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <label htmlFor="cv">
              <div className="candidate-form__cv">
                <h3>CV</h3>
                <div className="candidate-form__cv-file">
                  <img src={cvLogo} alt="cv-logo" />
                  <p>{cv || "choose file"}</p>
                </div>
              </div>
            </label>
            <input
              className="candidate-form__file-input"
              id="cv"
              type="file"
              name="cv"
              {...register("cv")}
              onChange={showPreviewCV}
            />
            <p className="candidate-form__error">{errors.cv?.message}</p>
          </Grid>
          <Grid item md={6}>
            <SelectCustom
              id="major"
              label="Major"
              placeholder="Please choose option"
              options={listMajors}
              register={register}
            >
              {errors.major?.message}
            </SelectCustom>
          </Grid>
          <Grid item md={6}>
            <SelectCustom
              id="gender"
              label="Gender"
              placeholder="Please choose option"
              options={[
                {
                  id: true,
                  name: "Male",
                },
                {
                  id: false,
                  name: "Female",
                },
              ]}
              register={register}
            >
              {errors.major?.message}
            </SelectCustom>
          </Grid>
        </Grid>
      </form>
      <div className="candidate-form__foot">
        <button type="button" onClick={handleSubmit(onSubmit)}>
          Save
        </button>
      </div>
    </>
  );
};

export default EditCandidateProfile;
