import React, { useState, useEffect } from "react";
import { Tooltip, IconButton } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useSelector } from "react-redux";

import { DemandList } from "../../../../../components";
import universityAPI from "../../../../../config/api/university/universityAPI";

const { getDemandsByUniversityId } = universityAPI;

const ListDemand = () => {
  const [listDemand, setListDemand] = useState([]);
  const { roleDetail } = useSelector((state) => state.user);

  useEffect(() => {
    getDemandsByUniversityId(roleDetail?.id).then((res) => {
      setListDemand(res.data.data);
    });
  }, [roleDetail?.id]);

  return (
    <>
      <div className="profile__list-job box">
        <div className="box__header">
          <div className="box__header-left">
            <h3>List Demand</h3>
            {/* <p>Endorsements from past clients</p> */}
          </div>
          <Tooltip title="Post more demands">
            <IconButton>
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <DemandList list={listDemand} />
      </div>
    </>
  );
};

export default ListDemand;
