import React from "react";

import "./styles.scss";
import Header from "./Header";
import Main from "./Main";
import ListDemand from "./ListDemand";

const UniversityProfile = () => {
  return (
    <>
      <div className="university-profile">
        <Header />
        <Main />
        <ListDemand />
      </div>
    </>
  );
};

export default UniversityProfile;
