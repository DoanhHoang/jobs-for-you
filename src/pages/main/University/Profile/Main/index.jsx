import React, { useState } from "react";
import { Grid, Divider } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

import userAvatar from "../../../../../asset/img/admin-user.png";
import AddOrUpdateUser from "../../../../../components/CustomModal/AddOrUpdateUser/AddOrUpdateUser";
import UpdateUniversityProfile from "../../../../../components/CustomModal/UpdateUniversityProfile";

const Main = () => {
  const [open, setOpen] = useState(false);
  const [openUniversity, setOpenUniversity] = useState(false);
  const { userDetail, roleDetail } = useSelector((state) => state.user);

  const navigate = useNavigate();

  return (
    <>
      <div className="university-profile__main box">
        <div className="university-profile__main--header">
          <Grid container>
            <Grid item md={8}>
              <div className="university-profile__main--header-avatar">
                <img src={userAvatar} alt="avatar" />
                <div>
                  <h1>{roleDetail.name}</h1>
                  <p>Active</p>
                </div>
              </div>
            </Grid>
            <Grid item md={4}>
              <div className="profile__main--header-action">
                <button
                  type="button"
                  onClick={() => navigate("/pass/change-password")}
                >
                  Change Password
                </button>
                <div className="university-profile__main--header-action">
                  <button type="button" onClick={() => setOpenUniversity(true)}>
                    Edit University
                  </button>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
        <Divider />
        <div className="university-profile__main--info">
          <h3>Description</h3>
          <p>{roleDetail.description}</p>
          <Grid container spacing={3}>
            <Grid item md={6}>
              <div className="university-profile__main--info-text">
                <h5>Email: </h5>
                <p>{roleDetail.email}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="university-profile__main--info-text">
                <h5>Phone: </h5>
                <p>{roleDetail.phone}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="university-profile__main--info-text">
                <h5>Website: </h5>
                <p>{roleDetail.website}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="university-profile__main--info-text">
                <h5>Short Name: </h5>
                <p>{roleDetail.shortName}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="university-profile__main--info-text">
                <h5>Address: </h5>
                <p>
                  {roleDetail?.locations?.length > 0 &&
                    roleDetail?.locations[0]?.address}
                  , {}
                </p>
              </div>
            </Grid>
          </Grid>
          <h3>User</h3>
          <Grid container spacing={3}>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>First Name: </h5>
                <p>{userDetail.firstName}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Last Name: </h5>
                <p>{userDetail.lastName}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Username: </h5>
                <p>{userDetail.username}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Gender: </h5>
                <p>{userDetail.gender ? "Male" : "Female"}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Email: </h5>
                <p>{userDetail.email}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Phone: </h5>
                <p>{userDetail.phone}</p>
              </div>
            </Grid>
          </Grid>
          <div className="university-profile__main--header-action">
            <button type="button" onClick={() => setOpen(true)}>
              Edit User
            </button>
          </div>
        </div>
      </div>
      <AddOrUpdateUser open={open} setOpen={setOpen} id={userDetail.id} />
      <UpdateUniversityProfile
        open={openUniversity}
        setOpen={setOpenUniversity}
        id={roleDetail.id}
      />
    </>
  );
};

export default Main;
