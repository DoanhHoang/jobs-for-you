import "./styles.scss";
import { CustomInput, SelectCustom, Textarea } from "../../../../components";
import { useForm, FormProvider } from "react-hook-form";
import "./styles.scss";
import { schema } from "./handleForm";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { getJobDetailThunk } from "../../../../store/slices/JobSlice";

import { getUniversityListThunk } from "../../../../store/slices/UniversitySlice";

import { useNavigate } from "react-router-dom";
import { yupResolver } from "@hookform/resolvers/yup";

import { AddOrUpdateJob } from "./handleForm";

const PostDemand = ({ title, id = null, isEdit }) => {
  // const { majorList } = useSelector((state) => state.major);
  // const { provinceList, districtList } = useSelector((state) => state.location);
  // const { jobPosition, status } = useSelector((state) => state.job);
  const [status, setStatus] = useState("idle");
  const { roleDetail } = useSelector((state) => state.user);
  const { demandDetail } = useSelector((state) => state.demand);
  const { universityList } = useSelector((state) => state.university);

  const methods = useForm({
    resolver: yupResolver(schema),
  });
  const {
    register,
    handleSubmit,
    setValue,
    reset,
    formState: { errors },
  } = methods;

  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    id && dispatch(getJobDetailThunk(id));
    dispatch(getUniversityListThunk({ no: 0, limit: 10000 }));
    // dispatch(getProvinceList());
    // dispatch(getJobPositionList());
  }, [id, dispatch]);

  // useEffect(() => {
  //   setValue("name", !isEdit ? "" : jobDetail.name);
  //   setValue("description", !isEdit ? "" : jobDetail.description);
  //   setValue(
  //     "jobTypes",
  //     !isEdit
  //       ? ""
  //       : jobDetail?.jobTypes?.length > 0
  //       ? jobDetail.jobTypes[0]?.id
  //       : ""
  //   );
  //   setValue("company", !isEdit ? "" : jobDetail?.company?.id);
  //   setValue("amount", !isEdit ? "" : jobDetail.amount ? jobDetail.amount : 0);
  //   setValue(
  //     "start",
  //     !isEdit ? "" : jobDetail?.start?.split("-").reverse().join("-")
  //   );
  //   setValue(
  //     "end",
  //     !isEdit ? "" : jobDetail?.end?.split("-").reverse().join("-")
  //   );
  //   setValue("description", !isEdit ? "" : jobDetail.description);
  // }, [jobDetail, isEdit, setValue]);

  const onSubmit = (data) => {
    console.log(data);
    AddOrUpdateJob(true, data, id, reset, navigate, setStatus, roleDetail?.id);
  };

  return (
    <FormProvider {...methods}>
      <form
        className="postJob-form"
        autoComplete="off"
        encType="multipart/form-data"
      >
        <div className="hr-post__container">
          <div className="form__container">
            <div className="hr-post__form">
              <div className="hr-post__heading">
                <h2>{title}</h2>
              </div>
              <p className="title-requirement">
                (<span className="field-requirment"> * </span>) Require
              </p>
              <div className="row-2-col">
                <CustomInput
                  title="Name"
                  name="name"
                  type="text"
                  placeholder="Eg: Fresher UI-UX"
                  register={register}
                  errors={errors}
                />
                <CustomInput
                  title="Amount"
                  name="amount"
                  type="number"
                  placeholder="Type amount"
                  register={register}
                  errors={errors}
                />
              </div>
              <div className="row-2-col">
                <CustomInput
                  title="Time start"
                  name="startDate"
                  type="date"
                  placeholder=""
                  register={register}
                  errors={errors}
                />
                <CustomInput
                  title="Time end"
                  name="endDate"
                  type="date"
                  placeholder=""
                  register={register}
                  errors={errors}
                />
              </div>
              <div className="hr-post__textarea">
                <Textarea
                  label="Description"
                  id="description"
                  placeholder="Type Demand Description"
                  register={register}
                  setValue={setValue}
                  check={false}
                  status={status}
                >
                  {errors.description?.message}
                </Textarea>
              </div>
              <div className="hr-post__textarea">
                <Textarea
                  label="Requirement"
                  id="requirement"
                  placeholder="Type Demand Requirement"
                  setValue={setValue}
                  register={register}
                  check={false}
                  status={status}
                >
                  {errors.requirement?.message}
                </Textarea>
              </div>
              <div className="hr-post__textarea">
                <Textarea
                  label="Orther Info"
                  id="ortherInfo"
                  placeholder="Type Orther Info"
                  register={register}
                  setValue={setValue}
                  check={false}
                  status={status}
                >
                  {errors.ortherInfo?.message}
                </Textarea>
              </div>
              <div className="hr-post__action">
                <button onClick={handleSubmit(onSubmit)}>Submit</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </FormProvider>
  );
};

export default PostDemand;
