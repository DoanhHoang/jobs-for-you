import React, { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { CircularProgress } from "@mui/material";
import PermIdentityIcon from "@mui/icons-material/PermIdentity";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

import "./styles.scss";
import CustomInput from "../../../components/CustomInput";
import globalAPI from "../../../config/api/globalAPI";

import { AuthenticationContext } from "../../../App";
import { ROLE_NAME } from "../../../config/constant/roleNameConstant";
import { checkRole } from "../../../utils";

const { signIn } = globalAPI;

const schema = yup.object({
  username: yup.string().required("Username is a required field."),
  password: yup.string().required("Password is a required field."),
});

const AdminSignIn = () => {
  const [status, setStatus] = useState("idle");
  const { setAdminAuth } = useContext(AuthenticationContext);

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    setStatus("loading");
    signIn(data.username, data.password)
      .then((res) => {
        if (res.code >= 400) {
          setStatus("error");
          toast.error(res.message);
        } else {
          if (
            checkRole(res.roles, ROLE_NAME.ADMIN) ||
            checkRole(res.roles, ROLE_NAME.SUPER_ADMIN)
          ) {
            setStatus("success");
            toast.success(res.message);
            sessionStorage.setItem("adminAccount", JSON.stringify(res));
            setAdminAuth(true);
            navigate("/admin");
          } else {
            setStatus("error");
            toast.error("user and password are incorrect");
          }
        }
      })
      .catch((err) => {
        setStatus("error");
        toast.error(err.message);
      });
  };

  return (
    <>
      <div className="sign-in">
        <form className="account-form__main" autoComplete="off">
          <CustomInput
            name="username"
            register={register}
            errors={errors}
            placeholder="Username"
            startIcon={<PermIdentityIcon />}
          />
          <CustomInput
            name="password"
            register={register}
            type="password"
            errors={errors}
            placeholder="Password"
            startIcon={<LockOpenIcon />}
          />
        </form>

        <div className="account-form__foot">
          {status === "loading" ? (
            <CircularProgress color="inherit" />
          ) : (
            <button type="button" onClick={handleSubmit(onSubmit)}>
              Sign In
            </button>
          )}
        </div>
      </div>
    </>
  );
};

export default AdminSignIn;
