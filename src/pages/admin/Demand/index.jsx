import React from "react";
import { IconButton, Tooltip, Paper } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useNavigate } from "react-router-dom";

import PageHeader from "../../../components/PageHeader";
import DemandTable from "./DemandTable";

const Demand = () => {
  //local state
  const navigate = useNavigate();

  return (
    <>
      <div className="admin-page">
        <div className="admin-page__page-header">
          <PageHeader
            title="Demand List"
            subTitle="Admin / Demand management"
          />
          <Tooltip title="Add Demand">
            <IconButton aria-label="add" onClick={() => navigate("add")}>
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <Paper className="admin-page__page-tabs">
          <DemandTable />

          {/* <CustomTabs value={value} setValue={setValue} listTabs={listStatus}>
            <TabPanel value={value} index={0}>
              <JobTable listStatus={listStatus} />
            </TabPanel>
            {listStatus.length > 0 &&
              listStatus.map((tab) => {
                return (
                  <TabPanel value={value} index={tab.id} key={tab.id}>
                    <JobTable statusId={tab.id} listStatus={listStatus} />
                  </TabPanel>
                );
              })}
          </CustomTabs> */}
        </Paper>
      </div>
    </>
  );
};

export default Demand;
