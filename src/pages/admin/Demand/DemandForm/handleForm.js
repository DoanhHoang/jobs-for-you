import * as yup from "yup";
import { toast } from "react-toastify";

import { convert } from "../../../../utils/index";
import demandAPI from "../../../../config/api/demand/demandAPI";

const { createNewDemand } = demandAPI;

export const schema = yup
  .object({
    name: yup.string().required(" * Bạn phải điền ten."),
    university: yup.string().required(" * Bạn phải chọn truong hoc."),
    amount: yup
      .number()
      // .typeError(
      //   " * Số lượng ứng viên không được để trống hoặc không phải là số."
      // )
      .min(1, " * Số lượng ứng viên phải lớn hơn 0. ")
      .integer(" * Số lượng ứng viên phải là số nguyên. "),
    startDate: yup
      .date()
      // .min(
      //   `${date}`,
      //   ` * Bạn không thể chọn ngày bắt đầu tuyển sau ngày ${dateNow}`
      // )
      .required(),
    endDate: yup
      .date()
      // .min(`${tomorowFormat}`, "Ngày hết hạn phải lớn hơn ngày bắt đầu")
      .required(),
    desciption: yup.string(),
    requirement: yup.string(),
    ortherInfo: yup.string(),
  })
  .required();

const defaultValue = {
  name: "",
  description: "",
  university: "",
  amount: 0,
  startDate: "",
  endDate: "",
  requirement: "",
  ortherInfo: "",
};

/**
 * Add or Edit company
 * @param {*} isAdd
 * @param {*} data
 * @param {*} id
 * @param {*} reset
 */
export const AddOrUpdateJob = async (
  isAdd,
  data,
  id,
  reset,
  navigate,
  setStatus
) => {
  const demandData = {
    name: data.name,
    amount: data.amount,
    requirement: data.requirement,
    desciption: data.desciption,
    ortherInfo: data.ortherInfo,
    startDate: convert(data.startDate),
    endDate: convert(data.endDate),
    universityDTO: {
      id: data.university,
    },
  };

  if (isAdd) {
    await createNewDemand(demandData)
      .then(() => {
        setStatus("success");
        toast.success("Created Demand successfully");
        reset(defaultValue);
      })
      .catch(() => {
        setStatus("error");
        toast.error("Created Job failed");
      });
  }
  // else {
  //   await updateJob(jobData, id)
  //     .then(() => {
  //       setStatus("success");
  //       toast.success("Updated Job successfully");
  //       navigate(-1);
  //     })
  //     .catch(() => {
  //       setStatus("success");
  //       toast.error("Updated Job failed");
  //     });
  // }
};
