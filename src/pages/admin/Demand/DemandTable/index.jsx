import React, { useEffect, useState } from "react";
// import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import DoDisturbAltIcon from "@mui/icons-material/DoDisturbAlt";
import { IconButton, Tooltip } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import moment from "moment";

import "./styles.scss";
import DataTable from "../../../../components/Table";
import { getDemandListThunk } from "../../../../store/slices/DemandSlice";
import ViewDetailModal from "../../../../components/CustomModal/ViewDetailModal";
import VertifyDelete from "../../../../components/CustomModal/VertifyDelete";

const DemandTable = ({ statusId, listStatus }) => {
  const [selectedRows, setSelectedRows] = useState([]);
  const [viewDetailOpen, setViewDetailOpen] = useState(false);
  const [vertifyDeleteOpen, setVertifyDeleteOpen] = useState(false);
  const [showId, setShowId] = useState();
  const [isActive, setIsActive] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { demandList } = useSelector((state) => state.demand);

  useEffect(() => {
    dispatch(getDemandListThunk());
  }, [dispatch]);

  const columns = [
    { field: "stt", headerName: "#", width: 70 },
    {
      field: "name",
      headerName: "Name",
      width: 270,
    },
    { field: "start", headerName: "Date start", flex: 1 },
    { field: "end", headerName: "Date end", flex: 1 },
    {
      field: "status",
      headerName: "Status",
      flex: 1,
      renderCell: (params) => {
        const { row, value } = params;
        const handleShow = () => {
          setShowId(row.id);
          setViewDetailOpen(true);
          if (value === 1) {
            setIsActive(false);
          } else {
            setIsActive(true);
          }
        };
        return (
          <>
            {value === 1 && (
              <button type="button" onClick={handleShow} className="btn-active">
                Active
              </button>
            )}
            {value === 2 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-disable"
              >
                Inactive
              </button>
            )}
            {value === 3 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-deleted"
              >
                Deleted
              </button>
            )}
            {value === 4 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-pending"
              >
                Pending
              </button>
            )}
          </>
        );
      },
    },
    {
      field: "action",
      headerName: "Actions",
      width: 120,
      sortable: false,
      renderCell: (params) => {
        const { row } = params;
        const handleClick = () => {
          // console.log(row);
          navigate(`/admin/job/${row.id}`);
        };
        return (
          <>
            <Tooltip title="Detail">
              <IconButton className="job-edit__button" onClick={handleClick}>
                <VisibilityOutlinedIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Delete">
              <IconButton className="job-delete__button">
                <DoDisturbAltIcon />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const rows = [];
  for (let i = 0; i < demandList?.length; i++) {
    rows.push({
      id: demandList[i].id,
      stt: i + 1,
      name: demandList[i].name,
      start: demandList[i].startDate
        ? moment(demandList[i].date).format("DD/MM/YYYY")
        : moment().format("DD/MM/YYYY"),
      end: demandList[i].endDate
        ? moment(demandList[i].date).format("DD/MM/YYYY")
        : moment().format("DD/MM/YYYY"),
      status: demandList[i].status.id,
      description: demandList[i].description,
    });
  }
  return (
    <>
      <DataTable rows={rows} columns={columns} />
      <VertifyDelete
        open={vertifyDeleteOpen}
        setOpen={setVertifyDeleteOpen}
        id={showId}
        item="demand"
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
      />

      <ViewDetailModal
        open={viewDetailOpen}
        setOpen={setViewDetailOpen}
        id={showId}
        isActive={isActive}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        item="demand"
      />
    </>
  );
};

export default DemandTable;
