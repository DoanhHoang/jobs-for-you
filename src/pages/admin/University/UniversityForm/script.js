import * as yup from "yup";
import { toast } from "react-toastify";
import universityAPI from "../../../../config/api/university/universityAPI";

const { addUniversity, updateUniversityInfo } = universityAPI;

// yup validation for company table
export const schema = yup
  .object({
    logo: yup.mixed().nullable(),
    // .test(
    //   "FILE_SIZE",
    //   "Uploaded file is too bid!",
    //   (value) => !value || (value && value.size <= 1024 * 1024)
    // ),
    name: yup.string().required(" * This field is required."),
    website: yup.string().required(" * This field is required."),
    email: yup
      .string(" * Email is invalid.")
      .email(" * Email is invalid.")
      .matches(
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        " * Email is invalid."
      )
      .required(" * This field is required."),
    phone: yup
      .string()
      .required(" * This field is required.")
      .matches(
        /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
        " * Số điện thoại không đúng."
      ),
    shortName: yup.string().required(" * This field is required."),
    description: yup.string().required(" * This field is required."),
    address: yup.string(),
    province: yup.string().required(" * Bạn phải nhập dia chi"),
    district: yup.string().required(" * Bạn phải nhập quan/huyen"),
  })
  .required();

const defaultValue = {
  name: "",
  description: "",
  website: "",
  email: "",
  shortName: "",
  phone: "",
  logo: "",
  address: "",
  province: "",
  district: "",
};

/**
 * Add or Edit company
 * @param {*} isAdd
 * @param {*} data
 * @param {*} id
 * @param {*} reset
 */
export const AddOrUpdateUniversity = (
  isAdd,
  data,
  id,
  reset,
  navigate,
  setStatus
) => {
  const universityData = {
    university: JSON.stringify({
      name: data.name,
      description: data.description,
      website: data.website,
      email: data.email,
      shortName: data.shortName,
      phone: data.phone,
      locations: [
        {
          address: data.address,
          district: {
            id: parseInt(data.district),
          },
        },
      ],
    }),
    file: data.logo ? data.logo[0] : null,
  };

  if (isAdd) {
    addUniversity(universityData)
      .then(() => {
        setStatus("success");
        toast.success("Created university successfully");
        reset(defaultValue);
      })
      .catch(() => {
        setStatus("error");
        toast.error("Created university failed");
      });
  } else {
    updateUniversityInfo(universityData, id)
      .then(() => {
        setStatus("success");
        toast.success("Updated university successfully");
        navigate(-1);
      })
      .catch(() => {
        setStatus("success");
        toast.error("Updated university failed");
      });
  }
};
