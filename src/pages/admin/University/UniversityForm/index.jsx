import React, { useState, useEffect } from "react";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Grid, Avatar, TextField, Paper } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";
import { getUniversityDetailThunk } from "../../../../store/slices/UniversitySlice";
import locationAPI from "../../../../config/api/location/locationAPI";

import "./styles.scss";
import { schema, AddOrUpdateUniversity } from "./script";
import cameraLogo from "../../../../asset/img/camera.png";
import { PageHeader, CustomInput, SelectCustom } from "../../../../components";

const { getProvinceList } = locationAPI;

const UniversityForm = ({ universityId, setOpen }) => {
  // get university ID params from URL
  const { id } = useParams();

  // local state
  const [status, setStatus] = useState("idle");
  const [listProvince, setListProvince] = useState([]);
  const [listDistrict, setListDistrict] = useState([]);
  const [image, setImage] = useState(cameraLogo);
  const [isEdit, setIsEdit] = useState(!id ? false : true);
  const { universityDetail } = useSelector((state) => state.university);

  const methods = useForm({
    resolver: yupResolver(schema),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    reset,
  } = methods;

  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    getProvinceList().then((res) => {
      setListProvince(res.data);
    });
  }, []);

  /**
   * get university details
   * @dependency  comid
   */
  useEffect(() => {
    if (isEdit) {
      dispatch(getUniversityDetailThunk(id || universityId));
    }
  }, [isEdit, dispatch, id, universityId]);

  /**
   * @dependency universityDetail
   * isAdd ? "" : universityDetail
   */
  useEffect(() => {
    if (isEdit) {
      setImage(universityDetail.avatar);
    }
    setValue("name", !isEdit ? "" : universityDetail.name);
    setValue("description", !isEdit ? "" : universityDetail.description);
    setValue("email", !isEdit ? "" : universityDetail.email);
    setValue("phone", !isEdit ? "" : universityDetail.phone);
    setValue("shortName", !isEdit ? "" : universityDetail.shortName);
    setValue("website", !isEdit ? "" : universityDetail.website);
    if (universityDetail.locations && universityDetail.locations.length > 0) {
      console.log(universityDetail.locations[0]?.district?.id);
      setValue(
        "address",
        !isEdit ? "" : universityDetail.locations[0]?.address
      );
      setValue(
        "district",
        !isEdit ? "" : parseInt(universityDetail.locations[0]?.district?.id)
      );
      setValue(
        "province",
        !isEdit
          ? ""
          : parseInt(universityDetail.locations[0]?.district?.province?.id)
      );
    }
  }, [universityDetail]);

  // show preview image
  const showPreviewImage = (e) => {
    if (e.target.files && e.target.files[0]) {
      let imageFile = e.target.files[0];
      const reader = new FileReader();
      reader.onload = (x) => {
        // setImageFile(imageFile);
        setImage(x.target.result);
      };
      reader.readAsDataURL(imageFile);
    }
  };

  // handle Submit form
  const onSubmit = (data) => {
    AddOrUpdateUniversity(!isEdit, data, id, reset, navigate, setStatus);
  };

  return (
    <FormProvider {...methods}>
      <div className="university-form">
        <PageHeader
          title={!isEdit ? "Create new  university" : "Edit university"}
          subTitle={`Admin / university / ${
            !isEdit ? "Create university" : "Update university"
          } `}
        />

        <form
          autoComplete="off"
          encType="multipart/form-data"
          onSubmit={handleSubmit(onSubmit)}
        >
          <Grid container spacing={2}>
            <Grid item md={4}>
              <Paper variant="outlined" className="university-form__left">
                <label htmlFor="logo">
                  <div className="university-form__left-logo">
                    <Avatar
                      src={image}
                      variant="rounded"
                      alt="university-logo"
                      className="university-form__avatar"
                      // onClick={() => fileInput.current.click()}
                    />
                  </div>
                </label>

                <h6>Allowed *.jpeg, *.jpg, *.png</h6>
                <h6>max size of 3.1 MB</h6>
                <input
                  className="university-form__left-file-input"
                  id="logo"
                  type="file"
                  name="logo"
                  {...register("logo")}
                  onChange={showPreviewImage}
                  // ref={fileInput}
                />
                <p className="university-form__error">{errors.logo?.message}</p>
              </Paper>
            </Grid>
            <Grid item md={8}>
              <Paper variant="outlined" className="university-form__right">
                <Grid container spacing={2}>
                  <Grid item md={12}>
                    <CustomInput
                      name="name"
                      register={register}
                      title="Name"
                      placeholder="Name"
                      errors={errors}
                    />
                  </Grid>
                  <Grid item md={6}>
                    <CustomInput
                      name="email"
                      title="Email"
                      register={register}
                      placeholder="Email"
                      errors={errors}
                    />
                    <CustomInput
                      name="phone"
                      title="Phone"
                      register={register}
                      placeholder="Phone"
                      errors={errors}
                    />
                    <SelectCustom
                      id="province"
                      label="Province/City"
                      placeholder="please choose option"
                      dispatch={dispatch}
                      // action={getDistrictList}
                      options={listProvince}
                      setListDistrict={setListDistrict}
                      updateData={universityDetail}
                    >
                      {errors.province?.message}
                    </SelectCustom>
                  </Grid>
                  <Grid item md={6}>
                    <CustomInput
                      name="website"
                      title="Website"
                      register={register}
                      placeholder="Website"
                      errors={errors}
                    />

                    <CustomInput
                      name="shortName"
                      title="Short Name"
                      register={register}
                      placeholder="Short Name"
                      errors={errors}
                    />
                    <SelectCustom
                      id="district"
                      label="District"
                      placeholder="please choose option"
                      options={listDistrict}
                      register={register}
                    >
                      {errors.district?.message}
                    </SelectCustom>
                  </Grid>
                  <Grid item md={12}>
                    <CustomInput
                      name="address"
                      register={register}
                      title="Address"
                      placeholder="Address"
                      errors={errors}
                    />
                  </Grid>
                  <Grid item md={12}>
                    <CustomInput
                      name="description"
                      register={register}
                      title="Description"
                      multiline={true}
                      minRows={4}
                    />
                  </Grid>
                  <Grid item md={12}>
                    <div className="university-form__right-submit">
                      <button
                        type="button"
                        className="university-form__right-submit-btn"
                        onClick={handleSubmit(onSubmit)}
                      >
                        Save changes
                      </button>
                    </div>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </form>
      </div>
    </FormProvider>
  );
};

export default UniversityForm;
