import React, { useEffect, useState } from "react";
// import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import DoNotDisturb from "@mui/icons-material/DoNotDisturb";
import DoDisturbAltIcon from "@mui/icons-material/DoDisturbAlt";
import TaskAlt from "@mui/icons-material/TaskAlt";
import DeleteForeverOutlined from "@mui/icons-material/DeleteForeverOutlined";
import { IconButton, Tooltip, Pagination } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import moment from "moment";

import "./styles.scss";
import DataTable from "../../../../components/Table";
import { getJobListThunk } from "../../../../store/slices/JobSlice";
// import ProfileTable from "../../../../components/ProfileTable";
import ViewDetailModal from "../../../../components/CustomModal/ViewDetailModal";
import VertifyDelete from "../../../../components/CustomModal/VertifyDelete";

const JobTable = ({ statusId, listStatus }) => {
  const [page, setPage] = useState(1);
  const [selectedRows, setSelectedRows] = useState([]);
  const [viewDetailOpen, setViewDetailOpen] = useState(false);
  const [vertifyDeleteOpen, setVertifyDeleteOpen] = useState(false);
  const [showId, setShowId] = useState();
  const [isActive, setIsActive] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { jobList, pagination, status } = useSelector((state) => state.job);

  const handleDisOrAcAllSelectedLesson = (isActive) => {
    setViewDetailOpen(true);
    setIsActive(isActive);
  };

  console.log(pagination);

  useEffect(() => {
    dispatch(getJobListThunk({ no: 0, limit: 5, status: statusId }));
  }, [dispatch, statusId]);

  const columns = [
    { field: "stt", headerName: "#", width: 70 },
    {
      field: "name",
      headerName: "Name",
      width: 270,
    },
    { field: "start", headerName: "Date start", flex: 1 },
    { field: "end", headerName: "Date end", flex: 1 },
    {
      field: "status",
      headerName: "Status",
      flex: 1,
      renderCell: (params) => {
        const { row, value } = params;
        const handleShow = () => {
          setShowId(row.id);
          setViewDetailOpen(true);
          if (value === 1) {
            setIsActive(false);
          } else {
            setIsActive(true);
          }
        };
        return (
          <>
            {value === 1 && (
              <button type="button" onClick={handleShow} className="btn-active">
                Active
              </button>
            )}
            {value === 2 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-disable"
              >
                Inactive
              </button>
            )}
            {value === 3 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-deleted"
              >
                Deleted
              </button>
            )}
            {value === 4 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-pending"
              >
                Pending
              </button>
            )}
          </>
        );
      },
    },
    {
      field: "action",
      headerName: "Actions",
      width: 120,
      sortable: false,
      renderCell: (params) => {
        const { row } = params;
        const handleClick = () => {
          // console.log(row);
          navigate(`/admin/job/${row.id}`);
        };
        return (
          <>
            <Tooltip title="Detail">
              <IconButton className="job-edit__button" onClick={handleClick}>
                <VisibilityOutlinedIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Delete">
              <IconButton
                className="job-delete__button"
                onClick={() => {
                  setShowId(row.id);
                  setVertifyDeleteOpen(true);
                }}
              >
                <DoDisturbAltIcon />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const rows = [];
  for (let i = 0; i < jobList?.length; i++) {
    rows.push({
      id: jobList[i].id,
      stt: i + 1,
      name: jobList[i].name,
      start: jobList[i].start
        ? moment(jobList[i].date).format("DD/MM/YYYY")
        : moment().format("DD/MM/YYYY"),
      end: jobList[i].end
        ? moment(jobList[i].date).format("DD/MM/YYYY")
        : moment().format("DD/MM/YYYY"),
      status: jobList[i].status.id,
      description: jobList[i].description,
    });
  }
  return (
    <>
      <div className="company-select">
        <p>
          {selectedRows.length
            ? selectedRows.length + " Selected"
            : "Total records: " + pagination.sizeCurrentItems + " records"}
        </p>
        {selectedRows.length > 0 && (
          <div>
            <Tooltip title="Delete all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => setVertifyDeleteOpen(true)}
              >
                <DeleteForeverOutlined style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
            <Tooltip title="Active all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => handleDisOrAcAllSelectedLesson(true)}
              >
                <TaskAlt style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
            <Tooltip title="Disable all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => handleDisOrAcAllSelectedLesson(false)}
              >
                <DoNotDisturb style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
          </div>
        )}
      </div>
      <DataTable
        rows={rows}
        columns={columns}
        setSelectedRows={setSelectedRows}
        status={status}
      />
      <div className="admin-page__page-pagination">
        <Pagination
          count={pagination.totalPages}
          page={page}
          onChange={(e, value) => {
            setPage(value);
            dispatch(
              getJobListThunk({ no: value - 1, limit: 5, status: statusId })
            );
          }}
        />
      </div>
      <VertifyDelete
        open={vertifyDeleteOpen}
        setOpen={setVertifyDeleteOpen}
        id={showId}
        item="job"
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
      />

      <ViewDetailModal
        open={viewDetailOpen}
        setOpen={setViewDetailOpen}
        id={showId}
        isActive={isActive}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        item="job"
      />
    </>
  );
};

export default JobTable;
