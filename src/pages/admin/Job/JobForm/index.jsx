import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";

import "./styles.scss";
import { JobForm, PageHeader } from "../../../../components";

const JobFormCompany = () => {
  // get company ID params from URL
  const { id } = useParams();

  // local state
  const [status, setStatus] = useState("idle");
  const isEdit = !id ? false : true;
  const navigate = useNavigate();

  return (
    <>
      <div className="job-form">
        <PageHeader
          title={!isEdit ? "Create Job" : "Edit Job"}
          subTitle={`Admin / Job / ${!isEdit ? "Create Job" : "Edit Job"} `}
        />
        <JobForm id={id} isEdit={isEdit} />
      </div>
    </>
  );
};

export default JobFormCompany;
