import React, { useState, useEffect } from "react";
import { IconButton, Tooltip, Paper } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import { useNavigate } from "react-router-dom";

import PageHeader from "../../../components/PageHeader";
import JobTable from "./JobTable";
import CustomTabs from "../../../components/CustomTabs";
import { TabPanel } from "../../../components/CustomTabs";
import statusAPI from "../../../config/api/status/statusAPI";

const Job = () => {
  //local state

  const [value, setValue] = useState(0);
  const [listStatus, setListStatus] = useState([]);
  const { getAllStatus } = statusAPI;

  const navigate = useNavigate();

  useEffect(() => {
    getAllStatus().then((res) => {
      setListStatus(res);
    });
  }, [getAllStatus]);
  return (
    <>
      <div className="admin-page">
        <div className="admin-page__page-header">
          <PageHeader title="Job List" subTitle="Admin / Job management" />
          <Tooltip title="Add Job">
            <IconButton aria-label="add" onClick={() => navigate("add")}>
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <Paper className="admin-page__page-tabs">
          <CustomTabs value={value} setValue={setValue} listTabs={listStatus}>
            <TabPanel value={value} index={0}>
              <JobTable listStatus={listStatus} />
            </TabPanel>
            {listStatus.length > 0 &&
              listStatus.map((tab) => {
                return (
                  <TabPanel value={value} index={tab.id} key={tab.id}>
                    <JobTable statusId={tab.name} listStatus={listStatus} />
                  </TabPanel>
                );
              })}
          </CustomTabs>
        </Paper>
      </div>
    </>
  );
};

export default Job;
