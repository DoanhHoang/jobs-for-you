import React, { useState, useEffect } from "react";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { Grid, Avatar, Paper, CircularProgress } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";
import { getCompanyDetailThunk } from "../../../../store/slices/CompanySlice";

import "./styles.scss";
import { schema, AddOrUpdateCompany } from "./script";
import cameraLogo from "../../../../asset/img/camera.png";
import { PageHeader, CustomInput, SelectCustom } from "../../../../components";

import locationAPI from "../../../../config/api/location/locationAPI";

const { getProvinceList } = locationAPI;

const CompanyForm = ({ companyId, setOpen }) => {
  // get company ID params from URL
  const { id } = useParams();

  // local state
  const [listProvince, setListProvince] = useState([]);
  const [listDistrict, setListDistrict] = useState([]);
  const [status, setStatus] = useState("idle");
  const [image, setImage] = useState(cameraLogo);
  const isEdit = !id && !companyId ? false : true;
  const { companyDetail } = useSelector((state) => state.company);

  const navigate = useNavigate();

  const methods = useForm({
    resolver: yupResolver(schema),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    reset,
  } = methods;

  const dispatch = useDispatch();

  useEffect(() => {
    getProvinceList().then((res) => {
      setListProvince(res.data);
    });
  }, []);

  /**
   * get company details
   * @dependency  comid
   */
  useEffect(() => {
    if (isEdit) {
      id && dispatch(getCompanyDetailThunk(id));
      companyId && dispatch(getCompanyDetailThunk(companyId));
    }
  }, [isEdit, dispatch, id, companyId]);

  useEffect(() => {
    if (status === "success") {
      setImage(cameraLogo);
    }
  }, [status]);

  /**
   * @dependency companyDetail
   * isAdd ? "" : companyDetail
   */
  useEffect(() => {
    if (isEdit) {
      setImage(companyDetail.logo ? companyDetail.logo : cameraLogo);
    }
    setValue("name", !isEdit ? "" : companyDetail.name);
    setValue("description", !isEdit ? "" : companyDetail.description);
    setValue("email", !isEdit ? "" : companyDetail.email);
    setValue("phone", !isEdit ? "" : companyDetail.phone);
    setValue("tax", !isEdit ? "" : companyDetail.tax);
    setValue("website", !isEdit ? "" : companyDetail.website);
    if (companyDetail.locations && companyDetail.locations.length > 0) {
      console.log(companyDetail.locations[0]?.district?.id);
      setValue("address", !isEdit ? "" : companyDetail.locations[0]?.address);
      setValue(
        "district",
        !isEdit ? "" : parseInt(companyDetail.locations[0]?.district?.id)
      );
      setValue(
        "province",
        !isEdit
          ? ""
          : parseInt(companyDetail.locations[0]?.district?.province?.id)
      );
    }
  }, [companyDetail, isEdit, setValue]);

  // show preview image
  const showPreviewImage = (e) => {
    if (e.target.files && e.target.files[0]) {
      let imageFile = e.target.files[0];
      const reader = new FileReader();
      reader.onload = (x) => {
        // setImageFile(imageFile);
        setImage(x.target.result);
      };
      reader.readAsDataURL(imageFile);
    }
  };

  // handle Submit form
  const onSubmit = (data) => {
    setStatus("loading");
    AddOrUpdateCompany({
      isAdd: !isEdit,
      data,
      id: id || companyId,
      reset,
      navigate,
      setStatus,
      setOpen,
    });
  };

  return (
    <FormProvider {...methods}>
      <div className="company-form">
        <PageHeader
          title={!isEdit ? "Create Company" : "Edit Company"}
          subTitle={`Admin / Company / ${
            !isEdit ? "Create Company" : "Edit Company"
          } `}
        />

        <form
          autoComplete="off"
          encType="multipart/form-data"
          onSubmit={handleSubmit(onSubmit)}
        >
          <Grid container spacing={2}>
            <Grid item md={4}>
              <Paper variant="outlined" className="company-form__left">
                <label htmlFor="logo">
                  <div className="company-form__left-logo">
                    <Avatar
                      src={image}
                      variant="rounded"
                      alt="company-logo"
                      className="company-form__avatar"
                      // onClick={() => fileInput.current.click()}
                    />
                  </div>
                </label>
                <h6>Allowed *.jpeg, *.jpg, *.png</h6>
                <h6>max size of 3.1 MB</h6>
                <input
                  className="company-form__left-file-input"
                  id="logo"
                  type="file"
                  name="logo"
                  {...register("logo")}
                  onChange={showPreviewImage}
                  // ref={fileInput}
                />
                <p className="company-form__error">{errors.logo?.message}</p>
              </Paper>
            </Grid>
            <Grid item md={8}>
              <Paper variant="outlined" className="company-form__right">
                <Grid container spacing={2}>
                  <Grid item md={12}>
                    <CustomInput
                      name="name"
                      register={register}
                      title="Name"
                      placeholder="Name"
                      errors={errors}
                    />
                  </Grid>
                  <Grid item md={6}>
                    <CustomInput
                      name="email"
                      register={register}
                      title="Email"
                      placeholder="Email"
                      errors={errors}
                    />
                    <CustomInput
                      name="phone"
                      register={register}
                      title="Phone"
                      placeholder="Phone"
                      errors={errors}
                    />
                    <SelectCustom
                      id="province"
                      label="Province/City"
                      placeholder="please choose option"
                      dispatch={dispatch}
                      // action={getDistrictList}
                      options={listProvince}
                      setListDistrict={setListDistrict}
                      updateData={companyDetail}
                    >
                      {errors.province?.message}
                    </SelectCustom>
                  </Grid>
                  <Grid item md={6}>
                    <CustomInput
                      name="website"
                      register={register}
                      title="Website"
                      placeholder="Website"
                      errors={errors}
                    />
                    <CustomInput
                      name="tax"
                      register={register}
                      title="Tax"
                      placeholder="Tax"
                      errors={errors}
                    />
                    <SelectCustom
                      id="district"
                      label="District"
                      placeholder="please choose option"
                      options={listDistrict}
                      register={register}
                    >
                      {errors.district?.message}
                    </SelectCustom>
                  </Grid>
                  <Grid item md={12}>
                    <CustomInput
                      name="address"
                      register={register}
                      title="Address"
                      placeholder="Address"
                      errors={errors}
                    />
                  </Grid>
                  <Grid item md={12}>
                    <CustomInput
                      name="description"
                      register={register}
                      title="Description"
                      multiline={true}
                      minRows={4}
                    />
                  </Grid>
                  <Grid item md={12}>
                    <div className="company-form__right-submit">
                      {status === "loading" ? (
                        <CircularProgress />
                      ) : (
                        <button
                          type="button"
                          className="company-form__right-submit-btn"
                          onClick={handleSubmit(onSubmit)}
                        >
                          Save changes
                        </button>
                      )}
                    </div>
                  </Grid>
                </Grid>
              </Paper>
            </Grid>
          </Grid>
        </form>
      </div>
    </FormProvider>
  );
};

export default CompanyForm;
