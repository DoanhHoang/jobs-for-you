import * as yup from "yup";
import { toast } from "react-toastify";
import companyAPI from "../../../../config/api/company/companyAPI";

const { addCompany, updateCompanyInfo } = companyAPI;

// yup validation for company table
export const schema = yup
  .object({
    logo: yup.mixed().nullable(),
    // .test(
    //   "FILE_SIZE",
    //   "Uploaded file is too bid!",
    //   (value) => !value || (value && value.size <= 1024 * 1024)
    // ),
    name: yup.string().required(" * Bạn phải nhập tên công ty."),
    website: yup.string().required(" * Bạn phải nhập tên website công ty."),
    email: yup
      .string(" * email không hợp lệ.")
      // .email(" * Ban nhap email khong dung")
      .matches(
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        " * Email khong hop le"
      )
      .required(" * Bạn phải nhập email công ty."),
    phone: yup
      .string()
      .matches(
        /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
        " * Số điện thoại không đúng."
      ),
    tax: yup
      .string()
      .required(" * Bạn phải nhập mã số thuế.")
      .matches(
        /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
        " * Mã số thuế không đúng."
      ),
    description: yup.string(),
    address: yup.string(),
    province: yup.string().required(" * Bạn phải nhập dia chi"),
    district: yup.string().required(" * Bạn phải nhập quan/huyen"),
  })
  .required();

const defaultValue = {
  name: "",
  description: "",
  website: "",
  email: "",
  tax: "",
  phone: "",
  logo: "",
  address: "",
  province: "",
  district: "",
};

/**
 * Add or Edit company
 * @param {*} isAdd
 * @param {*} data
 * @param {*} id
 * @param {*} reset
 */
export const AddOrUpdateCompany = async ({
  isAdd,
  data,
  id,
  reset,
  setOpen,
  navigate,
  setStatus,
}) => {
  const companyData = {
    company: JSON.stringify({
      name: data.name,
      description: data.description,
      website: data.website,
      email: data.email,
      tax: data.tax,
      phone: data.phone,
      locations: [
        {
          address: data.address,
          district: {
            id: parseInt(data.district),
          },
        },
      ],
    }),
    file: data.logo ? data.logo[0] : null,
  };

  console.log(data);

  if (isAdd) {
    await addCompany(companyData)
      .then(() => {
        setStatus("success");
        toast.success("Created company successfully");
        reset(defaultValue);
      })
      .catch(() => {
        setStatus("error");
        toast.error("Created company failed");
      });
  } else {
    await updateCompanyInfo(companyData, id)
      .then(() => {
        setStatus("success");
        toast.success("Updated company successfully");
        if (setOpen) {
          setOpen(false);
        } else {
          navigate(-1);
        }
      })
      .catch(() => {
        setStatus("error");
        toast.error("Updated company failed");
      });
  }
};
