import React, { useEffect, useState } from "react";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import DoNotDisturb from "@mui/icons-material/DoNotDisturb";
import DoDisturbAltIcon from "@mui/icons-material/DoDisturbAlt";
import TaskAlt from "@mui/icons-material/TaskAlt";
import DeleteForeverOutlined from "@mui/icons-material/DeleteForeverOutlined";
import { IconButton, Tooltip } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import moment from "moment";

import "./styles.scss";
import DataTable from "../../../../components/Table";
import {
  searchCompanyByNameStatusThunk,
  updateFilterCompany,
} from "../../../../store/slices/CompanySlice";
import ProfileTable from "../../../../components/ProfileTable";
import ViewDetailModal from "../../../../components/CustomModal/ViewDetailModal";
import VertifyDelete from "../../../../components/CustomModal/VertifyDelete";

const CompanyTable = ({ statusId, name, setName }) => {
  const [selectedRows, setSelectedRows] = useState([]);
  const [viewDetailOpen, setViewDetailOpen] = useState(false);
  const [vertifyDeleteOpen, setVertifyDeleteOpen] = useState(false);
  const [showId, setShowId] = useState();
  const [isActive, setIsActive] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { companyList, pagination, status } = useSelector(
    (state) => state.company
  );

  useEffect(() => {
    setName("");
  }, [statusId]);

  useEffect(() => {
    dispatch(
      updateFilterCompany({
        statusId,
        name: name === "" ? null : name,
      })
    );
    dispatch(
      searchCompanyByNameStatusThunk({
        statusId: statusId ? statusId : 0,
        name: name === "" ? null : name,
      })
    );
  }, [name, statusId]);

  const handleDisOrAcAllSelectedLesson = (isActive) => {
    setViewDetailOpen(true);
    setIsActive(isActive);
  };

  const columns = [
    { field: "stt", headerName: "#", width: 70 },
    {
      field: "name",
      headerName: "Name",
      width: 430,
      renderCell: (params) => {
        const { row } = params;
        return <ProfileTable row={row} />;
      },
    },
    {
      field: "tax",
      headerName: "Tax",
      flex: 1,
      renderCell: (params) => {
        const { row } = params;
        // console.log(row.email);
        return (
          <a
            href={`https://www.google.com/search?q=ma+so+thue+${row.tax}`}
            className="company-table__hyperlink"
          >
            {row.tax}
          </a>
        );
      },
    },
    { field: "date", headerName: "Create date", flex: 1 },
    {
      field: "status",
      headerName: "Status",
      flex: 1,
      renderCell: (params) => {
        const { row, value } = params;
        const handleShow = () => {
          setShowId(row.id);
          setViewDetailOpen(true);
          if (value === 1) {
            setIsActive(false);
          } else {
            setIsActive(true);
          }
        };
        return (
          <>
            {value === 1 && (
              <button type="button" onClick={handleShow} className="btn-active">
                Active
              </button>
            )}
            {value === 2 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-disable"
              >
                Inactive
              </button>
            )}
            {value === 3 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-deleted"
              >
                Deleted
              </button>
            )}
            {value === 4 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-pending"
              >
                Pending
              </button>
            )}
          </>
        );
      },
    },
    {
      field: "action",
      headerName: "Actions",
      width: 120,
      sortable: false,
      renderCell: (params) => {
        const { row } = params;
        const handleClick = () => {
          navigate(`/admin/company/${row.id}`);
        };
        return (
          <>
            <Tooltip title="Detail">
              <IconButton
                className="company-edit__button"
                onClick={handleClick}
              >
                <VisibilityOutlinedIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Delete">
              <IconButton
                className="company-delete__button"
                onClick={() => {
                  setVertifyDeleteOpen(true);
                  setShowId(row.id);
                }}
              >
                <DoDisturbAltIcon />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const rows = [];
  for (let i = 0; i < companyList?.length; i++) {
    rows.push({
      id: companyList[i].id,
      stt: i + 1,
      name: companyList[i].name,
      website: companyList[i].website,
      email: companyList[i].email,
      tax: companyList[i].tax,
      date: companyList[i].date
        ? moment(companyList[i].date).format("DD/MM/YYYY")
        : moment().format("DD/MM/YYYY"),
      status: companyList[i]?.status?.id,
      description: companyList[i].description,
      logo: companyList[i].logo,
    });
  }
  return (
    <>
      <div className="company-select">
        <p>
          {selectedRows.length
            ? selectedRows.length + " Selected"
            : "Total records: " + pagination.totalItems + " records"}
        </p>
        {selectedRows.length > 0 && (
          <div>
            <Tooltip title="Delete all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => setVertifyDeleteOpen(true)}
              >
                <DeleteForeverOutlined style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
            <Tooltip title="Active all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => handleDisOrAcAllSelectedLesson(true)}
              >
                <TaskAlt style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
            <Tooltip title="Disable all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => handleDisOrAcAllSelectedLesson(false)}
              >
                <DoNotDisturb style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
          </div>
        )}
      </div>
      <DataTable
        rows={rows}
        columns={columns}
        setSelectedRows={setSelectedRows}
        status={status}
      />
      <VertifyDelete
        open={vertifyDeleteOpen}
        setOpen={setVertifyDeleteOpen}
        id={showId}
        item="company"
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
      />

      <ViewDetailModal
        open={viewDetailOpen}
        setOpen={setViewDetailOpen}
        id={showId}
        isActive={isActive}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        item="company"
      />
    </>
  );
};

export default CompanyTable;
