import React, { useState, useEffect } from "react";
import {
  IconButton,
  Tooltip,
  Paper,
  Pagination,
  TextField,
  InputAdornment,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import SearchIcon from "@mui/icons-material/Search";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import "./styles.scss";
import CompanyTable from "./CompanyTable";
import PageHeader from "../../../components/PageHeader";
import CustomTabs from "../../../components/CustomTabs";
import { TabPanel } from "../../../components/CustomTabs";
import statusAPI from "../../../config/api/status/statusAPI";
import { searchCompanyByNameStatusThunk } from "../../../store/slices/CompanySlice";

const Company = () => {
  //local state
  const [value, setValue] = useState(0);
  const [listStatus, setListStatus] = useState([]);
  const [page, setPage] = useState(1);
  const [name, setName] = useState("");

  const { getAllStatus } = statusAPI;

  const { pagination } = useSelector((state) => state.company);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    getAllStatus().then((res) => {
      setListStatus(res);
    });
  }, []);

  const handleChangeSearchKey = (e) => {
    if (e.target.value !== "") {
      setName(e.target.value);
    } else {
      setName("");
    }
  };

  return (
    <>
      <div className="admin-page">
        <div className="admin-page__page-header">
          <PageHeader title="Company" subTitle="Admin / Company management" />
          <Tooltip title="Add Company">
            <IconButton
              aria-label="add"
              onClick={() => {
                navigate("/admin/company/add");
              }}
            >
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <Paper className="admin-page__page-tabs">
          <CustomTabs value={value} setValue={setValue} listTabs={listStatus}>
            <div className="admin-page__page-tabs-search">
              <TextField
                placeholder="Placeholder"
                onChange={handleChangeSearchKey}
                value={name}
                defaultValue={""}
                fullWidth
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  ),
                }}
              />
            </div>
            <TabPanel value={value} index={0}>
              <CompanyTable
                statusId={0}
                listStatus={listStatus}
                name={name}
                setName={setName}
              />
            </TabPanel>
            {listStatus.length > 0 &&
              listStatus.map((tab) => {
                return (
                  <TabPanel value={value} index={tab.id} key={tab.id}>
                    <CompanyTable
                      statusId={tab.id}
                      listStatus={listStatus}
                      name={name}
                      setName={setName}
                    />
                  </TabPanel>
                );
              })}
          </CustomTabs>
        </Paper>
        <div className="admin-page__page-pagination">
          <Pagination
            count={pagination.totalPages}
            page={page}
            onChange={(e, value) => {
              setPage(value);
              dispatch(searchCompanyByNameStatusThunk({ no: value - 1 }));
            }}
          />
        </div>
      </div>
    </>
  );
};

export default Company;
