export const dataBarChart = (labels, data, label) => ({
  labels: labels,
  datasets: [
    {
      label: label,
      backgroundColor: "#5d49dcea",
      borderColor: "#5d49dcea",
      borderWidth: 2,
      data: data,
    },
  ],
});

export const dataLineChart = {
  labels: [9, 10, 11, 12],
  datasets: [
    {
      label: "Job",
      backgroundColor: "#FF5C58",
      borderColor: "#FF5C58",
      borderWidth: 2,
      data: [2, 1, 1, 5],
    },
    {
      label: "Demand",
      backgroundColor: "#ffb400",
      borderColor: "#ffb400",
      borderWidth: 2,
      data: [0, 1, 1, 7],
    },
  ],
};

export const dataDoughnutActiveChart = (lables = [], data, label) => ({
  labels: lables,
  datasets: [
    {
      label: label,
      data: data,
      backgroundColor: [
        "rgba(75, 192, 192, 1)",
        "rgba(255, 99, 132, 1)",
        "rgba(255, 206, 86, 1)",
        "rgba(54, 162, 235, 1)",
        "#5d49dcea",
        "#0072e5",
      ],
      borderColor: [
        "rgba(75, 192, 192, 1)",
        "rgba(255, 99, 132, 1)",
        "rgba(255, 206, 86, 1)",
        "rgba(54, 162, 235, 1)",
        "#5d49dcea",
        "#0072e5",
      ],
      borderWidth: 1,
    },
  ],
});
