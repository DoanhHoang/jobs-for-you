import React, { useState, useEffect } from "react";
import { Grid } from "@mui/material";

import "./styles.scss";
import {
  dataBarChart,
  dataLineChart,
  // dataDoughnutChart,
  dataDoughnutActiveChart,
} from "./script";
import {
  CustomBar,
  CustomLine,
  CustomDoughnut,
} from "../../../components/CustomChart";

import statisticalAPI from "../../../config/api/statistical/statisticaAPI";

const { statisticUserByMonth, statisticUserVerifyEmail, statisticUserRole } =
  statisticalAPI;

const Dashboard = () => {
  const [userData, setUserData] = useState(null);
  const [userVerify, setUserVerify] = useState(null);
  const [userRole, setUserRole] = useState(null);

  useEffect(() => {
    statisticUserByMonth(6).then(async (res) => {
      const month = res.data.map((month) => month[0]);
      const data = res.data.map((item) => item[1]);
      const dataUser = await dataBarChart(
        month,
        data,
        "User register by month"
      );
      setUserData(dataUser);
    });
    statisticUserVerifyEmail().then(async (res) => {
      const labels = res.data.map((item) => {
        return item[0] === true ? "Not verify" : "Verified";
      });
      const data = res.data.map((item) => item[1]);
      const dataUser = await dataDoughnutActiveChart(
        labels,
        data,
        "User verify email"
      );

      setUserVerify(dataUser);
    });
    statisticUserRole().then(async (res) => {
      const labels = res?.data?.map((item) => {
        return item[0]?.split("_")[1];
      });
      const data = res?.data?.map((item) => item[1]);
      console.log(labels);
      const dataUser = await dataDoughnutActiveChart(labels, data, "User Role");

      setUserRole(dataUser);
    });
  }, []);

  return (
    <>
      <div className="dashboard">
        <Grid container spacing={3}>
          <Grid item md={6} xs={12}>
            {userData && (
              <CustomBar
                data={userData}
                title="Number of subscriptions by month"
              />
            )}
          </Grid>
          <Grid item md={6} xs={12}>
            <CustomLine
              data={dataLineChart}
              title="Number of practice by month"
            />
          </Grid>
          <Grid item md={6} xs={12}>
            {userRole && (
              <CustomDoughnut data={userRole} title="Number of User by Role" />
            )}
          </Grid>
          <Grid item md={6} xs={12}>
            {userVerify && (
              <CustomDoughnut
                data={userVerify}
                title="Number of user verify email"
              />
            )}
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default Dashboard;
