import React, { useEffect, useState } from "react";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import DoNotDisturb from "@mui/icons-material/DoNotDisturb";
import DoDisturbAltIcon from "@mui/icons-material/DoDisturbAlt";
import TaskAlt from "@mui/icons-material/TaskAlt";
import DeleteForeverOutlined from "@mui/icons-material/DeleteForeverOutlined";
import { IconButton, Tooltip } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
// import { useNavigate } from "react-router-dom";

import "./styles.scss";
import DataTable from "../../../../components/Table";
import {
  getUserListThunk,
  getUsersByRoleIdThunk,
} from "../../../../store/slices/userSlice";
import ProfileTable from "../../../../components/ProfileTable";
import ViewDetailModal from "../../../../components/CustomModal/ViewDetailModal";
import VertifyDelete from "../../../../components/CustomModal/VertifyDelete";
import { ROLE_NAME } from "../../../../config/constant/roleNameConstant";
import { checkRole } from "../../../../utils";

const UserTable = ({ showId, setShowId, setOpen, setName }) => {
  const [selectedRows, setSelectedRows] = useState([]);
  const [viewDetailOpen, setViewDetailOpen] = useState(false);
  const [vertifyDeleteOpen, setVertifyDeleteOpen] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const dispatch = useDispatch();
  // const navigate = useNavigate();

  const { listUser, pagination, status } = useSelector((state) => state.user);

  useEffect(() => {
    // dispatch(getUserListThunk({ pageNumber: 0, pageSize: 10 }));
    dispatch(getUsersByRoleIdThunk(7));
  }, [dispatch]);

  const handleDisOrAcAllSelectedLesson = (isActive) => {
    setViewDetailOpen(true);
    setIsActive(isActive);
  };

  const columns = [
    { field: "stt", headerName: "#", width: 70 },
    {
      field: "username",
      headerName: "Name",
      width: 430,
      renderCell: (params) => {
        const { row } = params;
        return <ProfileTable row={row} />;
      },
    },
    { field: "name", headerName: "Name", flex: 1 },
    // {
    //   field: "role",
    //   headerName: "Role",
    //   flex: 1,
    //   renderCell: (params) => {
    //     const { row } = params;
    //     return (
    //       <p>{row.role.map((item) => item.name.split("_")[1]).join(", ")}</p>
    //     );
    //   },
    // },

    {
      field: "action",
      headerName: "Actions",
      width: 120,
      sortable: false,
      renderCell: (params) => {
        const { row } = params;
        const handleClick = () => {
          setShowId(row.id);
          setOpen(true);
        };
        return (
          <>
            <Tooltip title="Detail">
              <IconButton
                className="company-edit__button"
                onClick={handleClick}
              >
                <VisibilityOutlinedIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Delete">
              <IconButton
                className="company-delete__button"
                onClick={() => {
                  setVertifyDeleteOpen(true);
                  setShowId(row.id);
                }}
              >
                <DoDisturbAltIcon />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const rows = [];
  for (let i = 0; i < listUser?.length; i++) {
    rows.push({
      id: listUser[i].id,
      stt: i + 1,
      username: listUser[i].username,
      gender: listUser[i].gender,
      email: listUser[i].email,
      phone: listUser[i].phone,
      name: (listUser[i].firstName || "") + " " + (listUser[i].lastName || ""),
      firstName: listUser[i].firstName,
      lastName: listUser[i].lastName,
      avatar: listUser[i].avatar,
      role: listUser[i].role,
    });
  }
  return (
    <>
      <div className="company-select">
        <p>
          {selectedRows.length
            ? selectedRows.length + " Selected"
            : "Total records: " + pagination.totalItems + " records"}
        </p>
        {selectedRows.length > 0 && (
          <div>
            <Tooltip title="Delete all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => setVertifyDeleteOpen(true)}
              >
                <DeleteForeverOutlined style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
            <Tooltip title="Active all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => handleDisOrAcAllSelectedLesson(true)}
              >
                <TaskAlt style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
            <Tooltip title="Disable all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => handleDisOrAcAllSelectedLesson(false)}
              >
                <DoNotDisturb style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
          </div>
        )}
      </div>
      <DataTable
        rows={rows}
        columns={columns}
        setSelectedRows={setSelectedRows}
        status={status}
      />
      <VertifyDelete
        open={vertifyDeleteOpen}
        setOpen={setVertifyDeleteOpen}
        id={showId}
        item="company"
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
      />

      <ViewDetailModal
        open={viewDetailOpen}
        setOpen={setViewDetailOpen}
        id={showId}
        isActive={isActive}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        item="company"
      />
    </>
  );
};

export default UserTable;
