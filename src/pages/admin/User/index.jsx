import React, { useState, useEffect } from "react";
import {
  IconButton,
  Tooltip,
  Paper,
  Pagination,
  TextField,
  InputAdornment,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import SearchIcon from "@mui/icons-material/Search";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import "./styles.scss";
import UserTable from "./UserTable";
import PageHeader from "../../../components/PageHeader";
import statusAPI from "../../../config/api/status/statusAPI";
import { getUserListThunk } from "../../../store/slices/userSlice";
import AddOrUpdateUser from "../../../components/CustomModal/AddOrUpdateUser/AddOrUpdateUser";

const User = () => {
  //local state
  const [listStatus, setListStatus] = useState([]);
  const [open, setOpen] = useState(false);
  const [page, setPage] = useState(1);
  const [name, setName] = useState("");
  const [showId, setShowId] = useState(null);

  const { getAllStatus } = statusAPI;

  const { pagination } = useSelector((state) => state.user);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    getAllStatus().then((res) => {
      setListStatus(res);
    });
  }, []);

  const handleChangeSearchKey = (e) => {
    if (e.target.value !== "") {
      setName(e.target.value);
    } else {
      setName("");
    }
  };

  return (
    <>
      <div className="admin-page">
        <div className="admin-page__page-header">
          <PageHeader title="User" subTitle="Admin" />
          <Tooltip title="Add User">
            <IconButton
              aria-label="add"
              onClick={() => {
                setShowId(null);
                setOpen(true);
              }}
            >
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <Paper className="admin-page__page-tabs">
          <div className="admin-page__page-tabs-search">
            <TextField
              placeholder="Placeholder"
              onChange={handleChangeSearchKey}
              value={name}
              defaultValue={""}
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <UserTable
            name={name}
            setName={setName}
            showId={showId}
            setShowId={setShowId}
            open={open}
            setOpen={setOpen}
          />
        </Paper>
        <div className="admin-page__page-pagination">
          <Pagination
            count={pagination.totalPages}
            page={page}
            onChange={(e, value) => {
              setPage(value);
              dispatch(getUserListThunk({ pageNumber: value - 1 }));
            }}
          />
        </div>
      </div>
      <AddOrUpdateUser open={open} setOpen={setOpen} id={showId} />
    </>
  );
};

export default User;
