import Dashboard from "./Dashboard";
import Company from "./Company";
import University from "./University";
import UniversityForm from "./University/UniversityForm";
import CompanyForm from "./Company/CompanyForm";
import Job from "./Job";
import JobFormCompany from "./Job/JobForm";
import User from "./User";
import Demand from "./Demand";
import Candidate from "./Candidate";
import DemandFormUniversity from "./Demand/DemandForm";

export {
  Dashboard,
  Company,
  University,
  CompanyForm,
  UniversityForm,
  Job,
  JobFormCompany,
  User,
  Demand,
  Candidate,
  DemandFormUniversity,
};
