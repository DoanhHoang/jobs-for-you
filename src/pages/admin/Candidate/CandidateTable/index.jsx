import React, { useEffect, useState } from "react";
import VisibilityOutlinedIcon from "@mui/icons-material/VisibilityOutlined";
import DoNotDisturb from "@mui/icons-material/DoNotDisturb";
import DoDisturbAltIcon from "@mui/icons-material/DoDisturbAlt";
import TaskAlt from "@mui/icons-material/TaskAlt";
import DeleteForeverOutlined from "@mui/icons-material/DeleteForeverOutlined";
import { IconButton, Tooltip } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
// import { useNavigate } from "react-router-dom";

import "./styles.scss";
import DataTable from "../../../../components/Table";
import { getListCandidateThunk } from "../../../../store/slices/CandidateSlice";
import ProfileTable from "../../../../components/ProfileTable";
import ViewDetailModal from "../../../../components/CustomModal/ViewDetailModal";
import VertifyDelete from "../../../../components/CustomModal/VertifyDelete";

const CandidateTable = ({ showId, setShowId, setOpen }) => {
  const [selectedRows, setSelectedRows] = useState([]);
  const [viewDetailOpen, setViewDetailOpen] = useState(false);
  const [vertifyDeleteOpen, setVertifyDeleteOpen] = useState(false);
  const [isActive, setIsActive] = useState(false);
  const dispatch = useDispatch();
  // const navigate = useNavigate();

  const { listCandidate, pagination, status } = useSelector(
    (state) => state.candidate
  );

  useEffect(() => {
    dispatch(getListCandidateThunk({ no: 0, limit: 8 }));
  }, [dispatch]);

  const handleDisOrAcAllSelectedLesson = (isActive) => {
    setViewDetailOpen(true);
    setIsActive(isActive);
  };

  const columns = [
    { field: "stt", headerName: "#", width: 70 },
    {
      field: "username",
      headerName: "Username",
      width: 430,
      renderCell: (params) => {
        const { row } = params;
        return <ProfileTable row={row} />;
      },
    },
    {
      field: "name",
      headerName: "Name",
      flex: 1,
    },
    {
      field: "status",
      headerName: "Status",
      flex: 1,
      renderCell: (params) => {
        const { row, value } = params;
        const handleShow = () => {
          setShowId(row.id);
          setViewDetailOpen(true);
          if (value === 1) {
            setIsActive(false);
          } else {
            setIsActive(true);
          }
        };
        return (
          <>
            {value === 1 && (
              <button type="button" onClick={handleShow} className="btn-active">
                Active
              </button>
            )}
            {value === 2 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-disable"
              >
                Inactive
              </button>
            )}
            {value === 3 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-deleted"
              >
                Deleted
              </button>
            )}
            {value === 4 && (
              <button
                type="button"
                onClick={handleShow}
                className="btn-pending"
              >
                Pending
              </button>
            )}
          </>
        );
      },
    },

    {
      field: "action",
      headerName: "Actions",
      width: 120,
      sortable: false,
      renderCell: (params) => {
        const { row } = params;
        const handleClick = () => {
          setShowId(row.id);
          setOpen(true);
        };
        return (
          <>
            <Tooltip title="Detail">
              <IconButton
                className="company-edit__button"
                onClick={handleClick}
              >
                <VisibilityOutlinedIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Delete">
              <IconButton
                className="company-delete__button"
                onClick={() => {
                  setVertifyDeleteOpen(true);
                  setShowId(row.id);
                }}
              >
                <DoDisturbAltIcon />
              </IconButton>
            </Tooltip>
          </>
        );
      },
    },
  ];

  const rows = [];
  for (let i = 0; i < listCandidate?.length; i++) {
    rows.push({
      id: listCandidate[i]?.id,
      stt: i + 1,
      username: listCandidate[i]?.userDTO.username,
      gender: listCandidate[i]?.userDTO.gender,
      email: listCandidate[i]?.userDTO.email,
      phone: listCandidate[i]?.userDTO.phone,
      name:
        listCandidate[i]?.userDTO.firstName +
        " " +
        listCandidate[i]?.userDTO.lastName,
      avatar: listCandidate[i]?.userDTO.avatar,
      status: listCandidate[i]?.userDTO?.status?.id,
    });
  }
  return (
    <>
      <div className="company-select">
        <p>
          {selectedRows.length
            ? selectedRows.length + " Selected"
            : "Total records: " + pagination.totalItems + " records"}
        </p>
        {selectedRows.length > 0 && (
          <div>
            <Tooltip title="Delete all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => setVertifyDeleteOpen(true)}
              >
                <DeleteForeverOutlined style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
            <Tooltip title="Active all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => handleDisOrAcAllSelectedLesson(true)}
              >
                <TaskAlt style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
            <Tooltip title="Disable all selected">
              <IconButton
                className="lesson-table__btn"
                onClick={() => handleDisOrAcAllSelectedLesson(false)}
              >
                <DoNotDisturb style={{ color: "#fff" }} />
              </IconButton>
            </Tooltip>
          </div>
        )}
      </div>
      <DataTable
        rows={rows}
        columns={columns}
        setSelectedRows={setSelectedRows}
        status={status}
      />
      <VertifyDelete
        open={vertifyDeleteOpen}
        setOpen={setVertifyDeleteOpen}
        id={showId}
        item="candidate"
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
      />

      <ViewDetailModal
        open={viewDetailOpen}
        setOpen={setViewDetailOpen}
        id={showId}
        isActive={isActive}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        item="candidate"
      />
    </>
  );
};

export default CandidateTable;
