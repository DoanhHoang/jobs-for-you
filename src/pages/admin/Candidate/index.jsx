import React, { useState } from "react";
import {
  IconButton,
  Tooltip,
  Paper,
  Pagination,
  TextField,
  InputAdornment,
} from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import SearchIcon from "@mui/icons-material/Search";
import { useSelector, useDispatch } from "react-redux";

import "./styles.scss";
import CandidateTable from "./CandidateTable";
import PageHeader from "../../../components/PageHeader";
import { getListCandidateThunk } from "../../../store/slices/CandidateSlice";

const Candidate = () => {
  //local state
  const [open, setOpen] = useState(false);
  const [page, setPage] = useState(1);
  const [showId, setShowId] = useState(null);

  const { pagination } = useSelector((state) => state.candidate);

  const dispatch = useDispatch();

  const handleChangeSearchKey = (e) => {
    if (e.target.value === "") {
      dispatch(getListCandidateThunk());
    } else {
      dispatch(
        getListCandidateThunk({ lastName: e.target.value, firstName: null })
      );
    }
  };

  return (
    <>
      <div className="admin-page">
        <div className="admin-page__page-header">
          <PageHeader
            title="Candidate"
            subTitle="Admin / Candidate management"
          />
          <Tooltip title="Add Candidate">
            <IconButton
              aria-label="add"
              onClick={() => {
                setShowId(null);
                setOpen(true);
              }}
            >
              <AddIcon />
            </IconButton>
          </Tooltip>
        </div>
        <Paper className="admin-page__page-tabs">
          <div className="admin-page__page-tabs-search">
            <TextField
              placeholder="Placeholder"
              onChange={handleChangeSearchKey}
              fullWidth
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <SearchIcon />
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <CandidateTable
            showId={showId}
            setShowId={setShowId}
            open={open}
            setOpen={setOpen}
          />
        </Paper>
        <div className="admin-page__page-pagination">
          <Pagination
            count={pagination.totalPages}
            page={page}
            onChange={(e, value) => {
              setPage(value);
              dispatch(getListCandidateThunk({ no: value - 1 }));
            }}
          />
        </div>
      </div>
    </>
  );
};

export default Candidate;
