import React, { useState, useContext } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Divider, CircularProgress } from "@mui/material";
import PermIdentityIcon from "@mui/icons-material/PermIdentity";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import { useNavigate, NavLink } from "react-router-dom";
import { toast } from "react-toastify";

import "./styles.scss";
import CustomInput from "../../../components/CustomInput";
import globalAPI from "../../../config/api/globalAPI";
import GoogleIcon from "@mui/icons-material/Google";

import { AuthenticationContext } from "../../../App";

const { signIn } = globalAPI;

const schema = yup.object({
  username: yup.string().required("Username is a required field."),
  password: yup.string().required("Password is a required field."),
});

const SignIn = () => {
  const [status, setStatus] = useState("idle");
  const { setAuth } = useContext(AuthenticationContext);

  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    setStatus("loading");
    signIn(data.username, data.password)
      .then((res) => {
        console.log(res);
        if (res.code >= 400) {
          setStatus("error");
          toast.error(res.message);
        } else {
          setStatus("success");
          toast.success(res.message);
          localStorage.setItem("userAccount", JSON.stringify(res));
          setAuth(true);
          navigate("/");
        }
      })
      .catch((err) => {
        setStatus("error");
        toast.error(err.message);
      });
  };

  return (
    <>
      <div className="sign-in">
        <form className="account-form__main" autoComplete="off">
          <CustomInput
            name="username"
            register={register}
            errors={errors}
            placeholder="Username"
            startIcon={<PermIdentityIcon />}
          />
          <CustomInput
            name="password"
            register={register}
            type="password"
            errors={errors}
            placeholder="Password"
            startIcon={<LockOpenIcon />}
          />
        </form>

        <div className="account-form__foot">
          <NavLink to="/account/forget-password">Forgot Password?</NavLink>
          {status === "loading" ? (
            <CircularProgress color="inherit" />
          ) : (
            <button type="button" onClick={handleSubmit(onSubmit)}>
              Sign In
            </button>
          )}
        </div>
        <Divider>Or</Divider>
        <div className="account-form__sign-up">
          <button
            type="button"
            onClick={() =>
              window.location.replace(
                "https://accounts.google.com/o/oauth2/auth?scope=email&redirect_uri=http://localhost:8080/login&response_type=code&client_id=960937734510-48v4403fs2peo7duacsfp93dq0qcalr9.apps.googleusercontent.com&approval_prompt=force"
              )
            }
          >
            <GoogleIcon />
            Sign In with Google account
          </button>
        </div>
        <Divider>Don't have an account?</Divider>
        <div className="account-form__sign-up">
          <button
            type="button"
            onClick={() => navigate("/account/choose-role")}
          >
            Sign Up
          </button>
        </div>
      </div>
    </>
  );
};

export default SignIn;
