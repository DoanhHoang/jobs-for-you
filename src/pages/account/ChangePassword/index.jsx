import React, { useContext, useState } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { CircularProgress } from "@mui/material";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import userAPI from "../../../config/api/user/userAPI";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

import { CustomInput } from "../../../components";
import { AuthenticationContext } from "../../../App";
import { handleLogOut } from "../../../utils/index";

const { changePassword } = userAPI;

const schema = yup.object({
  oldPassword: yup.string().required("Username is a required field."),
  newPassword: yup.string().required("Password is a required field."),
});

const ChangePassword = () => {
  const [status, setStatus] = useState("idle");
  const { setAuth } = useContext(AuthenticationContext);
  const navigate = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const { userDetail } = useSelector((state) => state.user);

  const onSubmit = (data) => {
    setStatus("loading");
    changePassword(userDetail.id, data)
      .then(() => {
        setStatus("success");
        toast.success("Change password successfully");
        handleLogOut(setAuth);
        navigate("/account/sign-in");
      })
      .catch((err) => {
        setStatus("error");
        toast.error(err.response.data.message);
      });
  };
  return (
    <>
      <div className="forget-password">
        <form className="account-form__main" autoComplete="off">
          <CustomInput
            name="oldPassword"
            register={register}
            type="password"
            errors={errors}
            placeholder="Old Password"
            startIcon={<LockOpenIcon />}
          />
          <CustomInput
            name="newPassword"
            register={register}
            type="password"
            errors={errors}
            placeholder="New Password"
            startIcon={<LockOpenIcon />}
          />
        </form>

        <div className="account-form__foot">
          {status === "loading" ? (
            <CircularProgress color="inherit" />
          ) : (
            <button type="button" onClick={handleSubmit(onSubmit)}>
              Change Password
            </button>
          )}
        </div>
      </div>
    </>
  );
};

export default ChangePassword;
