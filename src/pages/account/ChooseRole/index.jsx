import React, { useState } from "react";
import { Divider } from "@mui/material";
import { useNavigate } from "react-router-dom";

import "./styles.scss";
import ListRole from "./ListRole";

import clientLogo from "../../../asset/img/client.png";
import freelancerLogo from "../../../asset/img/freelancer.png";

export const RoleContext = React.createContext();

const listRole = [
  {
    id: 1,
    image: clientLogo,
    name: "Human resources",
    subTitle: "I’m a client, hiring for a project",
  },
  {
    id: 2,
    image: freelancerLogo,
    name: "Candidate",
    subTitle: "I’m a candidate, looking for work",
  },
  {
    id: 3,
    image: freelancerLogo,
    name: "Partner",
    subTitle: "I’m a Partner, looking for place to work",
  },
];

const ChooseRole = () => {
  const [roleId, setRoleId] = useState(2);
  const [title, setTitle] = useState("I’m a candidate, looking for work");

  const navigate = useNavigate();

  const setRoleButton = (roleId) => {
    const checkRoleId = listRole.findIndex((role) => role.id === roleId);

    if (checkRoleId >= 0) {
      setRoleId(listRole[checkRoleId].id);
      setTitle(listRole[checkRoleId].subTitle);
    }
  };
  return (
    <RoleContext.Provider
      value={{ roleId, setRoleId, title, setTitle, setRoleButton }}
    >
      <div className="choose-role">
        <div className="choose-role__header">
          <h1>Join as a client or freelancer</h1>
        </div>
        <div className="choose-role__main">
          <ListRole listRole={listRole} />
        </div>
        <div className="choose-role__footer">
          <button
            type="button"
            disabled={roleId === null ? true : false}
            onClick={() => navigate(`/account/sign-up/${roleId}`)}
          >
            {title}
          </button>
        </div>
        <Divider>Don't have an account?</Divider>
        <div className="account-form__sign-up">
          <button type="button" onClick={() => navigate("/account/sign-in")}>
            Sign In
          </button>
        </div>
      </div>
    </RoleContext.Provider>
  );
};

export default ChooseRole;
