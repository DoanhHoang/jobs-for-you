import React, { useContext, useState, useEffect } from "react";

import "./styles.scss";

import { RoleContext } from "../index";

const Role = ({ role }) => {
  const [selected, setSelected] = useState(false);
  const { roleId, setRoleButton } = useContext(RoleContext);

  const handleClick = () => {
    setRoleButton(role.id);
  };

  useEffect(() => {
    roleId === role.id ? setSelected(true) : setSelected(false);
  }, [roleId]);

  return (
    <>
      <div
        className={"role " + (selected ? "selected" : "")}
        onClick={handleClick}
      >
        <div className="role-title">
          <img src={role.image} alt="logo-role" />
          <h2>{role.name}</h2>
        </div>
        <p>{role.subTitle}</p>
      </div>
    </>
  );
};

export default Role;
