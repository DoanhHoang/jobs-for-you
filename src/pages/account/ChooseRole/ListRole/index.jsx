import React from "react";

import "./styles.scss";
import Role from "../Role";

const ListRole = ({ listRole }) => {
  return (
    <>
      <div className="list-role">
        {listRole?.map((role) => (
          <Role role={role} key={role.id} />
        ))}
      </div>
    </>
  );
};

export default ListRole;
