import React, { useContext } from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import { useParams, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { handleLogOut } from "../../../utils/index";
import { AuthenticationContext } from "../../../App";

import "./styles.scss";
import { CustomInput } from "../../../components";
import userAPI from "../../../config/api/user/userAPI";

const schema = yup.object({
  password: yup.string().required("Password is a required field."),
  confirmPassword: yup
    .string()
    .required("conFirmPassword is a required field."),
});

const { resetPassword } = userAPI;

const ResetPassword = () => {
  const { email, verifyCode } = useParams();

  const { setAuth } = useContext(AuthenticationContext);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    resetPassword(email, verifyCode, {
      password: data.password,
    })
      .then(() => {
        toast.success("Reset password success.");
        handleLogOut(setAuth, dispatch, navigate);
      })
      .catch(() => {
        toast.error("Reset password failed.");
      });
  };

  return (
    <>
      <div className="reset-password">
        <form className="account-form__main" autoComplete="off">
          <CustomInput
            name="password"
            register={register}
            errors={errors}
            placeholder="Password"
            startIcon={<LockOpenIcon />}
          />
          <CustomInput
            name="confirmPassword"
            register={register}
            errors={errors}
            placeholder="Confirm Password"
            startIcon={<LockOpenIcon />}
          />
        </form>

        <div className="account-form__foot">
          <button type="button" onClick={handleSubmit(onSubmit)}>
            Reset Password
          </button>
        </div>
      </div>
    </>
  );
};

export default ResetPassword;
