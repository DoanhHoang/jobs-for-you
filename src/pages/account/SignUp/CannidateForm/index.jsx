import React, { useState, useEffect } from "react";
import { Grid, Avatar, CircularProgress } from "@mui/material";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigate } from "react-router-dom";
import PermIdentityIcon from "@mui/icons-material/PermIdentity";
import LockOpenIcon from "@mui/icons-material/LockOpen";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import BadgeOutlinedIcon from "@mui/icons-material/BadgeOutlined";
import ContactPhoneOutlinedIcon from "@mui/icons-material/ContactPhoneOutlined";

import "./styles.scss";
import cameraLogo from "../../../../asset/img/camera.png";
import cvLogo from "../../../../asset/img/cv.png";
import { CustomInput, SelectCustom } from "../../../../components";
import { schema, handleCandidateRegister } from "./script";
import globalAPI from "../../../../config/api/globalAPI";

const { getAllMajors } = globalAPI;

const CannidateForm = () => {
  const [image, setImage] = useState(cameraLogo);
  const [cv, setCv] = useState("");
  const [listMajors, setListMajors] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();

  const methods = useForm({
    resolver: yupResolver(schema),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = methods;

  useEffect(() => {
    getAllMajors().then((res) => {
      setListMajors(res);
    });
  }, []);

  // show preview image
  const showPreviewImage = (e) => {
    if (e.target.files && e.target.files[0]) {
      let imageFile = e.target.files[0];
      const reader = new FileReader();
      reader.onload = (x) => {
        setImage(x.target.result);
      };
      reader.readAsDataURL(imageFile);
    }
  };
  // show preview cv
  const showPreviewCV = (e) => {
    if (e.target.files && e.target.files[0]) {
      let cvFile = e.target.files[0];
      setCv(cvFile.name);
    }
  };

  const onSubmit = (data) => {
    handleCandidateRegister(data, false, navigate, setIsLoading);
  };

  return (
    <FormProvider {...methods}>
      <form
        className="cannidate-form"
        encType="multipart/form-data"
        autoComplete="off"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Grid container spacing={3}>
          <Grid item md={12}>
            <h3>Avatar</h3>
            <label htmlFor="avatar">
              <div className="cannidate-form__logo">
                <Avatar
                  src={image}
                  variant="rounded"
                  alt="cannidate-logo"
                  className="cannidate-form__logo-avatar"
                />
              </div>
            </label>
            <input
              className="cannidate-form__file-input"
              id="avatar"
              type="file"
              name="avatar"
              {...register("avatar")}
              onChange={showPreviewImage}
            />
            <p className="cannidate-form__error">{errors.avatar?.message}</p>
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="firstName"
              register={register}
              errors={errors}
              placeholder="First Name"
              title="First Name"
              startIcon={<BadgeOutlinedIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="lastName"
              register={register}
              errors={errors}
              placeholder="Last Name"
              title="Last Name"
              startIcon={<BadgeOutlinedIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="username"
              register={register}
              errors={errors}
              placeholder="Username"
              title="Username"
              startIcon={<PermIdentityIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="email"
              register={register}
              errors={errors}
              placeholder="Email"
              title="Email"
              startIcon={<MailOutlineIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="password"
              register={register}
              type="password"
              errors={errors}
              placeholder="Password"
              title="Password"
              startIcon={<LockOpenIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="comfirmPassword"
              register={register}
              type="password"
              errors={errors}
              placeholder="Comfirm Password"
              title="Comfirm Password"
              startIcon={<LockOpenIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="phone"
              register={register}
              errors={errors}
              placeholder="Phone"
              title="Phone"
              startIcon={<ContactPhoneOutlinedIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <label htmlFor="cv">
              <div className="cannidate-form__cv">
                <h3>CV</h3>
                <div className="cannidate-form__cv-file">
                  <img src={cvLogo} alt="cv-logo" />
                  <p>{cv || "choose file"}</p>
                </div>
              </div>
            </label>
            <input
              className="cannidate-form__file-input"
              id="cv"
              type="file"
              name="cv"
              {...register("cv")}
              onChange={showPreviewCV}
            />
            <p className="cannidate-form__error">{errors.cv?.message}</p>
          </Grid>
          <Grid item md={6}>
            <SelectCustom
              id="major"
              label="Major"
              placeholder="Please choose option"
              options={listMajors}
              register={register}
            >
              {errors.major?.message}
            </SelectCustom>
          </Grid>
          <Grid item md={6}>
            <SelectCustom
              id="gender"
              label="Gender"
              placeholder="Please choose option"
              options={[
                {
                  id: 0,
                  name: "Male",
                },
                {
                  id: 1,
                  name: "Female",
                },
              ]}
              register={register}
            >
              {errors.major?.message}
            </SelectCustom>
          </Grid>
        </Grid>
      </form>
      <div className="account-form__foot">
        {isLoading ? (
          <CircularProgress />
        ) : (
          <button type="button" onClick={handleSubmit(onSubmit)}>
            Sign Up
          </button>
        )}
      </div>
    </FormProvider>
  );
};

export default CannidateForm;
