import React from "react";
import { useParams } from "react-router-dom";

import CompanyForm from "./CompanyForm";
import CannidateForm from "./CannidateForm";
import UniversityForm from "./UniversityForm";

const SignUp = () => {
  const { roleId } = useParams();

  return (
    <>
      <div className="sign-up">
        {roleId === "1" && <CompanyForm />}
        {roleId === "2" && <CannidateForm />}
        {roleId === "3" && <UniversityForm />}
      </div>
    </>
  );
};

export default SignUp;
