import React, { useState, useEffect } from "react";
import { Grid, CircularProgress } from "@mui/material";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useNavigate } from "react-router-dom";

import "./styles.scss";
import { CustomInput, SelectCustom } from "../../../../components";
import { schema } from "./script";
import { handleCompanyRegister } from "./script";

import locationAPI from "../../../../config/api/location/locationAPI";

const { getProvinceList } = locationAPI;

const UniversityForm = () => {
  const [listProvince, setListProvince] = useState([]);
  const [listDistrict, setListDistrict] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const navigate = useNavigate();

  const methods = useForm({
    resolver: yupResolver(schema),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = methods;

  useEffect(() => {
    getProvinceList().then((res) => {
      setListProvince(res.data);
    });
  }, []);

  const onSubmit = (data) => {
    handleCompanyRegister({ isAdd: true, data, navigate, setIsLoading });
  };

  return (
    <FormProvider {...methods}>
      <form
        className="company-form"
        autoComplete="off"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Grid container spacing={3}>
          <Grid item md={12}>
            <h3>University Info</h3>
          </Grid>
          <Grid item md={12}>
            <CustomInput
              name="name"
              register={register}
              title="Name"
              placeholder="Name"
              errors={errors}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="email"
              register={register}
              title="Email"
              placeholder="Email"
              errors={errors}
            />
            <CustomInput
              name="phone"
              register={register}
              title="Phone"
              placeholder="Phone"
              errors={errors}
            />
            <SelectCustom
              id="province"
              label="Province/City"
              placeholder="please choose option"
              // action={getDistrictList}
              options={listProvince}
              setListDistrict={setListDistrict}
            >
              {errors.province?.message}
            </SelectCustom>
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="website"
              register={register}
              title="Website"
              placeholder="Website"
              errors={errors}
            />
            <CustomInput
              name="shortName"
              register={register}
              title="Short Name"
              placeholder="Short Name"
              errors={errors}
            />

            <SelectCustom
              id="district"
              label="District"
              placeholder="please choose option"
              options={listDistrict}
              register={register}
            >
              {errors.district?.message}
            </SelectCustom>
          </Grid>
          <Grid item md={12}>
            <CustomInput
              name="address"
              register={register}
              title="Address"
              placeholder="Address"
              errors={errors}
            />
          </Grid>
          <Grid item md={12}>
            <CustomInput
              title="Description"
              register={register}
              name="description"
              multiline={true}
              minRows={4}
            />
          </Grid>

          <Grid item md={12}>
            <h3>User Info</h3>
          </Grid>
          <Grid item md={12}>
            <CustomInput
              name="username"
              register={register}
              errors={errors}
              placeholder="Username"
              title="Username"
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="password"
              register={register}
              type="password"
              errors={errors}
              placeholder="Password"
              title="Password"
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="confirmPassword"
              register={register}
              type="password"
              errors={errors}
              placeholder="Confirm Password"
              title="Confirm Password"
            />
          </Grid>
        </Grid>
      </form>
      <div className="account-form__foot">
        {isLoading ? (
          <CircularProgress />
        ) : (
          <button type="button" onClick={handleSubmit(onSubmit)}>
            Sign Up
          </button>
        )}
      </div>
    </FormProvider>
  );
};

export default UniversityForm;
