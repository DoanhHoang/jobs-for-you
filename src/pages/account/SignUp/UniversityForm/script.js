import * as yup from "yup";
import { toast } from "react-toastify";
import universityAPI from "../../../../config/api/university/universityAPI";

const { universityRegister } = universityAPI;

// yup validation for company table
export const schema = yup
  .object({
    logo: yup.mixed().nullable(),
    // .test(
    //   "FILE_SIZE",
    //   "Uploaded file is too bid!",
    //   (value) => !value || (value && value.size <= 1024 * 1024)
    // ),
    name: yup.string().required(" * Bạn phải nhập tên công ty."),
    website: yup.string().required(" * Bạn phải nhập tên website công ty."),
    email: yup
      .string(" * email không hợp lệ.")
      // .email(" * Ban nhap email khong dung")
      .matches(
        /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
        " * Email khong hop le"
      )
      .required(" * Bạn phải nhập email công ty."),
    phone: yup
      .string()
      .matches(
        /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/,
        " * Số điện thoại không đúng."
      ),
    description: yup.string(),
    address: yup.string(),
    shortName: yup.string(),
    province: yup.string().required(" * Bạn phải nhập dia chi"),
    district: yup.string().required(" * Bạn phải nhập quan/huyen"),
    username: yup.string().required(" * This field is required."),
    password: yup.string().required(" * This field is required."),
    confirmPassword: yup.string().required(" * This field is required."),
  })
  .required();

// const defaultValue = {
//   name: "",
//   description: "",
//   website: "",
//   email: "",
//   tax: "",
//   phone: "",
//   logo: "",
//   address: "",
//   province: "",
//   district: "",
//   username: "",
//   password: "",
//   confirmPassword: "",
//   code: "",
// };

/**
 * Add or Edit company
 * @param {*} isAdd
 * @param {*} data
 * @param {*} id
 * @param {*} reset
 */
export const handleCompanyRegister = async ({
  isAdd,
  data,
  navigate,
  setIsLoading,
}) => {
  const universityData = {
    university: JSON.stringify({
      name: data.name,
      description: data.description,
      website: data.website,
      email: data.email,
      phone: data.phone,
      shortName: data.shortName,
      locations: [
        {
          address: data.address,
          district: {
            id: parseInt(data.district),
          },
        },
      ],
    }),
    user: JSON.stringify({
      username: data.username,
      password: data.password,
      confirmPassword: data.confirmPassword,
      role: [
        {
          id: 2,
        },
      ],
    }),
  };

  // console.log(data, companyData);

  if (isAdd) {
    await universityRegister(universityData);
    setIsLoading(true)
      .then(() => {
        toast.success("Successfully registered");
        navigate("/account/sign-in");
        setIsLoading(false);
      })
      .catch(() => {
        toast.error("Failed registered");
        setIsLoading(false);
      });
  }
};
