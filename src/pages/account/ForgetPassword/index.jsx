import React from "react";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import { toast } from "react-toastify";

import { CustomInput } from "../../../components";

import userAPI from "../../../config/api/user/userAPI";

const { forgetPassword } = userAPI;

const schema = yup.object({
  email: yup.string().required("email is a required field."),
});

const ForgetPassword = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    console.log(data);
    forgetPassword(data.email)
      .then((res) => {
        toast.success(res.message);
      })
      .catch((err) => {
        toast.error(err.response.data.message);
      });
  };

  return (
    <>
      <div className="forget-password">
        <form className="account-form__main" autoComplete="off">
          <CustomInput
            name="email"
            register={register}
            errors={errors}
            placeholder="Email"
            startIcon={<MailOutlineIcon />}
          />
        </form>

        <div className="account-form__foot">
          <button type="button" onClick={handleSubmit(onSubmit)}>
            Get password
          </button>
        </div>
      </div>
    </>
  );
};

export default ForgetPassword;
