import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useDispatch } from "react-redux";

import AdminLayout from "./layouts/admin";
import MainLayout from "./layouts/main";
import AccountLayout from "./layouts/account";
import {
  adminRouter,
  clientRouter,
  accountRouter,
  passRouter,
  authRouter,
} from "./config/router";

import PrivateRoutes from "./config/PrivateRoutes";
import {
  getUserDetailByIdThunk,
  getRoleDetailByIdThunk,
} from "./store/slices/userSlice";

export const AuthenticationContext = React.createContext();

const App = () => {
  const userAccount = JSON.parse(localStorage.getItem("userAccount"));
  const adminAccount = JSON.parse(sessionStorage.getItem("adminAccount"));
  const [auth, setAuth] = useState(userAccount ? true : false);
  const [adminAuth, setAdminAuth] = useState(adminAccount ? true : false);
  // const [adminAuth, setAdminAuth] = useState(true);

  const dispatch = useDispatch();

  useEffect(() => {
    if (userAccount) {
      dispatch(getUserDetailByIdThunk(userAccount.id));
      dispatch(
        getRoleDetailByIdThunk({
          id: userAccount.id,
          roles: userAccount.roles,
        })
      );
    }
  }, [userAccount, dispatch]);

  // render layout
  const renderLayout = (listRouter) => {
    return listRouter.map(({ path, Component, id }) => {
      return <Route path={path} element={<Component />} key={id} />;
    });
  };

  return (
    <AuthenticationContext.Provider
      value={{ setAuth, auth, adminAuth, setAdminAuth }}
    >
      <Router>
        <Routes>
          <Route
            element={
              <PrivateRoutes redirectLink="auth/sign-in" check={adminAuth} />
            }
          >
            <Route path="/admin" element={<AdminLayout />}>
              {renderLayout(adminRouter)}
            </Route>
          </Route>
          <Route
            element={<PrivateRoutes redirectLink="/admin" check={!adminAuth} />}
          >
            <Route path="/auth" element={<AccountLayout />}>
              {renderLayout(authRouter)}
            </Route>
          </Route>

          <Route element={<PrivateRoutes redirectLink="/" check={!auth} />}>
            <Route path="/account" element={<AccountLayout />}>
              {renderLayout(accountRouter)}
            </Route>
          </Route>
          <Route path="/pass" element={<AccountLayout />}>
            {renderLayout(passRouter)}
          </Route>
          <Route path="" element={<MainLayout />}>
            {renderLayout(clientRouter)}
          </Route>
          {/* <Route path="*" element={<NotFound />} /> */}
        </Routes>
      </Router>
    </AuthenticationContext.Provider>
  );
};

export default App;
