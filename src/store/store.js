import { configureStore } from "@reduxjs/toolkit";
import notificationSlice from "./slices/NotificationSlice";
import companySlice from "./slices/CompanySlice";
import UniversitySlice from "./slices/UniversitySlice";
import JobSlice from "./slices/JobSlice";
import CandidateSlice from "./slices/CandidateSlice";
import userSlice from "./slices/userSlice";
import DemandSlice from "./slices/DemandSlice";

const store = configureStore({
  reducer: {
    notification: notificationSlice.reducer,
    company: companySlice.reducer,
    university: UniversitySlice.reducer,
    job: JobSlice.reducer,
    candidate: CandidateSlice.reducer,
    user: userSlice.reducer,
    demand: DemandSlice.reducer,
  },
  // middleware: (getDefaultMiddleware) => getDefaultMiddleware(),
});

export default store;
