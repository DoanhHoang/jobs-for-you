import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import demandAPI from "../../config/api/demand/demandAPI";

const { getAllDemand, getDemandDetail } = demandAPI;

const initialPagination = {
  totalPages: 0,
  totalItems: 0,
  sizeCurrentItems: 0,
  numberOfCurrentPage: 0,
};

const DemandSlice = createSlice({
  name: "demand",
  initialState: {
    status: "idle",
    demandList: [],
    demandDetail: {},
    pagination: initialPagination,
    filtersDemand: {
      province: null,
      district: null,
      type: 0,
      company: null,
      name: null,
      no: 0,
      limit: 10,
    },
    error: [],
  },
  reducers: {
    updateFilter: (state, action) => {
      state.filtersDemand = { ...state.filtersDemand, ...action.payload };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getDemandListThunk.fulfilled, (state, { payload }) => {
      state.demandList = payload;
    });
    builder.addCase(getDemandDetailThunk.fulfilled, (state, { payload }) => {
      state.demandDetail = payload;
    });
  },
});

export const { updateFilter } = DemandSlice.actions;
export default DemandSlice;

/**
 * actions:
 * getDemandListThunk()
 * getDemandDetailThunk()
 * getDemandsLikeByCandidateIdThunk()
 */

export const getDemandListThunk = createAsyncThunk(
  "demand/getDemandListThunk",
  async () => {
    const res = await getAllDemand();

    return res;
  }
);

export const getDemandDetailThunk = createAsyncThunk(
  "demand/getDemandDetailThunk",
  async (id) => {
    const res = await getDemandDetail(id);

    return res;
  }
);
