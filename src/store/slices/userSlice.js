import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import userAPI from "../../config/api/user/userAPI";
import { ROLE_NAME } from "../../config/constant/roleNameConstant";
import { checkRole } from "../../utils";

const {
  getUserDetailById,
  getCandidateDetailByUserId,
  getCompanyDetailByUserId,
  getUniversityDetailByUserId,
  getAllUsers,
  getUsersByRoleId,
} = userAPI;

const initialPagination = {
  totalPages: 0,
  totalItems: 0,
  sizeCurrentItems: 0,
  numberOfCurrentPage: 0,
};

const userSlice = createSlice({
  name: "user",
  initialState: {
    status: "idle",
    listUser: [],
    pagination: initialPagination,
    userDetail: {},
    roleDetail: {},
  },
  reducers: {
    logOut: (state) => {
      state.userDetail = {};
      state.roleDetail = {};
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getUserDetailByIdThunk.fulfilled, (state, { payload }) => {
      state.userDetail = payload;
    });
    builder.addCase(getRoleDetailByIdThunk.fulfilled, (state, { payload }) => {
      state.roleDetail = payload ? payload.data : {};
    });
    builder.addCase(getUserListThunk.fulfilled, (state, { payload }) => {
      state.status = "success";
      state.listUser = payload.data.data;
      state.pagination = {
        totalPages: payload.data.totalPages,
        totalItems: payload.data.totalItems,
        sizeCurrentItems: payload.data.sizeCurrentItems,
        numberOfCurrentPage: payload.data.numberOfCurrentPage,
      };
    });
    builder.addCase(getUsersByRoleIdThunk.fulfilled, (state, { payload }) => {
      state.status = "success";
      state.listUser = payload.data.data;
      state.pagination = {
        totalPages: payload.data.totalPages,
        totalItems: payload.data.totalItems,
        sizeCurrentItems: payload.data.sizeCurrentItems,
        numberOfCurrentPage: payload.data.numberOfCurrentPage,
      };
    });
  },
});

export default userSlice;

/**
 * actions:
 * getUserDetailByIdThunk()
 */

export const getUserDetailByIdThunk = createAsyncThunk(
  "user/getUserDetailByIdThunk",
  async (id) => {
    const res = await getUserDetailById(id);

    return res;
  }
);

export const getRoleDetailByIdThunk = createAsyncThunk(
  "user/getRoleDetailByIdThunk",
  async (data) => {
    const { id, roles } = data;
    if (checkRole(roles, ROLE_NAME.CANDIDATE)) {
      const res = await getCandidateDetailByUserId(id);
      return res;
    }
    if (checkRole(roles, ROLE_NAME.COMPANY)) {
      const res = await getCompanyDetailByUserId(id);
      return res;
    }
    if (checkRole(roles, ROLE_NAME.UNIVERSITY)) {
      const res = await getUniversityDetailByUserId(id);
      return res;
    }
  }
);

export const getUserListThunk = createAsyncThunk(
  "user/getUserListThunk",
  async (data) => {
    const res = await getAllUsers(data);

    return res;
  }
);

export const getUsersByRoleIdThunk = createAsyncThunk(
  "user/getUsersByRoleIdThunk",
  async (id) => {
    const res = await getUsersByRoleId(id);

    return res;
  }
);
