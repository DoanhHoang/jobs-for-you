import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import cannidateAPI from "../../config/api/cannidate/cannidateAPI";

const { getListCandidate, getCannidateDetail } = cannidateAPI;

const initialPagination = {
  totalPages: 0,
  totalItems: 0,
  sizeCurrentItems: 0,
  numberOfCurrentPage: 0,
};

const CandidateSlice = createSlice({
  name: "candidate",
  initialState: {
    status: "idle",
    listCandidate: [],
    candidateDetail: {},
    pagination: initialPagination,
    filters: {
      firstName: "",
      lastName: "",
      no: 0,
      limit: 9,
    },
    error: [],
  },
  reducers: {
    updateFilter: (state, action) => {
      state.filters = { ...state.filters, ...action.payload };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getListCandidateThunk.fulfilled, (state, { payload }) => {
      state.status = "success";
      state.listCandidate = payload.data.data;
      state.pagination = {
        totalPages: payload.data.totalPages,
        totalItems: payload.data.totalItems,
        sizeCurrentItems: payload.data.sizeCurrentItems,
        numberOfCurrentPage: payload.data.numberOfCurrentPage,
      };
    });
    builder.addCase(getListCandidateThunk.pending, (state) => {
      state.status = "loading";
    });
    builder.addCase(getListCandidateThunk.rejected, (state) => {
      state.status = "error";
    });
    builder.addCase(getCandidateDetailThunk.fulfilled, (state, { payload }) => {
      state.candidateDetail = payload;
    });
  },
});

export const { updateFilter } = CandidateSlice.actions;
export default CandidateSlice;

/**
 * actions
 * getListCandidateThunk()
 */

export const getListCandidateThunk = createAsyncThunk(
  "candidate/getListCandidateThunk",
  async (data, thunkAPI) => {
    const filterState = thunkAPI.getState().candidate.filters;
    const filterCandidate = { ...filterState, ...data };
    const res = await getListCandidate({ ...filterCandidate });

    return res;
  }
);

export const getCandidateDetailThunk = createAsyncThunk(
  "candidate/getCandidateDetailThunk",
  async (id) => {
    const res = await getCannidateDetail(id);

    return res;
  }
);
