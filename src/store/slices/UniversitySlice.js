import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import universityAPI from "../../config/api/university/universityAPI";

const {
  getUniversityList,
  getUniversityDetail,
  filterUniversityByStatus,
  searchUniversityByNameStatus,
} = universityAPI;

const initialPagination = {
  totalPages: 0,
  totalItems: 0,
  sizeCurrentItems: 0,
  numberOfCurrentPage: 0,
};

const UniversitySlice = createSlice({
  name: "university",
  initialState: {
    universityList: [],
    universityDetail: {},
    error: [],
    status: "idle",
    pagination: initialPagination,
    filtersUni: {
      statusId: 0,
      name: null,
      no: 0,
      limit: 6,
    },
  },
  reducers: {
    updateFilterUniversity: (state, action) => {
      state.filtersUni = { ...state.filtersUni, ...action.payload };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getUniversityListThunk.fulfilled, (state, { payload }) => {
      state.universityList = payload.data;
    });
    builder.addCase(
      getUniversityDetailThunk.fulfilled,
      (state, { payload }) => {
        state.universityDetail = payload;
      }
    );
    builder.addCase(getUnibyStatusIDThunk.fulfilled, (state, { payload }) => {
      state.universityList = payload.contents;
    });
    builder.addCase(
      searchUniversityByNameStatusThunk.fulfilled,
      (state, { payload }) => {
        state.status = "success";
        state.universityList = payload.data;
        state.pagination = {
          totalPages: payload.totalPages,
          totalItems: payload.totalItems,
          sizeCurrentItems: payload.sizeCurrentItems,
          numberOfCurrentPage: payload.numberOfCurrentPage,
        };
      }
    );
    builder.addCase(searchUniversityByNameStatusThunk.pending, (state) => {
      state.status = "loading";
    });
    builder.addCase(searchUniversityByNameStatusThunk.rejected, (state) => {
      state.status = "error";
    });
  },
});

export const { updateFilterUniversity } = UniversitySlice.actions;
export default UniversitySlice;

/**
 * get university list
 * @returns university list
 */
export const getUniversityListThunk = createAsyncThunk(
  "university/getUniversityListThunk",
  async (data) => {
    const { no, limit } = data;
    const res = await getUniversityList(no, limit);
    return res;
  }
);

/**
 * get univer detail by id
 * @params id
 * @return university detail
 */
export const getUniversityDetailThunk = createAsyncThunk(
  "university/getUniversityDetailThunk",
  async (id) => {
    const res = await getUniversityDetail(id);

    return res;
  }
);

export const getUnibyStatusIDThunk = createAsyncThunk(
  "university/getUnibyStatusIDThunk",
  async (statusId) => {
    const res = await filterUniversityByStatus(statusId);

    return res;
  }
);

export const searchUniversityByNameStatusThunk = createAsyncThunk(
  "university/searchUniversityByNameStatusThunk",
  async (data, thunkAPI) => {
    const filterState = thunkAPI.getState().university.filtersUni;
    const filterUniversity = { ...filterState, ...data };
    const res = await searchUniversityByNameStatus({ ...filterUniversity });

    return res;
  }
);
