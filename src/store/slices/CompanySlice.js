import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import companyAPI from "../../config/api/company/companyAPI";

const { getCompanyDetail, searchCompanyByNameStatus, getCompanyList } =
  companyAPI;

const initialPagination = {
  totalPages: 0,
  totalItems: 0,
  sizeCurrentItems: 0,
  numberOfCurrentPage: 0,
};

const companySlice = createSlice({
  name: "company",
  initialState: {
    status: "idle",
    companyList: [],
    companyDetail: {},
    pagination: initialPagination,
    filtersCom: {
      statusId: 0,
      name: null,
      no: 0,
      limit: 6,
    },
    error: [],
  },
  reducers: {
    updateFilterCompany: (state, action) => {
      state.filtersCom = { ...state.filtersCom, ...action.payload };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getCompanyDetailThunk.fulfilled, (state, { payload }) => {
      state.companyDetail = payload;
    });
    builder.addCase(
      searchCompanyByNameStatusThunk.fulfilled,
      (state, { payload }) => {
        state.status = "success";
        state.companyList = payload.data;
        state.pagination = {
          totalPages: payload.totalPages,
          totalItems: payload.totalItems,
          sizeCurrentItems: payload.sizeCurrentItems,
          numberOfCurrentPage: payload.numberOfCurrentPage,
        };
      }
    );
    builder.addCase(searchCompanyByNameStatusThunk.pending, (state) => {
      state.status = "loading";
    });
    builder.addCase(searchCompanyByNameStatusThunk.rejected, (state) => {
      state.status = "error";
    });
    builder.addCase(getCompanyListThunk.fulfilled, (state, { payload }) => {
      state.status = "success";
      state.companyList = payload.data;
      state.pagination = {
        totalPages: payload.totalPages,
        totalItems: payload.totalItems,
        sizeCurrentItems: payload.sizeCurrentItems,
        numberOfCurrentPage: payload.numberOfCurrentPage,
      };
    });
    builder.addCase(getCompanyListThunk.pending, (state) => {
      state.status = "loading";
    });
    builder.addCase(getCompanyListThunk.rejected, (state) => {
      state.status = "error";
    });
  },
});

export const { updateFilterCompany } = companySlice.actions;
export default companySlice;

/**
 * actions
 * getCompanyDetailThunk()
 * searchCompanyByNameStatusThunk()
 * getCompanyListThunk()
 */

export const getCompanyListThunk = createAsyncThunk(
  "company/getCompanyListThunk",
  async (data) => {
    const { no, limit } = data;
    const res = await getCompanyList(no, limit);

    return res;
  }
);

export const getCompanyDetailThunk = createAsyncThunk(
  "company/getCompanyDetailThunk",
  async (comId) => {
    const res = await getCompanyDetail(comId);

    return res;
  }
);

export const searchCompanyByNameStatusThunk = createAsyncThunk(
  "company/searchCompanyByNameStatus",
  async (data, thunkAPI) => {
    const filterState = thunkAPI.getState().company.filtersCom;
    const filterCompany = { ...filterState, ...data };
    const res = await searchCompanyByNameStatus({ ...filterCompany });

    return res;
  }
);
