import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import jobAPI from "../../config/api/job/jobAPI";
import companyAPI from "../../config/api/company/companyAPI";

const { getJobByCompanyId } = companyAPI;

const {
  getJobList,
  getJobDetail,
  searchJob,
  getJobsLikeByCandidateId,
  getJobsApplyByCandidateId,
} = jobAPI;

const initialPagination = {
  totalPages: 0,
  totalItems: 0,
  sizeCurrentItems: 0,
  numberOfCurrentPage: 0,
};

const JobSlice = createSlice({
  name: "job",
  initialState: {
    status: "idle",
    jobList: [],
    jobLikeList: [],
    jobApplyList: [],
    jobDetail: {},
    pagination: initialPagination,
    filtersJob: {
      province: null,
      district: null,
      type: 0,
      company: null,
      name: null,
      no: 0,
      limit: 10,
    },
    error: [],
  },
  reducers: {
    updateFilter: (state, action) => {
      state.filtersJob = { ...state.filtersJob, ...action.payload };
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getJobListThunk.fulfilled, (state, { payload }) => {
      state.jobList = payload.data;
      state.pagination = {
        totalPages: payload.totalPages,
        totalItems: payload.totalItems,
        sizeCurrentItems: payload.sizeCurrentItems,
        numberOfCurrentPage: payload.numberOfCurrentPage,
      };
    });
    builder.addCase(getJobDetailThunk.fulfilled, (state, { payload }) => {
      state.jobDetail = payload;
    });
    builder.addCase(searchJobThunk.fulfilled, (state, { payload }) => {
      state.jobList = payload.data.data;
      state.pagination = {
        totalPages: payload.data.data.totalPages,
        totalItems: payload.data.data.totalItems,
        sizeCurrentItems: payload.data.data.sizeCurrentItems,
        numberOfCurrentPage: payload.data.data.numberOfCurrentPage,
      };
    });
    builder.addCase(getJobByCompanyIdThunk.fulfilled, (state, { payload }) => {
      state.jobList = payload.data;
      state.pagination = {
        totalPages: payload.totalPages,
        totalItems: payload.totalItems,
        sizeCurrentItems: payload.sizeCurrentItems,
        numberOfCurrentPage: payload.numberOfCurrentPage,
      };
    });
    builder.addCase(
      getJobsLikeByCandidateIdThunk.fulfilled,
      (state, { payload }) => {
        state.jobLikeList = payload.data.data;
        state.pagination = {
          totalPages: payload.totalPages,
          totalItems: payload.totalItems,
          sizeCurrentItems: payload.sizeCurrentItems,
          numberOfCurrentPage: payload.numberOfCurrentPage,
        };
      }
    );
    builder.addCase(
      getJobsApplyByCandidateIdThunk.fulfilled,
      (state, { payload }) => {
        state.jobApplyList = payload;
        state.pagination = {
          totalPages: payload.totalPages,
          totalItems: payload.totalItems,
          sizeCurrentItems: payload.sizeCurrentItems,
          numberOfCurrentPage: payload.numberOfCurrentPage,
        };
      }
    );
    builder.addCase(
      getJobsApplyByCandidateIdThunk.rejected,
      (state, { payload }) => {
        state.status = "error";
      }
    );
  },
});

export const { updateFilter } = JobSlice.actions;
export default JobSlice;

/**
 * actions:
 * getJobListThunk()
 * getJobDetailThunk()
 * getJobsLikeByCandidateIdThunk()
 */

export const getJobListThunk = createAsyncThunk(
  "job/getJobListThunk",
  async (data) => {
    const res = await getJobList(data);

    return res;
  }
);

export const getJobDetailThunk = createAsyncThunk(
  "job/getJobDetailThunk",
  async (id) => {
    const res = await getJobDetail(id);

    return res;
  }
);

export const searchJobThunk = createAsyncThunk(
  "job/searchJobThunk",
  async (data, thunkAPI) => {
    const filterState = thunkAPI.getState().job.filtersJob;
    const filterJob = { ...filterState, ...data };
    const res = await searchJob({ ...filterJob });

    return res;
  }
);

export const getJobByCompanyIdThunk = createAsyncThunk(
  "job/getJobByCompanyIdThunk",
  async (id) => {
    const res = await getJobByCompanyId(id);
    return res;
  }
);

export const getJobsLikeByCandidateIdThunk = createAsyncThunk(
  "job/getJobsLikeByCandidateIdThunk",
  async (id) => {
    const res = await getJobsLikeByCandidateId(id);
    return res;
  }
);

export const getJobsApplyByCandidateIdThunk = createAsyncThunk(
  "job/getJobsApplyByCandidateIdThunk",
  async (id) => {
    const res = await getJobsApplyByCandidateId(id);
    return res;
  }
);
