import React from "react";
import { useLocation } from "react-router-dom";

import "./styles.scss";

const PageHeader = ({ title, subTitle }) => {
  const location = useLocation();

  // console.log(location);

  return (
    <>
      <div className="page-header">
        <h3>{title}</h3>
        <p>{subTitle}</p>
      </div>
    </>
  );
};

export default PageHeader;
