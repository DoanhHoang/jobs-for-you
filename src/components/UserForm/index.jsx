import React, { useState, useEffect } from "react";
import { Grid, Avatar } from "@mui/material";
import { useForm, FormProvider } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import PermIdentityIcon from "@mui/icons-material/PermIdentity";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import BadgeOutlinedIcon from "@mui/icons-material/BadgeOutlined";
import ContactPhoneOutlinedIcon from "@mui/icons-material/ContactPhoneOutlined";

import "./styles.scss";
import cameraLogo from "../../asset/img/camera.png";
import { CustomInput, SelectCustom } from "../index";
import { schema, handleAddOrUpdateUser } from "./script";
import SelectMulti from "../SelectMulti";

import userAPI from "../../config/api/user/userAPI";

const { getUserDetailById } = userAPI;

const UserForm = ({ id, setOpen }) => {
  const [image, setImage] = useState(cameraLogo);

  const methods = useForm({
    resolver: yupResolver(schema),
  });

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = methods;

  useEffect(() => {
    if (id) {
      getUserDetailById(id).then((user) => {
        setValue("username", id ? user.username : "");
        setValue("firstName", id ? user.firstName : "");
        setValue("lastName", id ? user.lastName : "");
        setValue("email", id ? user.email : "");
        setValue("phone", id ? user.phone : "");
        setValue("gender", id ? user.gender : "");
      });
    }
  }, [id, setValue]);

  // show preview image
  const showPreviewImage = (e) => {
    if (e.target.files && e.target.files[0]) {
      let imageFile = e.target.files[0];
      const reader = new FileReader();
      reader.onload = (x) => {
        setImage(x.target.result);
      };
      reader.readAsDataURL(imageFile);
    }
  };

  const onSubmit = (data) => {
    handleAddOrUpdateUser(data, id, setOpen);
  };

  return (
    <FormProvider {...methods}>
      <form
        className="cannidate-form"
        encType="multipart/form-data"
        autoComplete="off"
        onSubmit={handleSubmit(onSubmit)}
      >
        <Grid container spacing={3}>
          <Grid item md={12}>
            <h3>Avatar</h3>
            <label htmlFor="avatar">
              <div className="cannidate-form__logo">
                <Avatar
                  src={image}
                  variant="rounded"
                  alt="cannidate-logo"
                  className="cannidate-form__logo-avatar"
                />
              </div>
            </label>
            <input
              className="cannidate-form__file-input"
              id="avatar"
              type="file"
              name="avatar"
              {...register("avatar")}
              onChange={showPreviewImage}
            />
            <p className="cannidate-form__error">{errors.avatar?.message}</p>
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="firstName"
              register={register}
              errors={errors}
              placeholder="First Name"
              title="First Name"
              startIcon={<BadgeOutlinedIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="lastName"
              register={register}
              errors={errors}
              placeholder="Last Name"
              title="Last Name"
              startIcon={<BadgeOutlinedIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="username"
              register={register}
              errors={errors}
              placeholder="Username"
              title="Username"
              startIcon={<PermIdentityIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="email"
              register={register}
              errors={errors}
              placeholder="Email"
              title="Email"
              startIcon={<MailOutlineIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <CustomInput
              name="phone"
              register={register}
              errors={errors}
              placeholder="Phone"
              title="Phone"
              startIcon={<ContactPhoneOutlinedIcon />}
            />
          </Grid>
          <Grid item md={6}>
            <SelectCustom
              id="gender"
              label="Gender"
              placeholder="Please choose option"
              options={[
                {
                  id: true,
                  name: "Male",
                },
                {
                  id: false,
                  name: "Female",
                },
              ]}
              register={register}
            >
              {errors.major?.message}
            </SelectCustom>
          </Grid>
          {/* <Grid item md={6}>
            <SelectMulti />
          </Grid> */}
        </Grid>
      </form>
      <div className="account-form__foot">
        <button type="button" onClick={handleSubmit(onSubmit)}>
          Save
        </button>
      </div>
    </FormProvider>
  );
};

export default UserForm;
