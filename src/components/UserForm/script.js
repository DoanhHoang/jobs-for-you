import * as yup from "yup";
import userAPI from "../../config/api/user/userAPI";
import { toast } from "react-toastify";

const { updateUserDetails, addUser } = userAPI;

export const schema = yup.object({
  username: yup.string().required("Username is a required field."),
  gender: yup.boolean(),
  phone: yup.string(),
  firstName: yup.string().required("First name is a required field."),
  lastName: yup.string().required("Last name is a required field."),
  email: yup.string(),
  avatar: yup
    .mixed()
    // .optional()
    // .required("* please choose image")
    .test("fileSize", "* Uploaded file is too big!", (value) => {
      if (value) {
        return value[0]?.size ? value[0].size <= 51200 * 1024 : true;
      } else {
        return yup.mixed().notRequired();
      }
    })
    .test("type", " * Allowed *.jpeg, *.jpg, *.png, *.gif.", (value) => {
      if (value) {
        return value[0]?.type
          ? value[0]?.type === "image/jpeg" ||
              value[0].type === "image/png" ||
              value[0].type === "image/jpg" ||
              value[0].type === "image/gif"
          : true;
      } else {
        return yup.mixed().notRequired();
      }
    }),
});

export const handleAddOrUpdateUser = async (data, id, setOpen) => {
  console.log(data);
  // const candidateData = {
  //   candidate: JSON.stringify({
  //     user: {
  //       gender: data.gender,
  //       username: data.username,
  //       phone: data.phone,
  //       firstName: data.firstName,
  //       lastName: data.lastName,
  //       email: data.email,
  //     },
  //   }),
  //   file: data.avatar ? data.avatar[0] : null,
  // };

  const candidateData = {
    gender: data.gender,
    username: data.username,
    phone: data.phone,
    firstName: data.firstName,
    lastName: data.lastName,
    email: data.email,
  };

  const addData = {
    user: JSON.stringify({
      gender: data.gender,
      username: data.username,
      phone: data.phone,
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      pass: "123456",
      role: [
        {
          id: 7,
        },
      ],
    }),
    file: data.avatar ? data.avatar[0] : null,
  };

  if (id) {
    await updateUserDetails(id, candidateData)
      .then(() => {
        setOpen(false);
        toast.success("Update user successfully");
      })
      .catch(() => {
        toast.error("Update user failed");
      });
  } else {
    addUser(addData)
      .then(() => {
        setOpen(false);
        toast.success("Create new user successfully");
      })
      .catch(() => {
        toast.error("Create new user failed");
      });
  }
};
