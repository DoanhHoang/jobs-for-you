import * as React from "react";
import { DataGrid, GridToolbar } from "@mui/x-data-grid";
import { Paper } from "@mui/material";

import "./styles.scss";

const Table = ({ rows, columns, setSelectedRows, status }) => {
  return (
    <div className="data-table">
      <div className="data-table_container">
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={10}
          rowsPerPageOptions={[10]}
          getRowId={(row) => row.id}
          loading={status === "loading" ? true : false}
          disableSelectionOnClick
          checkboxSelection
          autoHeight
          density="comfortable"
          headerHeight={43}
          rowHeight={60}
          {...rows}
          onSelectionModelChange={(ids) => {
            const selectedIDs = new Set(ids);
            const selectedRows = rows.filter((row) => selectedIDs.has(row.id));

            setSelectedRows(selectedRows);
          }}
          // disableColumnFilter={true}
          disableDensitySelector={true}
          disableColumnMenu={true}
          hideFooterPagination
        />
      </div>
    </div>
  );
};

export default Table;
