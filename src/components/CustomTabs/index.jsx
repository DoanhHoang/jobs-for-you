import * as React from "react";
import { Tabs, Tab, Box } from "@mui/material";

import "./styles.scss";

export const TabPanel = (props) => {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`custom-tabpanel-${index}`}
      aria-labelledby={`custom-tab-${index}`}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
};

const a11yProps = (index) => {
  return {
    id: `custom-tab-${index}`,
    "aria-controls": `custom-tabpanel-${index}`,
  };
};

const CustomTabs = ({ value, setValue, listTabs, children }) => {
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box className="custom-tabs">
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="Custom tabs"
          indicatorColor="none"
        >
          <Tab label="All" id="0" />
          {listTabs.length > 0 &&
            listTabs.map((tab) => {
              return <Tab label={tab.name} id={`${tab.id}`} key={tab.id} />;
            })}
        </Tabs>
      </Box>
      {children}
    </Box>
  );
};

export default CustomTabs;
