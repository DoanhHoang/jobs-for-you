import React, { useState } from "react";
import { Grid, Pagination } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";

import "./styles.scss";
import FreelancerCard from "../CustomCard/FreelancerCard";
import { getListCandidateThunk } from "../../store/slices/CandidateSlice";

const FreelancerList = ({
  listFreelancer,
  title = "List Freelancer Apply",
}) => {
  const dispatch = useDispatch();

  const [page, setPage] = useState(1);
  const { pagination } = useSelector((state) => state.candidate);
  return (
    <>
      <div className="freelancer-list box">
        <div className="box__header">
          <h3>{title}</h3>
        </div>
        <div className="freelancer-list__main">
          <Grid container spacing={3}>
            {listFreelancer?.length > 0 &&
              listFreelancer.map((freelancer) => (
                <Grid item md={4} key={freelancer.id}>
                  <FreelancerCard freelancer={freelancer} />
                </Grid>
              ))}
          </Grid>
        </div>
      </div>
      <div className="freelancer-list__pagination">
        <Pagination
          count={pagination.totalPages}
          page={page}
          onChange={(e, value) => {
            setPage(value);
            dispatch(getListCandidateThunk({ no: value - 1 }));
          }}
        />
      </div>
    </>
  );
};

export default FreelancerList;
