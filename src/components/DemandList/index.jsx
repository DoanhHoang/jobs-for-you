import React from "react";
import { Grid } from "@mui/material";

import "./styles.scss";
import DemandCard from "../../components/CustomCard/DemandCard";

const DemandList = ({ list = [], setOpen, setDemandId }) => {
  // const dispatch = useDispatch();

  // const [page, setPage] = useState(1);
  // const { pagination } = useSelector((state) => state.demand);

  const handleOpenDrawer = (id) => {
    setDemandId(id);
    setOpen(true);
  };

  return (
    <>
      <div className="demand-list">
        <div className="demand-list__title">
          <p>
            {list?.length} {list?.length > 1 ? "demands" : "demand"} posted on
            Demands for you
          </p>
        </div>
        <Grid container spacing={3}>
          {list.length > 0 &&
            list?.map((demand) => (
              <Grid item key={demand.id} md={4}>
                <DemandCard
                  item={demand}
                  onClick={() => handleOpenDrawer(demand.id)}
                />
              </Grid>
            ))}
        </Grid>
        {/* <div className="demand-list__pagination">
          <Pagination
            count={pagination.totalPages}
            page={page}
            onChange={(e, value) => {
              setPage(value);
              dispatch(searchCompanyByNameStatusThunk({ no: value - 1 }));
            }}
          />
        </div> */}
      </div>
    </>
  );
};

export default DemandList;
