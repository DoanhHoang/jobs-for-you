import * as React from "react";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import ListItemText from "@mui/material/ListItemText";
import Select from "@mui/material/Select";
import Checkbox from "@mui/material/Checkbox";

import userAPI from "../../config/api/user/userAPI";

const { getListRole } = userAPI;

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const names = [
  "Oliver Hansen",
  "Van Henry",
  "April Tucker",
  "Ralph Hubbard",
  "Omar Alexander",
  "Carlos Abbott",
  "Miriam Wagner",
  "Bradley Wilkerson",
  "Virginia Andrews",
  "Kelly Snyder",
];

const SelectMulti = () => {
  const [roles, setRoles] = React.useState([]);
  const [roleName, setRoleName] = React.useState([]);
  const [roleId, setRoleId] = React.useState([]);

  React.useEffect(() => {
    getListRole().then((roles) => {
      setRoles(roles);
    });
  }, []);

  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setRoleName(typeof value === "string" ? value.split(",") : value);
  };

  React.useEffect(() => {
    if (roleName.length > 0) {
      const roleIdList = roleName.map((item) => {
        const roleFilter = roles.filter((role) => role.name === item);
        return roleFilter[0].id;
      });
      setRoleId(roleIdList);
    }
  }, [roleName, roles]);

  console.log(roleId, roleName.length);

  return (
    <div>
      <FormControl sx={{ m: 0, width: "100%" }}>
        <InputLabel id="demo-multiple-checkbox-label">Role</InputLabel>
        <Select
          labelId="demo-multiple-checkbox-label"
          id="demo-multiple-checkbox"
          multiple
          fullWidth
          value={roleName}
          onChange={handleChange}
          input={<OutlinedInput label="Tag" />}
          renderValue={(selected) => selected.join(", ")}
          MenuProps={MenuProps}
        >
          {roles.map((role) => (
            <MenuItem key={role.name} value={role.name}>
              <Checkbox checked={roleName?.indexOf(role.name) > -1} />
              <ListItemText primary={role.name} />
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default SelectMulti;
