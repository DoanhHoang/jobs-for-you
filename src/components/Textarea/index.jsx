import "./styles.scss";
import React, { useEffect, useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";

const Textarea = ({
  label,
  id,
  type,
  placeholder,
  children = null,
  register,
  check = false,
  requirementField = true,
  setValue,
  inputValue,
  status,
}) => {
  const [quillValue, setquillValue] = useState("");
  useEffect(() => {
    register(id);
  }, [register]);

  useEffect(() => {
    if (status === "success") {
      setquillValue("");
      if (setValue) {
        setValue(id, "");
      }
    }
  }, [status]);

  const [showError1, setShowError1] = useState(false);
  const [showError2, setShowError2] = useState(true);

  useEffect(() => {
    inputValue && setquillValue(inputValue);
  }, [inputValue]);

  const handleOnChange = (content, delta, source, editor) => {
    if (editor.getText().length <= 1) {
      setShowError1(true);
      setShowError2(true);
    } else {
      setShowError1(false);
      setShowError2(false);
    }
    setquillValue(content);
    setValue(id, content);
  };

  return (
    <div className="custom-textarea">
      <div htmlFor={id} className="custom-textarea__label">
        {label}
        {requirementField && <span className="field-requirment"> *</span>}
      </div>
      <div
        id={id}
        className={
          check
            ? "custom-input__textarea-disabled"
            : "custom-textarea__textfield"
        }
      >
        <ReactQuill
          theme="snow"
          value={quillValue}
          onChange={handleOnChange}
          placeholder={placeholder}
        />
        {check ? null : (
          <p className="custom-textarea__error">
            {(children === null ? showError1 : showError2 ? children : "") || (
              <span
                style={{
                  marginTop: "2px",
                  fontSize: "12px",
                  fontStyle: "italic",
                  color: "#999",
                }}
              >
                (Tối đa 1500 ký tự)
              </span>
            )}
          </p>
        )}
      </div>
    </div>
  );
};

export default Textarea;
