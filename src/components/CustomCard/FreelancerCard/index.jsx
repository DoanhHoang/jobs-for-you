import React from "react";
import { useNavigate } from "react-router-dom";

import "./styles.scss";
import userAvatar from "../../../asset/img/admin-user.png";

const FreelancerCard = ({ freelancer }) => {
  const navigate = useNavigate();
  return (
    <>
      <div className="user-card">
        <div className="user-card__header">
          <img src={freelancer.userDTO?.avatar || userAvatar} alt="avatar" />
        </div>
        <div className="user-card__main">
          <h3>
            {freelancer.userDTO?.firstName} {freelancer.userDTO?.lastName}
          </h3>
          <h5>{freelancer.major?.name}</h5>
          <p>{freelancer.userDTO?.email}</p>
        </div>
        <div className="user-card__footer">
          <button type="button" className="user-card__footer--connect">
            Connect
          </button>
          <button
            type="button"
            className="user-card__footer--cv"
            onClick={() =>
              navigate(`/find-talent/candidate-detail/${freelancer?.id}`)
            }
          >
            Watch CV
          </button>
        </div>
      </div>
    </>
  );
};

export default FreelancerCard;
