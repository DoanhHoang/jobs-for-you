import React from "react";
import PlaceOutlinedIcon from "@mui/icons-material/PlaceOutlined";
import CalendarMonthOutlinedIcon from "@mui/icons-material/CalendarMonthOutlined";
import { useNavigate } from "react-router-dom";

import "./styles.scss";

const DemandCard = ({ item, onClick }) => {
  const {
    name,
    salary,
    imageSrc = "https://careersidekick.com/wp-content/uploads/2020/05/What-is-an-IT-job-career-guide.jpg",
    ortherInfo,
    location = "Ho Chi Minh City",
    start = "03/10/2022",
  } = item;

  const navigate = useNavigate();

  return (
    <>
      <div className="custom-card">
        <div className="custom-card__header">
          <img src={imageSrc} alt="logo" />
          <div className="custom-card__header-title" onClick={onClick}>
            <h2>{name}</h2>
            <p>{salary}</p>
          </div>
        </div>
        <div className="custom-card__footer">
          <div className="custom-card__info">
            <div className="custom-card__info--address">
              <PlaceOutlinedIcon fontSize="small" />
              <p>{location}</p>
            </div>

            <div className="custom-card__info--date">
              <CalendarMonthOutlinedIcon fontSize="small" />
              <p>{start}</p>
            </div>
          </div>
        </div>
        <div
          className="custom-card__main"
          dangerouslySetInnerHTML={{ __html: ortherInfo }}
        >
          {/* <p>{ortherInfo}</p> */}
        </div>
        <div className="custom-card__footer">
          <button
            type="button"
            onClick={() => navigate(`/demand-detail/${item.id}`)}
          >
            See more
          </button>
        </div>
      </div>
    </>
  );
};

export default DemandCard;
