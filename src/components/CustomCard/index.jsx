import React, { useState, useEffect } from "react";
import PlaceOutlinedIcon from "@mui/icons-material/PlaceOutlined";
import { Tooltip } from "@mui/material";
import CalendarMonthOutlinedIcon from "@mui/icons-material/CalendarMonthOutlined";
import FormatListBulletedOutlinedIcon from "@mui/icons-material/FormatListBulletedOutlined";
import FavoriteBorderIcon from "@mui/icons-material/FavoriteBorder";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { toast } from "react-toastify";

import "./styles.scss";
import {
  getJobsLikeByCandidateIdThunk,
  // searchJobThunk,
} from "../../store/slices/JobSlice";
import jobAPI from "../../config/api/job/jobAPI";
import { ROLE_NAME } from "../../config/constant/roleNameConstant";

const { likeJob, unlikeJob } = jobAPI;

const CustomCard = ({ item, onClick }) => {
  const {
    id,
    name,
    salary,
    imageSrc = "https://careersidekick.com/wp-content/uploads/2020/05/What-is-an-IT-job-career-guide.jpg",
    ortherInfo,
    location = "Ho Chi Minh City",
    jobTypes,
    start = "03/10/2022",
  } = item;
  const { jobLikeList } = useSelector((state) => state.job);
  const { roleDetail, userDetail } = useSelector((state) => state.user);
  const [like, setLike] = useState(false);
  const navigate = useNavigate();

  const dispatch = useDispatch();

  useEffect(() => {
    setLike(
      jobLikeList && jobLikeList.filter((item) => item.id === id).length > 0
    );
  }, [jobLikeList, id]);

  const handleLikeJob = () => {
    if (like) {
      unlikeJob(id, roleDetail.id)
        .then(() => {
          setLike(false);
          toast.success("Unlike job successfully");
          // dispatch(searchJobThunk());
          dispatch(getJobsLikeByCandidateIdThunk(roleDetail.id));
        })
        .catch(() => {
          toast.success("Unlike job successfully");
        });
    } else {
      likeJob(id, roleDetail.id)
        .then(() => {
          setLike(true);
          toast.success("Like job successfully");
          // dispatch(searchJobThunk());
          dispatch(getJobsLikeByCandidateIdThunk(roleDetail.id));
        })
        .catch(() => {
          toast.success("Like job successfully");
        });
    }
  };

  return (
    <>
      <div className="custom-card">
        <div className="custom-card__header">
          <img src={imageSrc} alt="logo" />
          <div className="custom-card__header-title" onClick={onClick}>
            <h2>{name}</h2>
            <p>{salary}</p>
          </div>
        </div>
        <div className="custom-card__footer">
          <div className="custom-card__info">
            <div className="custom-card__info--address">
              <PlaceOutlinedIcon fontSize="small" />
              <p>{location}</p>
            </div>
            {jobTypes && (
              <div className="custom-card__info--type">
                <FormatListBulletedOutlinedIcon fontSize="small" />
                <p>{jobTypes[0]?.name}</p>
              </div>
            )}

            <div className="custom-card__info--date">
              <CalendarMonthOutlinedIcon fontSize="small" />
              <p>{start}</p>
            </div>
          </div>
        </div>
        <div
          className="custom-card__main"
          dangerouslySetInnerHTML={{ __html: ortherInfo }}
        >
          {/* <p>{ortherInfo}</p> */}
        </div>
        <div className="custom-card__footer">
          <button
            type="button"
            onClick={() => navigate(`/job-detail/${item.id}`)}
          >
            See more
          </button>
          {userDetail &&
            userDetail?.role?.length > 0 &&
            userDetail?.role[0]?.name === ROLE_NAME.CANDIDATE && (
              <Tooltip title="Like">
                <FavoriteBorderIcon
                  onClick={handleLikeJob}
                  className={like ? "like" : ""}
                />
              </Tooltip>
            )}
        </div>
      </div>
    </>
  );
};

export default CustomCard;
