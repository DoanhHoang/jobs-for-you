import React, { useState, useEffect, useContext } from "react";
import { useLocation, useNavigate, NavLink } from "react-router-dom";
import {
  Menu,
  MenuItem,
  Avatar,
  Divider,
  ListItemIcon,
  IconButton,
} from "@mui/material";
import Logout from "@mui/icons-material/Logout";
import AccountBoxOutlinedIcon from "@mui/icons-material/AccountBoxOutlined";
import PostAddOutlinedIcon from "@mui/icons-material/PostAddOutlined";
import { useSelector, useDispatch } from "react-redux";

import "./styles.scss";
import { handleLogOut } from "../../utils/index";
import { AuthenticationContext } from "../../App";
import { ROLE_NAME } from "../../config/constant/roleNameConstant";
import { checkRole } from "../../utils";

const MenuList = ({ anchorEl, open, handleClose }) => {
  const { setAuth } = useContext(AuthenticationContext);

  const { userDetail } = useSelector((state) => state.user);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  const regedictToMyProfile = () => {
    if (
      userDetail?.role?.length > 0 &&
      checkRole(userDetail?.role, ROLE_NAME.COMPANY)
    ) {
      navigate("/company/profile");
    } else if (
      userDetail?.role?.length > 0 &&
      checkRole(userDetail?.role, ROLE_NAME.UNIVERSITY)
    ) {
      navigate("/university/profile");
    } else {
      navigate("/candidate/profile");
    }
  };

  return (
    <Menu
      anchorEl={anchorEl}
      id="account-menu"
      open={open}
      onClose={handleClose}
      onClick={handleClose}
      PaperProps={{
        elevation: 0,
        sx: {
          padding: "10px 20px 10px 10px",
          overflow: "visible",
          filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
          mt: 1.5,
          "& .MuiAvatar-root": {
            width: 38,
            height: 38,
            ml: -0.5,
            mr: 1,
          },
          "&:before": {
            content: '""',
            display: "block",
            position: "absolute",
            top: 0,
            right: 14,
            width: 10,
            height: 10,
            bgcolor: "background.paper",
            transform: "translateY(-50%) rotate(45deg)",
            zIndex: 0,
          },
        },
      }}
      transformOrigin={{ horizontal: "right", vertical: "top" }}
      anchorOrigin={{ horizontal: "right", vertical: "bottom" }}
    >
      <MenuItem onClick={regedictToMyProfile}>
        <ListItemIcon>
          <AccountBoxOutlinedIcon />
        </ListItemIcon>
        Profile
      </MenuItem>
      {userDetail?.role?.length > 0 &&
        userDetail?.role[0]?.name === ROLE_NAME.COMPANY && (
          <MenuItem onClick={() => navigate("post-job")}>
            <ListItemIcon>
              <PostAddOutlinedIcon />
            </ListItemIcon>
            Post Job
          </MenuItem>
        )}
      {userDetail?.role?.length > 0 &&
        userDetail?.role[0]?.name === ROLE_NAME.UNIVERSITY && (
          <MenuItem onClick={() => navigate("post-demand")}>
            <ListItemIcon>
              <PostAddOutlinedIcon />
            </ListItemIcon>
            Post Demand
          </MenuItem>
        )}

      <Divider />
      <MenuItem onClick={() => handleLogOut(setAuth, dispatch, navigate)}>
        <ListItemIcon>
          <Logout fontSize="small" />
        </ListItemIcon>
        Logout
      </MenuItem>
    </Menu>
  );
};

const ClientHeader = ({ handleOpenDrawer }) => {
  const [sticky, setSticky] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const { auth } = useContext(AuthenticationContext);
  const { userDetail } = useSelector((state) => state.user);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const location = useLocation();
  const navigate = useNavigate();

  // add/remove scroll event listener
  useEffect(() => {
    const checkLocation = location.pathname !== "/";
    const handleScrollEvent = () => {
      window.scrollY > 400
        ? setSticky(true)
        : checkLocation
        ? setSticky(true)
        : setSticky(false);
    };

    window.addEventListener("scroll", handleScrollEvent);

    return () => {
      window.removeEventListener("scroll", handleScrollEvent);
    };
  }, [location.pathname]);

  useEffect(() => {
    const checkLocation = location.pathname !== "/";
    checkLocation ? setSticky(true) : setSticky(false);
  }, [location.pathname]);

  return (
    <>
      <div className={`client-header ${sticky ? "sticky" : ""}`}>
        <div className="client-header__container">
          <div className="client-header__container-left">
            <div className="client-header__container-left-show-sidebar">
              <img
                src="https://img.icons8.com/external-kmg-design-outline-color-kmg-design/28/000000/external-dashboard-user-interface-kmg-design-outline-color-kmg-design.png"
                onClick={handleOpenDrawer}
                alt="icon"
              />
            </div>
            <div className="client-header__container-left-logo">
              <h2 className="client-header__container-left-logo-text">
                Jobs For You
              </h2>
            </div>
            <div className="client-header__container-left-list">
              <NavLink to="/">home</NavLink>
              <NavLink to="/find-talent">find candidates</NavLink>
              <NavLink to="/find-jobs">find jobs</NavLink>
              {userDetail?.role?.length > 0 &&
                userDetail?.role[0]?.name === ROLE_NAME.COMPANY && (
                  <NavLink to="/find-demand">find demands</NavLink>
                )}
              <a
                href="https://www.facebook.com/profile.php?id=100080187162001"
                target="_blank"
                rel="noopener noreferrer"
              >
                Contact us
              </a>
            </div>
          </div>

          <div className="client-header__container-actions">
            {!auth ? (
              <>
                <button
                  className="client-header__container-actions-signup"
                  onClick={() => navigate("/account/choose-role")}
                >
                  Sign Up
                </button>
                <button
                  className="client-header__container-actions-login"
                  onClick={() => navigate("/account/sign-in")}
                >
                  Log In
                </button>
              </>
            ) : (
              <IconButton
                onClick={handleClick}
                size="small"
                aria-controls={open ? "account-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
              >
                <Avatar src={userDetail?.avatar} sx={{ width: 32, height: 32 }}>
                  {userDetail?.username?.charAt(0).toUpperCase()}
                </Avatar>
              </IconButton>
            )}
          </div>
        </div>
        <MenuList anchorEl={anchorEl} open={open} handleClose={handleClose} />
      </div>
    </>
  );
};

export default ClientHeader;
