import React from "react";
import { Grid } from "@mui/material";

import "./styles.scss";

const TrustBy = () => {
  return (
    <>
      <div className="trust-by">
        <Grid container spacing={2}>
          <Grid item md={2}>
            <div className="trust-by__conatiner">
              <p>Trust by</p>
            </div>
          </Grid>
          <Grid item md={2}>
            <div className="trust-by__conatiner">
              <img
                src="https://res.cloudinary.com/upwork-cloud-acquisition-prod/image/upload//c_fit/general/logobar/colored/microsoft.svg"
                alt="Microsoft-soft"
              />
            </div>
          </Grid>
          <Grid item md={2}>
            <div className="trust-by__conatiner">
              <img
                src="https://www.upwork.com/static/assets/Arges/img/airbnb.b4ccb67.svg"
                alt="air-bnb"
              />
            </div>
          </Grid>
          <Grid item md={2}>
            <div className="trust-by__conatiner">
              <img
                src="https://www.upwork.com/static/assets/Arges/img/ge.cd9c77d.svg"
                alt="ge"
              />
            </div>
          </Grid>

          <Grid item md={2}>
            <div className="trust-by__conatiner">
              <img
                src="https://www.upwork.com/static/assets/Arges/img/bissell.0c2b170.svg"
                alt="bissell"
              />
            </div>
          </Grid>
          <Grid item md={2}>
            <div className="trust-by__conatiner">
              <img
                src="https://www.upwork.com/static/assets/Arges/img/automatic.96cc1d6.svg"
                alt="automatic"
              />
            </div>
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default TrustBy;
