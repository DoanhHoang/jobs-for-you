import React from "react";
import CloseIcon from "@mui/icons-material/Close";

import "./styles.scss";
import CustomModal from "../index";
// import ClassForm from "../ClassForm/ClassForm";
import UserForm from "../../UserForm";

const AddOrUpdateUser = ({ open, setOpen, id }) => {
  return (
    <>
      <CustomModal open={open} setOpen={setOpen}>
        <div className="modal-header">
          <p>{!id ? "Create new user" : `Update user - ID: ${id}`}</p>
          <CloseIcon onClick={() => setOpen(false)} />
        </div>
        <UserForm id={id} setOpen={setOpen} />
      </CustomModal>
    </>
  );
};

export default AddOrUpdateUser;
