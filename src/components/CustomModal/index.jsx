import React from "react";
import "./styles.scss";

import { Modal } from "@mui/material";

const CustomModal = ({ open, setOpen, children }) => {
  const handleClose = () => setOpen(false);
  return (
    <Modal open={open} onClose={handleClose}>
      <div className="custom-modal">{children}</div>
    </Modal>
  );
};

export default CustomModal;
