import React from "react";
import CloseIcon from "@mui/icons-material/Close";

import "./styles.scss";
import CustomModal from "../index";
// import ClassForm from "../ClassForm/ClassForm";
import UniversityForm from "../../../pages/admin/University/UniversityForm";

const UpdateCompanyProfile = ({ open, setOpen, id }) => {
  return (
    <>
      <CustomModal open={open} setOpen={setOpen}>
        <div className="modal-header">
          <p>{!id ? "Create new university" : `Update university profile`}</p>
          <CloseIcon onClick={() => setOpen(false)} />
        </div>
        <UniversityForm universityId={id} setOpen={setOpen} />
      </CustomModal>
    </>
  );
};

export default UpdateCompanyProfile;
