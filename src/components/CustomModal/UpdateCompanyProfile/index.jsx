import React from "react";
import CloseIcon from "@mui/icons-material/Close";

import "./styles.scss";
import CustomModal from "../index";
// import ClassForm from "../ClassForm/ClassForm";
import CompanyForm from "../../../pages/admin/Company/CompanyForm";

const UpdateCompanyProfile = ({ open, setOpen, id }) => {
  console.log(id);
  return (
    <>
      <CustomModal open={open} setOpen={setOpen}>
        <div className="modal-header">
          <p>{!id ? "Create new company" : `Update company profile`}</p>
          <CloseIcon onClick={() => setOpen(false)} />
        </div>
        <CompanyForm companyId={id} setOpen={setOpen} />
      </CustomModal>
    </>
  );
};

export default UpdateCompanyProfile;
