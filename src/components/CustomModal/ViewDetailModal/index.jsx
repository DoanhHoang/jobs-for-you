import React, { useState } from "react";
import { CircularProgress } from "@mui/material";
import { DeleteForeverOutlined } from "@mui/icons-material/";
import TaskAltIcon from "@mui/icons-material/TaskAlt";
import HighlightOffOutlinedIcon from "@mui/icons-material/HighlightOffOutlined";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

import "./styles.scss";
import CustomModal from "../index";
import activelogo from "../../../asset/img/activities.png";
import companyAPI from "../../../config/api/company/companyAPI";
import universityAPI from "../../../config/api/university/universityAPI";
import cannidateAPI from "../../../config/api/cannidate/cannidateAPI";
import jobAPI from "../../../config/api/job/jobAPI";
import { getJobListThunk } from "../../../store/slices/JobSlice";
import { searchCompanyByNameStatusThunk } from "../../../store/slices/CompanySlice";
import { searchUniversityByNameStatusThunk } from "../../../store/slices/UniversitySlice";
import { getListCandidateThunk } from "../../../store/slices/CandidateSlice";

const ViewDetailModal = ({
  open,
  setOpen,
  id,
  isActive,
  item,
  selectedRows,
  setSelectedRows,
}) => {
  const [status, setStatus] = useState("idle");
  const { disableJobs, activeJobs } = jobAPI;
  const { activeCompany, disableCompany } = companyAPI;
  const { disableUniversity, activeUniversity } = universityAPI;
  const { updateCandidateStatus } = cannidateAPI;
  const { filtersCom } = useSelector((state) => state.company);
  const { filtersUni } = useSelector((state) => state.university);
  const { pagination } = useSelector((state) => state.job);
  const { filters } = useSelector((state) => state.candidate);

  const dispatch = useDispatch();

  const handleDisableItem = () => {
    setStatus("Loading");
    if (item === "company") {
      disableCompany(id ? [{ id: id }] : selectedRows)
        .then(() => {
          toast.success("Disable successfully");
          setSelectedRows([]);
          setStatus("success");
          dispatch(searchCompanyByNameStatusThunk(filtersCom));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Disable failed");
          setOpen(false);
          setStatus("error");
        });
    }
    if (item === "university") {
      disableUniversity(id ? [{ id: id }] : selectedRows)
        .then(() => {
          toast.success("Disable successfully");
          setSelectedRows([]);
          setStatus("success");
          dispatch(searchUniversityByNameStatusThunk(filtersUni));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Disable failed");
          setOpen(false);
          setStatus("error");
        });
    }
    if (item === "job") {
      disableJobs(id ? [{ id: id }] : selectedRows)
        .then(() => {
          toast.success("Disable successfully");
          setSelectedRows([]);
          setStatus("success");
          dispatch(getJobListThunk({ no: pagination.pageCurrent }));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Disable failed");
          setOpen(false);
          setStatus("error");
        });
    }
    if (item === "candidate") {
      updateCandidateStatus(id, 2)
        .then(() => {
          toast.success("Disable successfully");
          setSelectedRows([]);
          setStatus("success");
          dispatch(getListCandidateThunk(filters));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Disable failed");
          setOpen(false);
          setStatus("error");
        });
    }
  };

  const handleActiveItem = () => {
    setStatus("Loading");
    if (item === "company") {
      activeCompany(id ? [{ id: id }] : selectedRows)
        .then(() => {
          toast.success("Active successfully");
          setSelectedRows([]);
          setStatus("success");
          dispatch(searchCompanyByNameStatusThunk(filtersCom));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Active failed");
          setStatus("error");
          setOpen(false);
        });
    }
    if (item === "university") {
      activeUniversity(id ? [{ id: id }] : selectedRows)
        .then(() => {
          toast.success("Active successfully");
          setSelectedRows([]);
          setStatus("success");
          dispatch(searchUniversityByNameStatusThunk(filtersUni));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Active failed");
          setStatus("error");
          setOpen(false);
        });
    }
    if (item === "job") {
      activeJobs(id ? [{ id: id }] : selectedRows)
        .then(() => {
          toast.success("Active successfully");
          setSelectedRows([]);
          setStatus("success");
          dispatch(getJobListThunk({ no: pagination.pageCurrent }));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Active failed");
          setOpen(false);
          setStatus("error");
        });
    }
    if (item === "candidate") {
      updateCandidateStatus(id, 1)
        .then(() => {
          toast.success("Active successfully");
          setSelectedRows([]);
          setStatus("success");
          dispatch(getListCandidateThunk(filters));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Active failed");
          setOpen(false);
          setStatus("error");
        });
    }
  };

  return (
    <>
      <CustomModal open={open} setOpen={setOpen}>
        <div className="view-detail-modal">
          {status === "Loading" ? (
            <div className="view-detail-modal__progress">
              <CircularProgress />
            </div>
          ) : (
            <>
              <div className="view-detail-modal__main">
                <img src={activelogo} alt="active-logo" />
                <p>Are you sure about this change?</p>
              </div>
              <div className="view-detail-modal__footer">
                {!isActive && (
                  <button
                    className="view-detail-modal__footer--delete"
                    onClick={handleDisableItem}
                  >
                    <DeleteForeverOutlined
                      style={{ marginRight: 7, marginTop: -3 }}
                    />{" "}
                    Disable
                  </button>
                )}
                {isActive && (
                  <button
                    className="view-detail-modal__footer--active"
                    onClick={handleActiveItem}
                  >
                    <TaskAltIcon style={{ marginRight: 7, marginTop: -3 }} />
                    Active
                  </button>
                )}

                <button
                  className="view-detail-modal__footer--cancel"
                  onClick={() => setOpen(false)}
                >
                  <HighlightOffOutlinedIcon
                    style={{ marginRight: 7, marginTop: -3 }}
                  />
                  Cancel
                </button>
              </div>
            </>
          )}
        </div>
      </CustomModal>
    </>
  );
};

export default ViewDetailModal;
