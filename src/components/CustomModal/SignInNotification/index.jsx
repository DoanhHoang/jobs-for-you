import React from "react";
import { useNavigate } from "react-router-dom";

import "./styles.scss";
import CustomModal from "../index";
import siginInImage from "../../../asset/img/signin.png";

const SignInNotification = ({ open, setOpen }) => {
  const navigate = useNavigate();

  const handleSignInFoward = () => {
    navigate("/account/sign-in");
  };

  return (
    <>
      <CustomModal open={open} setOpen={setOpen}>
        <div className="Sign-in-notification">
          <>
            <img src={siginInImage} alt="delete-icon" />
            <p>You must be logged in to apply this job.</p>
            <div className="Sign-in-notification__actions">
              <button
                type="button"
                className="Sign-in-notification__actions--delete"
                onClick={handleSignInFoward}
              >
                Sign In
              </button>
              <button
                type="button"
                className="Sign-in-notification__actions--cancel"
                onClick={() => setOpen(false)}
              >
                Cancel
              </button>
            </div>
          </>
        </div>
      </CustomModal>
    </>
  );
};

export default SignInNotification;
