import React, { useState } from "react";
import { CircularProgress } from "@mui/material";
import companyAPI from "../../../config/api/company/companyAPI";
import cannidateAPI from "../../../config/api/cannidate/cannidateAPI";
import jobAPI from "../../../config/api/job/jobAPI";
import universityAPI from "../../../config/api/university/universityAPI";
import { searchCompanyByNameStatusThunk } from "../../../store/slices/CompanySlice";
import { getListCandidateThunk } from "../../../store/slices/CandidateSlice";
import { getJobListThunk } from "../../../store/slices/JobSlice";
import { searchUniversityByNameStatusThunk } from "../../../store/slices/UniversitySlice";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

import "./styles.scss";
import CustomModal from "../index";
import deleteImage from "../../../asset/img/delete-file.png";

const VertifyDelete = ({
  open,
  setOpen,
  id,
  setSelectedRows,
  selectedRows,
  item,
}) => {
  const [status, setStatus] = useState("idle");
  const { deleteForceCompany } = companyAPI;
  const { deleteCandidate } = cannidateAPI;
  const { deleteJob } = jobAPI;
  const { deleteForceUniversity } = universityAPI;
  const { filtersUni } = useSelector((state) => state.university);
  const { pagination } = useSelector((state) => state.job);
  const { filtersCom } = useSelector((state) => state.company);
  const { filters } = useSelector((state) => state.candidate);

  const dispatch = useDispatch();

  const handleDelete = () => {
    setStatus("Loading");
    if (item === "company") {
      deleteForceCompany(id ? [{ id: id }] : selectedRows)
        .then(() => {
          toast.success("Delete successfully");
          setStatus("success");
          dispatch(searchCompanyByNameStatusThunk(filtersCom));
          setSelectedRows([]);
          setOpen(false);
        })
        .catch(() => {
          toast.error("Delete failed");
          setStatus("error");
          setOpen(false);
        });
    }
    if (item === "candidate") {
      deleteCandidate(id)
        .then(() => {
          toast.success("Delete successfully");
          setStatus("success");
          dispatch(getListCandidateThunk(filters));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Delete failed");
          setStatus("error");
          setOpen(false);
        });
    }
    if (item === "job") {
      deleteJob(id)
        .then(() => {
          toast.success("Delete successfully");
          setStatus("success");
          dispatch(getJobListThunk({ no: pagination.pageCurrent }));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Delete failed");
          setStatus("error");
          setOpen(false);
        });
    }
    if (item === "university") {
      deleteForceUniversity([{ id: id }])
        .then(() => {
          toast.success("Delete successfully");
          setStatus("success");
          dispatch(searchUniversityByNameStatusThunk(filtersUni));
          setOpen(false);
        })
        .catch(() => {
          toast.error("Delete failed");
          setStatus("error");
          setOpen(false);
        });
    }
  };

  return (
    <>
      <CustomModal open={open} setOpen={setOpen}>
        <div className="vertify-delete">
          {status === "Loading" ? (
            <div className="vertify-delete__progress">
              <CircularProgress />
            </div>
          ) : (
            <>
              <img src={deleteImage} alt="delete-icon" />
              <p>Are you sure you want to delete this lesson ?</p>
              <div className="vertify-delete__actions">
                <button
                  type="button"
                  className="vertify-delete__actions--delete"
                  onClick={handleDelete}
                >
                  Delete
                </button>
                <button
                  type="button"
                  className="vertify-delete__actions--cancel"
                  onClick={() => setOpen(false)}
                >
                  Cancel
                </button>
              </div>
            </>
          )}
        </div>
      </CustomModal>
    </>
  );
};

export default VertifyDelete;
