import React from "react";
import { Grid } from "@mui/material";
import "./styles.scss";

const ClientFooter = () => {
  return (
    <footer>
      <div className="footer">
        <Grid container spacing={2}>
          <Grid item md={4} xs={12} className="footer-col">
            <div className="footer-logo">{/* <Logo color="white" /> */}</div>
          </Grid>

          <Grid item md={4} sm={12} className="footer-col">
            <h3>For Clients</h3>
            <ul>
              <li>
                <a target="_blank" href="/#">
                  How to Hire
                </a>
              </li>
              <li>
                <a target="_blank" href="/#">
                  Talent Marketplace
                </a>
              </li>
              <li>
                <a target="_blank" href="/#">
                  Project Catalog
                </a>
              </li>
              <li>
                <a target="_blank" href="/#">
                  Talent Scout
                </a>
              </li>
              <li>
                <a target="_blank" href="/#">
                  Hire an Agency
                </a>
              </li>
            </ul>
          </Grid>

          <Grid item md={4} sm={12} className="footer-col">
            <h3>For Talent</h3>
            <ul>
              <li>
                <a target="_blank" href="/#">
                  How to Find Work
                </a>
              </li>
              <li>
                <a target="_blank" href="/#">
                  Direct Contracts
                </a>
              </li>
            </ul>
          </Grid>

          <Grid item md={4} sm={12} className="footer-col">
            <h3>Resources</h3>
            <ul>
              <li>
                <a target="_blank" href="/#">
                  Help & Support
                </a>
              </li>
              <li>
                <a target="_blank" href="/#">
                  Success Stories
                </a>
              </li>
              <li>
                <a target="_blank" href="/#">
                  All Resources
                </a>
              </li>
            </ul>
          </Grid>

          <Grid item md={4} sm={12} className="footer-col">
            <h3>Company</h3>
            <ul>
              <li>
                <a target="_blank" href="/#">
                  About Us
                </a>
              </li>
              <li>
                <a target="_blank" href="/#">
                  Leadership
                </a>
              </li>
            </ul>
          </Grid>

          <Grid item md={4} sm={12} className="footer-col">
            <h3>Contact Us</h3>
            <ul>
              <li>Email: support@doanhhoang.com</li>
              <li>
                <a target="_blank" href="/#">
                  Community
                </a>
              </li>
            </ul>
          </Grid>
        </Grid>
      </div>
    </footer>
  );
};

export default ClientFooter;
