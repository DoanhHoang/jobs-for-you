import React from "react";

import "./styles.scss";

const AdminFooter = () => {
  return (
    <div className="admin-footer">
      @ Coppyright 2022, Make by Hoang Ngoc Doanh
    </div>
  );
};

export default AdminFooter;
