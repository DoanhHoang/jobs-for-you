import React, { useState, useEffect } from "react";
import { Select, MenuItem, FormControl, InputLabel } from "@mui/material";
import { useDispatch } from "react-redux";

import "./styles.scss";

const FilterSelect = (props) => {
  const { listFilter, label, id, setProvinceId, filters, filterSlice } = props;
  const [value, setValue] = useState("");
  const [filter, setFilter] = useState(
    listFilter.length > 0 && listFilter[0].name
  );
  const dispatch = useDispatch();

  const handleFilterChange = (event) => {
    const filtered = listFilter.filter(
      (filter) => filter.name === event.target.value
    )[0];
    console.log(filtered);

    if (setProvinceId) {
      dispatch(
        filterSlice.actions.updateFilter({
          ...filtered.value,
        })
      );
      setProvinceId(filtered.value.province);
      setValue(event.target.value);
    } else {
      dispatch(
        filterSlice.actions.updateFilter({
          ...filtered.value,
        })
      );

      setValue(event.target.value);
    }
  };

  useEffect(() => {
    if (listFilter.length > 0) {
      setFilter(listFilter[0].name);
    }
  }, [listFilter]);

  useEffect(() => {
    const filteredValue = listFilter.filter(
      (fil) => fil.value[id] === filters[id]
    )[0];
    if (filteredValue) {
      setFilter(filteredValue.name);
    }
  }, [filters[id]]);

  return (
    <div className="select__filter--select-list">
      <FormControl fullWidth size="small">
        <InputLabel id="select__filter--select-label">{label}</InputLabel>
        <Select
          labelId="select__filter--select-label"
          className={`select__filter--select`}
          value={value}
          defaultValue={filter}
          label={label}
          onChange={handleFilterChange}
        >
          {listFilter.length > 0 ? (
            listFilter.map((filterItem) => {
              return (
                <MenuItem value={filterItem.name} key={filterItem.name}>
                  {filterItem.name}
                </MenuItem>
              );
            })
          ) : (
            <MenuItem value="">Please choose province</MenuItem>
          )}
        </Select>
      </FormControl>
    </div>
  );
};

export default FilterSelect;
