import React from "react";

import "./styles.scss";

const AdminHeader = ({ handleOpenDrawer }) => {
  return (
    <div className="admin-header">
      <div className="admin-header__show-sidebar">
        <img
          src="https://img.icons8.com/external-kmg-design-outline-color-kmg-design/28/000000/external-dashboard-user-interface-kmg-design-outline-color-kmg-design.png"
          onClick={handleOpenDrawer}
        />
      </div>
      <div className="admin-header-logo"></div>
    </div>
  );
};

export default AdminHeader;
