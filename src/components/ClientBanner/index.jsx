import React from "react";
import { Grid } from "@mui/material";

import "./styles.scss";
import clientBanner from "../../asset/img/client-banner.png";

const ClientBanner = () => {
  return (
    <>
      <div className="client-banner">
        <Grid container className="client-banner__container">
          <Grid item md={6} className="client-banner__container-left">
            <div>
              <h1><span>Jobs</span> For You</h1>
              <p>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat
                inventore non, repellendus sed impedit vel quo suscipit culpa
                illum eaque?
              </p>
              <button type="button">Explore Now</button>
            </div>
          </Grid>
          <Grid item md={6} className="client-banner__container-right">
            <img src={clientBanner} alt="client-banner-img" />
          </Grid>
        </Grid>
      </div>
    </>
  );
};

export default ClientBanner;
