import React, { useState } from "react";
import { TextField, InputAdornment, IconButton } from "@mui/material";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";

import "./styles.scss";

const CustomInput = ({
  name,
  register,
  type = "text",
  placeholder,
  errors,
  label,
  title,
  startIcon,
  defaultValue = "",
  multiline,
  minRows = 1,
  maxRows = 8,
}) => {
  const [visibility, setVisibility] = useState(false);

  const onChangeVisibility = () => {
    setVisibility(!visibility);
  };

  return (
    <>
      <div className="custom-input">
        <div className="custom-input__title">{title && <p>{title}</p>}</div>
        <TextField
          id={name}
          name={name}
          type={type === "password" ? (visibility ? "text" : "password") : type}
          defaultValue={defaultValue}
          {...register(name)}
          placeholder={placeholder && placeholder}
          multiline={multiline}
          label={label}
          variant="outlined"
          fullWidth
          minRows={minRows}
          maxRows={maxRows}
          InputLabelProps={{ shrink: true }}
          InputProps={{
            startAdornment: startIcon && (
              <InputAdornment position="start">{startIcon}</InputAdornment>
            ),
            endAdornment: type === "password" && (
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={onChangeVisibility}
                  edge="end"
                >
                  {!visibility ? (
                    <VisibilityOff fontSize="small" />
                  ) : (
                    <Visibility fontSize="small" />
                  )}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <p className="custom-input--err">{errors && errors[name]?.message}</p>
      </div>
    </>
  );
};

export default CustomInput;
