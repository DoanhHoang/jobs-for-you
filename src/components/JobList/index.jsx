import React, { useState } from "react";
import { Grid, Pagination } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";

import "./styles.scss";
import CustomCard from "../../components/CustomCard";
import { searchCompanyByNameStatusThunk } from "../../store/slices/CompanySlice";

const JobList = ({ list = [], setOpen, setJobId }) => {
  const dispatch = useDispatch();

  const [page, setPage] = useState(1);
  const { pagination } = useSelector((state) => state.job);

  const handleOpenDrawer = (id) => {
    setJobId(id);
    setOpen(true);
  };

  return (
    <>
      <div className="job-list">
        <div className="job-list__title">
          <p>
            Check out a sample of the {list.length} Developer{" "}
            {list?.length > 1 ? "jobs" : "job"} posted on Jobs for you
          </p>
        </div>
        <Grid container spacing={3}>
          {list.length > 0 &&
            list?.map((job) => (
              <Grid item key={job.id} md={6}>
                <CustomCard
                  item={job}
                  onClick={() => handleOpenDrawer(job.id)}
                />
              </Grid>
            ))}
        </Grid>
        <div className="job-list__pagination">
          <Pagination
            count={pagination.totalPages}
            page={page}
            onChange={(e, value) => {
              setPage(value);
              dispatch(searchCompanyByNameStatusThunk({ no: value - 1 }));
            }}
          />
        </div>
      </div>
    </>
  );
};

export default JobList;
