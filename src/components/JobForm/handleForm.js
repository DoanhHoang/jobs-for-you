import * as yup from "yup";
import { toast } from "react-toastify";

import { convert } from "../../utils/index";
import jobAPI from "../../config/api/job/jobAPI";

const { createJob, updateJob } = jobAPI;

export const schema = yup
  .object({
    name: yup.string().required(" * Bạn phải điền chức danh."),
    jobTypes: yup.string().required(" * Bạn phải chọn hình thức làm việc."),
    company: yup.string(),
    jobPosition: yup.string(),
    amount: yup
      .number()
      // .typeError(
      //   " * Số lượng ứng viên không được để trống hoặc không phải là số."
      // )
      .min(1, " * Số lượng ứng viên phải lớn hơn 0. ")
      .integer(" * Số lượng ứng viên phải là số nguyên. "),
    start: yup
      .date()
      // .min(
      //   `${date}`,
      //   ` * Bạn không thể chọn ngày bắt đầu tuyển sau ngày ${dateNow}`
      // )
      .required(),
    end: yup
      .date()
      // .min(`${tomorowFormat}`, "Ngày hết hạn phải lớn hơn ngày bắt đầu")
      .required(),
    district: yup.string(),
    province: yup.string(),
    country: yup.string(),
    address: yup.string(),
    description: yup.string(),
    requirement: yup.string(),
    ortherInfo: yup.string(),
    salaryMin: yup
      .number()
      // .typeError(" * Vui lòng không nhập kí tự khác ngoài số.")
      .min(1000, " * Số tiền trợ cấp phải lớn hơn 1000."),
    salaryMax: yup
      .number()
      // .typeError(" * Vui lòng không nhập kí tự khác ngoài số.")
      .min(
        yup.ref("salaryMin"),
        " * Mức trợ cấp tối đa phải lớn hơn hoặc bằng mức tối thiểu."
      ),
  })
  .required();

const defaultValue = {
  name: "",
  description: "",
  jobTypes: "",
  company: "",
  jobPosition: "",
  amount: 0,
  start: "",
  end: "",
  district: "",
  province: "",
  country: "",
  address: "",
  requirement: "",
  ortherInfo: "",
  salaryMin: 0,
  salaryMax: 0,
};

/**
 * Add or Edit company
 * @param {*} isAdd
 * @param {*} data
 * @param {*} id
 * @param {*} reset
 */
export const AddOrUpdateJob = async (
  isAdd,
  data,
  id,
  reset,
  navigate,
  setStatus
) => {
  const jobData = {
    name: data.name,
    amount: data.amount,
    jobPosition: data.jobPosition,
    description: data.description,
    requirement: data.requirement,
    ortherInfo: data.ortherInfo,
    start: convert(data.start),
    end: convert(data.end),
    company: {
      id: parseInt(data.company),
    },
    jobTypes: [
      {
        id: parseInt(data.jobTypes),
      },
    ],
  };

  // console.log(data, companyData);

  if (isAdd) {
    await createJob(jobData)
      .then(() => {
        setStatus("success");
        toast.success("Created Job successfully");
        reset(defaultValue);
      })
      .catch(() => {
        setStatus("error");
        toast.error("Created Job failed");
      });
  } else {
    await updateJob(jobData, id)
      .then(() => {
        setStatus("success");
        toast.success("Updated Job successfully");
        navigate(-1);
      })
      .catch(() => {
        setStatus("success");
        toast.error("Updated Job failed");
      });
  }
};
