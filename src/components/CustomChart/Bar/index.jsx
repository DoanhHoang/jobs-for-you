import React from "react";
import { Bar } from "react-chartjs-2";
import { Chart, registerables } from "chart.js";

import "./styles.scss";

Chart.register(...registerables);

const CustomBar = ({ data, title }) => {
  return (
    <>
      <Bar
        className="custom-bar"
        data={data}
        options={{
          plugins: {
            title: {
              display: true,
              text: title,
              fontFamily: "'Poppins', sans-serif",
              position: "bottom",
            },
          },
          legend: {
            display: true,
            position: "right",
            fontFamily: "'Poppins', sans-serif",
          },
        }}
      />
    </>
  );
};

export default CustomBar;
