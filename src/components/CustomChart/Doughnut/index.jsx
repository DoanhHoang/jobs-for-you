import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Chart, registerables } from "chart.js";

import "./styles.scss";

Chart.register(...registerables);

const CustomDoughnut = ({ data, title }) => {
  console.log(data);
  return (
    <>
      <Doughnut
        className="custom-doughnut"
        data={data}
        options={{
          plugins: {
            title: {
              display: true,
              text: title,
              fontFamily: "'Poppins', sans-serif",
              position: "bottom",
            },
          },
          legend: {
            display: true,
            position: "right",
            fontFamily: "'Poppins', sans-serif",
          },
        }}
      />
    </>
  );
};

export default CustomDoughnut;
