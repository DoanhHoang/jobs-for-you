import React from "react";
import { Drawer } from "@mui/material";

import "./styles.scss";

const AdminDrawer = ({ open, setOpen, children }) => {
  const handleCloseDrawer = () => {
    setOpen(false);
  };

  return (
    <>
      <Drawer anchor="left" open={open} onClose={handleCloseDrawer}>
        {children}
      </Drawer>
    </>
  );
};

export default AdminDrawer;
