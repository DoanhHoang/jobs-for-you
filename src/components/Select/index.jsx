import React, { useEffect } from "react";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import "./styles.scss";

import { useFormContext, Controller } from "react-hook-form";
import locationAPI from "../../config/api/location/locationAPI";

const { getDistrictListByProvince } = locationAPI;

export default function SelectCustom({
  label,
  id,
  children,
  options = [],
  placeholder,
  requirementField = true,
  setListDistrict,
  updateData,
}) {
  const { control } = useFormContext();

  const renderSelectOption = () => {
    return options.map((item) => {
      return (
        <MenuItem
          onClick={() => {
            handleChangeLocation(item.id);
          }}
          value={item.id}
          key={item.id}
        >
          {item.name}
        </MenuItem>
      );
    });
  };

  //handle change district
  const handleChangeLocation = (id) => {
    if (setListDistrict) {
      getDistrictListByProvince(id).then((res) => setListDistrict(res.data));
    }
  };

  useEffect(() => {
    if (
      updateData?.locations &&
      updateData?.locations.length > 0 &&
      updateData?.locations[0].district?.province?.id
    ) {
      getDistrictListByProvince(
        updateData?.locations[0].district?.province?.id
      ).then((res) => setListDistrict(res.data));
    }
  }, [updateData, setListDistrict]);

  return (
    <>
      <div className="select-form">
        <h1 className="select-label">
          {label}
          {requirementField && <span className="field-requirment"> *</span>}
        </h1>
        <Controller
          render={({ field }) => (
            <Select {...field} fullWidth>
              <MenuItem disabled>
                <em className="select-placeholder">{placeholder}</em>
              </MenuItem>
              {renderSelectOption()}
            </Select>
          )}
          control={control}
          name={id}
          defaultValue=""
        />

        <p className="select-error">{children}</p>
      </div>
    </>
  );
}
