import React, { useState } from "react";
import {
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Collapse,
  List,
  Tooltip,
} from "@mui/material";
import { ExpandLess, ExpandMore } from "@mui/icons-material";

import "./styles.scss";

const ListMenu = ({ itemIcon, itemText, isCollapsed, children }) => {
  const [open, setOpen] = useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  return (
    <>
      <ListItemButton onClick={handleClick}>
        {itemIcon && <ListItemIcon>{itemIcon}</ListItemIcon>}
        <Tooltip title={itemText}>
          <ListItemText primary={itemText} />
        </Tooltip>
        {isCollapsed ? open ? <ExpandLess /> : <ExpandMore /> : null}
      </ListItemButton>
      {isCollapsed && (
        <div className="list-menu_children">
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {children}
            </List>
          </Collapse>
        </div>
      )}
    </>
  );
};

export default ListMenu;
