import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
// import AnalyticsOutlinedIcon from "@mui/icons-material/AnalyticsOutlined";
import AppsOutlinedIcon from "@mui/icons-material/AppsOutlined";
import SchoolOutlinedIcon from "@mui/icons-material/SchoolOutlined";
import BusinessOutlinedIcon from "@mui/icons-material/BusinessOutlined";
import ArticleOutlinedIcon from "@mui/icons-material/ArticleOutlined";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import LocationOnOutlinedIcon from "@mui/icons-material/LocationOnOutlined";
import AdminPanelSettingsOutlinedIcon from "@mui/icons-material/AdminPanelSettingsOutlined";

const sidebarLink = [
  {
    title: "Overview",
    list: [
      {
        id: 1,
        to: "",
        name: "Dashboard",
        icon: AppsOutlinedIcon,
      },
      // {
      //   id: 2,
      //   to: "statistical",
      //   name: "Statistical",
      //   icon: AnalyticsOutlinedIcon,
      // },
    ],
  },
  {
    title: "General",
    list: [
      {
        id: 3,
        to: "user",
        name: "Admin",
        icon: AdminPanelSettingsOutlinedIcon,
      },
      {
        id: 0,
        to: "candidate",
        name: "Candidate",
        icon: AccountCircleOutlinedIcon,
      },
      {
        id: 4,
        to: "university",
        name: "University",
        icon: SchoolOutlinedIcon,
      },
      {
        id: 5,
        to: "company",
        name: "Company",
        icon: BusinessOutlinedIcon,
      },
      {
        id: 6,
        to: "demand",
        name: "Demand",
        icon: ArticleOutlinedIcon,
      },
      {
        id: 7,
        to: "job",
        name: "Job",
        icon: ArticleOutlinedIcon,
      },
    ],
  },
  {
    title: "Miscellaneous",
    list: [
      {
        id: 7,
        to: "major",
        name: "Major",
        icon: AccountCircleIcon,
      },
      {
        id: 8,
        to: "location",
        name: "Location ",
        icon: LocationOnOutlinedIcon,
      },
    ],
  },
];

export default sidebarLink;
