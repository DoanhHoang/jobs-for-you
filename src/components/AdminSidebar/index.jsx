import React, { useContext } from "react";
import { List, Divider } from "@mui/material";
import { NavLink, useNavigate } from "react-router-dom";
import { checkRole } from "../../utils";

import "./styles.scss";
import ListMenu from "./ListMenu";
import sidebarLink from "./script";

import adminLogo from "../../asset/img/logo-j4u.jpg";
import adminUser from "../../asset/img/admin-user.png";
import { AuthenticationContext } from "../../App";

const AdminSidebar = () => {
  const navigate = useNavigate();
  const { setAdminAuth } = useContext(AuthenticationContext);

  const adminAccount = JSON.parse(sessionStorage.getItem("adminAccount"));

  // navigate to main dashboard
  const handleBackToDashboard = () => {
    navigate("/admin");
  };

  // render list Sidebar
  const renderListSidebar = () => {
    const navLinkClass = ({ isActive }) => {
      return isActive ? "active" : "";
    };

    return (
      sidebarLink &&
      sidebarLink.map((item) => (
        <ListMenu
          itemText={item.title}
          key={item.title}
          isCollapsed={item.list && true}
        >
          {item.list.map((child) => {
            return (
              <React.Fragment key={child.id}>
                {child.name !== "Admin" ? (
                  <NavLink to={child.to} className={navLinkClass}>
                    <ListMenu
                      key={child.id}
                      itemIcon={<child.icon />}
                      itemText={child.name}
                      isCollapsed={false}
                    />
                  </NavLink>
                ) : (
                  adminAccount?.roles?.length > 0 &&
                  checkRole(adminAccount?.roles, "Role_Super_Admin") && (
                    <NavLink to={child.to} className={navLinkClass}>
                      <ListMenu
                        key={child.id}
                        itemIcon={<child.icon />}
                        itemText={child.name}
                        isCollapsed={false}
                      />
                    </NavLink>
                  )
                )}
              </React.Fragment>
            );
          })}
        </ListMenu>
      ))
    );
  };

  return (
    <>
      <div className="admin-sidebar__logo" onClick={handleBackToDashboard}>
        <img src={adminLogo} alt="admin-logo" width="50%" />
      </div>
      <div className="admin-sidebar-content" id="sidebar-scrollbar">
        <div className="admin-sidebar-content__admin-user">
          <div className="admin-sidebar-content__admin-user-avatar">
            <img alt="admin avatar" src={adminUser} width="100px" />
            <div className="chip">Avatar</div>
          </div>
          <div className="admin-sidebar-content__admin-user-info">
            <p>{adminAccount?.username}</p>
            <h5>{adminAccount?.roles.join(", ")}</h5>
            <button
              type="button"
              onClick={() => {
                sessionStorage.removeItem("adminAccount");
                setAdminAuth(false);
              }}
            >
              Logout
            </button>
          </div>
        </div>
        <Divider />
        <div className="admin-sidebar-content__management">
          <List
            sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
            component="nav"
            aria-labelledby="nested-list-subheader"
          >
            {renderListSidebar()}
          </List>
        </div>
      </div>
    </>
  );
};

export default AdminSidebar;
