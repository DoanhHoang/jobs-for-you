import React, { useState, useEffect } from "react";
import { Document, Page, pdfjs } from "react-pdf";
import cv from "../../asset/pdf/cv.pdf";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";

import "./styles.scss";
import cannidateAPI from "../../config/api/cannidate/cannidateAPI";

const url = cv;
const { uploadCV } = cannidateAPI;

const PreviewPdf = () => {
  pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);
  const [cvName, setCvName] = useState("");
  const [cvFile, setCvFile] = useState(null);

  const { roleDetail } = useSelector((state) => state.user);

  useEffect(() => {
    if (roleDetail) {
      setCvName(
        roleDetail.cv?.split("/")[roleDetail.cv?.split("/").length - 1]
      );
      setCvFile(
        `http://localhost:8080/upload/${
          roleDetail.cv?.split("/")[roleDetail.cv?.split("/").length - 1]
        }`
      );
    }
  }, [roleDetail]);

  /*When document gets loaded successfully*/
  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
    setPageNumber(1);
  }

  function changePage(offset) {
    setPageNumber((prevPageNumber) => prevPageNumber + offset);
  }

  function previousPage() {
    changePage(-1);
  }

  function nextPage() {
    changePage(1);
  }

  // show preview pdf
  const showPreviewCV = (e) => {
    if (e.target.files && e.target.files[0]) {
      let imageFile = e.target.files[0];
      setCvName(imageFile.name);
      const reader = new FileReader();
      reader.onload = (x) => {
        setCvFile(x.target.result);
      };
      reader.readAsDataURL(imageFile);

      const data = {
        candidateId: JSON.stringify(roleDetail?.id),
        file: e.target.files[0],
      };
      uploadCV(data)
        .then(() => {
          toast.success("Upload CV successfully");
        })
        .catch(() => {
          toast.error("Upload CV failed");
        });
    }
  };

  return (
    <>
      <div className="preview-pdf box">
        <div className="box__header">
          <div className="box__header-left">
            <h3>Preview CV</h3>
            <p>Endorsements from past clients</p>
          </div>
        </div>
        <div className="preview-pdf__upload">
          <p>CV file name: {cvName || "Please upload your CV."}</p>
          <div className="preview-pdf__upload--cv">
            <label htmlFor="cv-upload">
              <p>Upload CV</p>
            </label>
            <input
              id="cv-upload"
              className="preview-pdf__upload--cv-btn"
              type="file"
              onChange={showPreviewCV}
            />
          </div>
        </div>

        <div className="preview-pdf__main">
          <Document file={cvFile} onLoadSuccess={onDocumentLoadSuccess}>
            <Page pageNumber={pageNumber} />
          </Document>
        </div>
        <div className="preview-pdf__footer">
          <div className="preview-pdf__footer-pagec">
            Page {pageNumber || (numPages ? 1 : "--")} of {numPages || "--"}
          </div>
          <div className="preview-pdf__footer-buttonc">
            <button
              type="button"
              disabled={pageNumber <= 1}
              onClick={previousPage}
              className="Pre"
            >
              Previous
            </button>
            <button
              type="button"
              disabled={pageNumber >= numPages}
              onClick={nextPage}
            >
              Next
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default PreviewPdf;
