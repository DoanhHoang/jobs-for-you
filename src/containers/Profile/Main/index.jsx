import React, { useState } from "react";
import { Grid, Divider } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";

import userAvatar from "../../../asset/img/admin-user.png";
import AddOrUpdateUser from "../../../components/CustomModal/AddOrUpdateUser/AddOrUpdateUser";

const Main = () => {
  const [open, setOpen] = useState(false);
  const { userDetail, roleDetail } = useSelector((state) => state.user);

  const navigate = useNavigate();

  return (
    <>
      <div className="profile__main box">
        <div className="profile__main--header">
          <Grid container>
            <Grid item md={8}>
              <div className="profile__main--header-avatar">
                <img src={userDetail?.avatar || userAvatar} alt="avatar" />
                <div>
                  <h1>
                    {userDetail.firstName} {userDetail.lastName}
                  </h1>
                  <p>{userDetail.status?.name}</p>
                </div>
              </div>
            </Grid>
            <Grid item md={4}>
              <div className="profile__main--header-action">
                <button
                  type="button"
                  onClick={() => navigate("/pass/change-password")}
                >
                  Change Password
                </button>
                <button type="button" onClick={() => setOpen(true)}>
                  Edit Profile
                </button>
              </div>
            </Grid>
          </Grid>
        </div>
        <Divider />
        <div className="profile__main--info">
          <h3>About</h3>
          <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sequi
            veniam aut perspiciatis repellendus iste sed, hic cumque quibusdam
            dolorem architecto fuga, ipsam rem blanditiis porro provident
            accusantium ut. Cum, architecto possimus necessitatibus aperiam
            facere voluptatem, nesciunt dignissimos dolores vel quasi,
            repudiandae eaque. Autem error nostrum velit, odio consectetur
            architecto nesciunt.
          </p>
          <Grid container spacing={3}>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Username: </h5>
                <p>{userDetail.username}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Gender: </h5>
                <p>{userDetail.gender ? "Male" : "Female"}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Email: </h5>
                <p>{userDetail.email}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Phone: </h5>
                <p>{userDetail.phone}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Major: </h5>
                <p>{roleDetail?.major?.name}</p>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
      <AddOrUpdateUser open={open} setOpen={setOpen} id={userDetail.id} />
    </>
  );
};

export default Main;
