import React from "react";

import "./styles.scss";
import Header from "./Header";
import Main from "./Main";

const Profile = ({ children }) => {
  return (
    <>
      <div className="profile">
        <Header />
        <Main />
        {children}
      </div>
    </>
  );
};

export default Profile;
