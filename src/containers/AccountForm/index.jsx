import React from "react";
import { useNavigate } from "react-router-dom";

import "./styles.scss";
import logo from "../../asset/img/logo-j4u.jpg";

// import Logo from "../../component/Logo";

const AccountForm = ({ children }) => {
  const navigate = useNavigate();
  return (
    <div className="account-form">
      {/* <Logo /> */}
      <img src={logo} alt="logo" width="180px" onClick={() => navigate("/")} />
      {children}
    </div>
  );
};

export default AccountForm;
