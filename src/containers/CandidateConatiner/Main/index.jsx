import React, { useState, useEffect } from "react";
import { Grid, Divider } from "@mui/material";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";

import userAvatar from "../../../asset/img/admin-user.png";
import AddOrUpdateUser from "../../../components/CustomModal/AddOrUpdateUser/AddOrUpdateUser";
import { getCandidateDetailThunk } from "../../../store/slices/CandidateSlice";

const Main = () => {
  const { id } = useParams();
  const [open, setOpen] = useState(false);
  const { userDetail } = useSelector((state) => state.user);
  const { candidateDetail } = useSelector((state) => state.candidate);

  const dispatch = useDispatch();

  useEffect(() => {
    id && dispatch(getCandidateDetailThunk(id));
  }, [id, dispatch]);
  console.log(candidateDetail);

  return (
    <>
      <div className="profile__main box">
        <div className="profile__main--header">
          <Grid container>
            <Grid item md={8}>
              <div className="profile__main--header-avatar">
                <img
                  src={candidateDetail?.userDTO?.avatar || userAvatar}
                  alt="avatar"
                />
                <div>
                  <h1>
                    {candidateDetail?.userDTO?.firstName}{" "}
                    {candidateDetail?.userDTO?.lastName}
                  </h1>
                  <p>{candidateDetail?.userDTO?.status?.name}</p>
                </div>
              </div>
            </Grid>
          </Grid>
        </div>
        <Divider />
        <div className="profile__main--info">
          <h3>About</h3>
          <p>
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Sequi
            veniam aut perspiciatis repellendus iste sed, hic cumque quibusdam
            dolorem architecto fuga, ipsam rem blanditiis porro provident
            accusantium ut. Cum, architecto possimus necessitatibus aperiam
            facere voluptatem, nesciunt dignissimos dolores vel quasi,
            repudiandae eaque. Autem error nostrum velit, odio consectetur
            architecto nesciunt.
          </p>
          <Grid container spacing={3}>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Username: </h5>
                <p>{candidateDetail?.userDTO?.username}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Gender: </h5>
                <p>{candidateDetail?.userDTO?.gender ? "Male" : "Female"}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Email: </h5>
                <p>{candidateDetail?.userDTO?.email}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Phone: </h5>
                <p>{candidateDetail?.userDTO?.phone}</p>
              </div>
            </Grid>
            <Grid item md={6}>
              <div className="profile__main--info-text">
                <h5>Major: </h5>
                <p>{candidateDetail?.major?.name}</p>
              </div>
            </Grid>
          </Grid>
        </div>
      </div>
      <AddOrUpdateUser open={open} setOpen={setOpen} id={userDetail.id} />
    </>
  );
};

export default Main;
