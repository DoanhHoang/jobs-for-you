import React from "react";

const Header = () => {
  return (
    <>
      <div className="profile__header box">
        <h1>Better market your expertise with specialized profiles</h1>
        <p>
          Specialized profiles allow you to display more specific skills,
          deliverables, and more – and help power better search results and job
          recommendations.
        </p>
        <button type="button">Add Specialized Profile</button>
      </div>
    </>
  );
};

export default Header;
