import {
  Dashboard,
  Company,
  University,
  CompanyForm,
  UniversityForm,
  Job,
  JobFormCompany,
  User,
  Demand,
  Candidate,
  DemandFormUniversity,
} from "../pages/admin";

import {
  Home,
  FindJob,
  PostJob,
  CannidateProfile,
  JobDetail,
  CompanyProfile,
  UniversityProfile,
  FindTalent,
  EditCandidateProfile,
  EditCompanyProfile,
  FindDemand,
  PostDemand,
  CandidateDetail,
  DemandDetail,
} from "../pages/main";

import {
  SignIn,
  SignUp,
  ChooseRole,
  ForgetPassword,
  ChangePassword,
  ResetPassword,
} from "../pages/account";

import AdminSignIn from "../pages/auth/AdminSignIn";

export const authRouter = [
  {
    id: 1,
    path: "sign-in",
    Component: AdminSignIn,
  },
];

export const adminRouter = [
  {
    id: 1,
    path: "/admin",
    Component: Dashboard,
  },
  {
    id: 2,
    path: "company",
    Component: Company,
  },
  {
    id: 3,
    path: "university",
    Component: University,
  },
  {
    id: 4,
    path: "company/:id",
    Component: CompanyForm,
  },
  {
    id: 5,
    path: "company/add",
    Component: CompanyForm,
  },
  {
    id: 6,
    path: "job",
    Component: Job,
  },
  {
    id: 7,
    path: "university/:id",
    Component: UniversityForm,
  },
  {
    id: 8,
    path: "university/add",
    Component: UniversityForm,
  },
  {
    id: 9,
    path: "job/:id",
    Component: JobFormCompany,
  },
  {
    id: 10,
    path: "job/add",
    Component: JobFormCompany,
  },
  {
    id: 11,
    path: "user",
    Component: User,
  },
  {
    id: 12,
    path: "demand",
    Component: Demand,
  },
  {
    id: 13,
    path: "candidate",
    Component: Candidate,
  },
  {
    id: 14,
    path: "demand/add",
    Component: DemandFormUniversity,
  },
  // {
  //   id: 15,
  //   path: "demand/:id"",
  //   Component: DemandFormUniversity,
  // },
];

export const clientRouter = [
  {
    id: 1,
    path: "",
    Component: Home,
  },
  {
    id: 2,
    path: "find-jobs",
    Component: FindJob,
  },
  {
    id: 3,
    path: "post-job",
    Component: PostJob,
  },
  {
    id: 4,
    path: "candidate/profile",
    Component: CannidateProfile,
  },
  {
    id: 5,
    path: "job-detail/:id",
    Component: JobDetail,
  },
  {
    id: 6,
    path: "company/profile",
    Component: CompanyProfile,
  },
  {
    id: 7,
    path: "university/profile",
    Component: UniversityProfile,
  },
  {
    id: 8,
    path: "find-talent",
    Component: FindTalent,
  },
  {
    id: 9,
    path: "candidate/profile/edit",
    Component: EditCandidateProfile,
  },
  {
    id: 10,
    path: "company/profile/edit",
    Component: EditCompanyProfile,
  },
  {
    id: 11,
    path: "reset-password/email=:email&verifyCode=:verifyCode",
    Component: ResetPassword,
  },
  {
    id: 12,
    path: "find-demand",
    Component: FindDemand,
  },
  {
    id: 13,
    path: "/find-talent/candidate-detail/:id",
    Component: CandidateDetail,
  },
  {
    id: 13,
    path: "post-demand",
    Component: PostDemand,
  },
  {
    id: 14,
    path: "demand-detail/:id",
    Component: DemandDetail,
  },
];

export const accountRouter = [
  {
    id: 1,
    path: "sign-in",
    Component: SignIn,
  },
  {
    id: 2,
    path: "sign-up/:roleId",
    Component: SignUp,
  },
  {
    id: 3,
    path: "choose-role",
    Component: ChooseRole,
  },
  {
    id: 4,
    path: "forget-password",
    Component: ForgetPassword,
  },
  {
    id: 5,
    path: "change-password",
    Component: ChangePassword,
  },
];

export const passRouter = [
  {
    id: 1,
    path: "forget-password",
    Component: ForgetPassword,
  },
  {
    id: 2,
    path: "change-password",
    Component: ChangePassword,
  },
  {
    id: 3,
    path: "reset-password/email=:email&verifyCode=:verifyCode",
    Component: ResetPassword,
  },
];
