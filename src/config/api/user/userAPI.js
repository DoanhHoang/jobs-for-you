import api from "../apiConfig";

const userAPI = {
  changePassword: (id, data) => {
    const url = `user/change-password/${id}`;

    return api.post(url, data);
  },
  forgetPassword: (email) => {
    const url = `user/forgot-password`;
    console.log(email);

    return api.get(url, {
      params: { email },
    });
  },
  resetPassword: (email, verifyCode, data) => {
    const url = "user/reset-password";

    return api.post(url, data, {
      params: { email, verifyCode },
    });
  },
  getUserDetailById: (id) => {
    const url = `user/${id}`;

    return api.get(url);
  },
  getCandidateDetailByUserId: (userId) => {
    const url = "candidates/user";

    return api.get(url, {
      params: { userId },
    });
  },
  getCompanyDetailByUserId: (userId) => {
    const url = "company/user";

    return api.get(url, {
      params: { userId },
    });
  },
  getUniversityDetailByUserId: (userId) => {
    const url = "university/user";

    return api.get(url, {
      params: { userId },
    });
  },
  updateUserDetails: (id, data) => {
    const url = `user/${id}`;

    return api.put(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  getAllUsers: ({ pageNumber, pageSize }) => {
    const url = "user";

    return api.get(url, {
      params: {
        pageNumber,
        pageSize,
      },
    });
  },
  getListRole: () => {
    const url = "role";

    return api.get(url);
  },
  addUser: (data) => {
    const url = "user";

    return api.post(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  getUsersByRoleId: (id) => {
    const url = `role/${id}/users`;

    return api.get(url);
  },
};

export default userAPI;
