import api from "../apiConfig";

const demandAPI = {
  getAllDemand: () => {
    const url = "demand";

    return api.get(url);
  },
  createNewDemand: (data) => {
    const url = "demand";

    return api.post(url, data);
  },
  disableDemand: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `company/v1/disable?${params}`;

    return api.put(url);
  },
  activeDemand: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `company/v1/active?${params}`;

    return api.put(url);
  },
  getDemandDetail: (id) => {
    const url = `demand/${id}`;

    return api.get(url);
  },
};

export default demandAPI;
