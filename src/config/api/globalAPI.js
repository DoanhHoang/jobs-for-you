import api from "./apiConfig";

const globalAPI = {
  getAllMajors: () => {
    const url = "/majors";

    return api.get(url);
  },
  getProvinceList: () => {
    const url = "location/provinces";

    return api.get(url);
  },
  getDistrictByProvince: (id) => {
    const url = `location/provinces/${id}/districts`;
    return api.get(url);
  },
  signIn: (username, password) => {
    const url = "login";

    return api.post(url, null, {
      params: { username, password },
    });
  },
};

export default globalAPI;
