import api from "../apiConfig";

const companyAPI = {
  getCompanyList: (no, limit) => {
    const url = "company/";

    return api.get(url, {
      params: {
        no,
        limit,
      },
    });
  },
  addCompany: (data) => {
    const url = "company";

    return api.post(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  getCompanyDetail: (comId) => {
    const url = `company/${comId}`;

    return api.get(url);
  },
  updateCompanyInfo: (data, id) => {
    const url = `company/v1/${id}`;

    return api.put(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  disableCompany: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `company/v1/disable?${params}`;

    return api.put(url);
  },
  activeCompany: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `company/v1/active?${params}`;

    return api.put(url);
  },
  deleteForceCompany: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `company/delete-force?${params}`;

    return api.delete(url);
  },
  filterCompanyByStatus: (statusId) => {
    const url = `company/status/${statusId}`;

    return api.get(url);
  },
  searchCompanyByNameStatus: ({ statusId, name, no, limit }) => {
    const url = "company/search";

    return api.get(url, {
      params: {
        status: statusId,
        name,
        no,
        limit,
      },
    });
  },
  getJobByCompanyId: (id) => {
    const url = `company/${id}/jobs`;

    return api.get(url);
  },
  companyRegister: (data) => {
    const url = "company/register";

    return api.post(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  getCandidatesApplyJobByJobId: (jobId) => {
    const url = `company/${jobId}/applied/candidates`;

    return api.get(url);
  },
};

export default companyAPI;
