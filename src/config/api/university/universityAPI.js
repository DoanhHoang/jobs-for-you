import api from "../apiConfig";

const universityAPI = {
  getUniversityList: (no, limit) => {
    const url = "university";

    return api.get(url, {
      params: {
        no,
        limit,
      },
    });
  },
  addUniversity: (data) => {
    const url = "university";

    return api.post(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  getUniversityDetail: (id) => {
    const url = `university/${id}`;

    return api.get(url);
  },
  updateUniversityInfo: (data, id) => {
    const url = `university/${id}`;

    return api.put(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  disableUniversity: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `university/v1/disable?${params}`;

    return api.put(url);
  },
  activeUniversity: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `university/v1/active?${params}`;

    return api.put(url);
  },
  deleteForceUniversity: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `university/delete-force?${params}`;

    return api.delete(url);
  },
  filterUniversityByStatus: (statusId) => {
    const url = `university/status/${statusId}`;

    return api.get(url);
  },
  searchUniversityByNameStatus: ({ statusId, name, no, limit }) => {
    const url = "university/search";

    return api.get(url, {
      params: {
        status: statusId,
        name,
        no,
        limit,
      },
    });
  },
  getDemandsByUniversityId: (id) => {
    const url = `university/${id}/demands`;

    return api.get(url);
  },
  universityRegister: (data) => {
    const url = "university/register";

    return api.post(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
};

export default universityAPI;
