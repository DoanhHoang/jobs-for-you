import api from "../apiConfig";

const locationAPI = {
  getProvinceList: () => {
    const url = "location/provinces";

    return api.get(url);
  },
  getDistrictListByProvince: (id) => {
    const url = `location/provinces/${id}/districts`;
    return api.get(url);
  },
};

export default locationAPI;
