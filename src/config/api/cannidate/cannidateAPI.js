import api from "../apiConfig";

const cannidateAPI = {
  getJobsCannidateApply: (id) => {
    const url = `/company/${id}/applied/candidates`;

    return api.get(url);
  },
  getCannidateDetail: (id) => {
    const url = `/candidates/${id}`;

    return api.get(url);
  },
  deleteCandidate: (id) => {
    const url = `/candidates/${id}`;

    return api.delete(url);
  },
  candidateRegister: (data) => {
    const url = "candidates";

    return api.post(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
  getListCandidate: ({ firstName, lastName, no, limit }) => {
    const url = "candidates";

    return api.get(url, {
      params: {
        firstName,
        lastName,
        no,
        limit,
      },
    });
  },
  updateCandidateStatus: (candidateId, statusId) => {
    const url = `candidates/status/${candidateId}/${statusId}`;

    return api.put(url);
  },
  uploadCV: (data) => {
    const url = "candidates/upload/cv";

    return api.put(url, data, {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    });
  },
};

export default cannidateAPI;
