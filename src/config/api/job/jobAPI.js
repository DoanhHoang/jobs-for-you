import api from "../apiConfig";

const jobAPI = {
  getJobList: ({ no, limit, status }) => {
    const url = "job";

    return api.get(url, {
      params: { no, limit, status },
    });
  },
  getJobDetail: (id) => {
    const url = `job/${id}`;

    return api.get(url);
  },
  createJob: (job) => {
    const url = "job";

    return api.post(url, job);
  },
  updateJob: (job, id) => {
    const url = `job/${id}`;

    return api.put(url, job);
  },
  deleteJob: (id) => {
    const url = `job/${id}`;

    return api.delete(url);
  },
  getJobByCompanyId: (comId) => {
    const url = `company/${comId}/jobs`;

    return api.get(url);
  },
  searchJob: ({ province, district, type, name, company, no, limit }) => {
    const url = "job/q";

    return api.get(url, {
      params: {
        province,
        district,
        type,
        name,
        company,
        no,
        limit,
      },
    });
  },
  applyJob: (jobId, candidateId) => {
    const url = "apply-job";

    const data = {
      candidate: {
        id: candidateId,
      },
      job: {
        id: jobId,
      },
    };

    return api.post(url, data);
  },
  likeJob: (jobId, candidateId) => {
    const url = `like/${candidateId}/${jobId}`;

    return api.post(url);
  },
  getJobsLikeByCandidateId: (candidateId) => {
    const url = `like/${candidateId}`;

    return api.get(url);
  },
  getJobsApplyByCandidateId: (candidateId) => {
    const url = `candidates/${candidateId}/jobs`;

    return api.get(url);
  },
  unlikeJob: (jobId, candidateId) => {
    const url = `like/${candidateId}/${jobId}`;

    return api.delete(url);
  },
  disableJobs: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `job/disable?${params}`;

    return api.put(url);
  },
  activeJobs: (data) => {
    const params = data
      .map((item) => {
        return `id=${item.id}`;
      })
      .join("&");

    const url = `job/active?${params}`;

    return api.put(url);
  },
};

export default jobAPI;
