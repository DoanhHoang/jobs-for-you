import api from "../apiConfig";

const statisticalAPI = {
  statisticUserByMonth: (month) => {
    const url = "statistics/user";

    return api.get(url, {
      params: { month },
    });
  },
  statisticUserVerifyEmail: () => {
    const url = "statistics/user/verify-email";

    return api.get(url);
  },
  statisticUserRole: () => {
    const url = "statistics/user-role";

    return api.get(url);
  },
};

export default statisticalAPI;
