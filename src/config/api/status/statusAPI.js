import api from "../apiConfig";

const statusAPI = {
  getAllStatus: () => {
    const url = "status";

    return api.get(url);
  },
};

export default statusAPI;
