export const ROLE_NAME = {
  CANDIDATE: "Role_Candidate",
  ADMIN: "Role_Admin",
  COMPANY: "Role_Company",
  SUPER_ADMIN: "Role_Super_Admin",
  UNIVERSITY: "Role_University",
};
